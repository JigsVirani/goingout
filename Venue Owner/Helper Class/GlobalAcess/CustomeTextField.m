//
//  CustomeTextField.m
//  DesinableTextField
//
//  Created by asheshshah on 24/12/15.
//  Copyright © 2015 Asheshshah. All rights reserved.
//

#import "CustomeTextField.h"

@implementation CustomeTextField
static  NSString *password;


-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        if (ArrayTextField == nil ) {
            ArrayTextField = [[NSMutableArray alloc]init];
        }
        else{
            
        }
        GlobalArray = [[NSMutableArray alloc]init];
        [self load];
    }
    return self;
}




-(void)load
{
    
    textfield.frame = self.bounds;
    textfield.textColor = self.textColor;
    if (self.background == Nil) {
        
        self.background = [UIImage imageNamed:CustomeBackGround];
        
    }
    else{
        
        textfield.background = self.background;
        
    }
    if (CustomeFontColor == nil) {
        textfield.textColor = self.textColor;
    }
    else{
        
        textfield.textColor = CustomeFontColor;
    }
    self.attributedText = textfield.attributedText;
    
    textfield.keyboardType = self.keyboardType;
    textfield.textAlignment = self.textAlignment;
    
    textfield.font = self.font;
    textfield.textColor = self.textColor;
    
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
    textfield.userInteractionEnabled = NO;
    textfield.delegate = self.delegate;
    
    [ArrayTextField addObject:self];
    
    
    
    
    [self addSubview:textfield];
    
    
    
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    
    if (txtBounds == Nil) {
    
    return CGRectInset(bounds, 8, 0);
    }
    else
        return CGRectInset(bounds, txtBounds.intValue, 0);
}
- (CGRect)editingRectForBounds:(CGRect)bounds
{
    
    if (editBounds == Nil) {
        
        return CGRectInset(bounds, 25, 0);
    }
    else
        
    return CGRectInset(bounds, editBounds.intValue, 0);
}


-(void)setEditBounds:(NSString *)EditBounds
{
        editBounds = EditBounds;
}
-(NSString *)EditBounds
{
    
    return editBounds;
}

-(void)setTextBounds:(NSString *)TextBounds
{
        txtBounds = TextBounds;
}
-(NSString *)TextBounds
{
    return txtBounds;
}

-(void)setMinValue:(NSString *)MinValue
{
    strMin = MinValue;
}

-(NSString *)MinValue
{
    return strMin;
}

-(void)setBorderColor:(UIColor *)BorderColor
{
    self.layer.borderColor = BorderColor.CGColor;
    
}
-(void)setCornerRadius:(NSString *)CornerRadius
{
    
    self.layer.cornerRadius = [CornerRadius integerValue];
}


-(void)setRegex:(NSString *)regex{
    if (regex == NULL) {
        strRegex = @"NULL";
    }
    else{
        strRegex = regex;
    }
}
-(NSString *)regex
{
    return  strRegex;
}


-(void)setMaxValue:(NSString *)MaxValue
{
    
    if (MaxValue == NULL) {
        strMaxLength = @"0";
    }
    else{
        strMaxLength = MaxValue;
    }
}
-(NSString *)MaxValue
{
    return strMaxLength;
}


-(void)setDataType:(NSString *)DataType
{
    if (DataType == NULL) {
        DataType = @"character";
    }
    else{
        CustomeDatatype = DataType;
    }
    if ([DataType isEqualToString:@"character"]) {
        self.keyboardType = UIKeyboardTypeDefault;
    }
    else if([DataType isEqualToString:@"numeric"]) {
        self.keyboardType = UIKeyboardTypeNumberPad;
    }
    else if([DataType isEqualToString:@"character + punctuation"]) {
        self.keyboardType = UIKeyboardTypeDefault;
    }
}
-(NSString *)DataType
{
    return CustomeDatatype;
}


-(void)setIsValidate:(BOOL)IsValidate
{
    isvalidate = IsValidate;
}
-(BOOL)IsValidate
{
    return isvalidate;
}


-(void)setIsRequired:(BOOL)IsRequired
{
    isrequired = IsRequired;
}
-(BOOL)IsRequired
{
    return isrequired;
}

-(void)setTabIndex:(NSString *)TabIndex;
{
    if (TabIndex == NULL) {
        tabindex = @"0";
    }
    else{
        tabindex = TabIndex;
    }
}

-(NSString *)TabIndex
{
    return tabindex;
}

-(void)setErrorMessage_r:(NSString *)ErrorMessage_r
{
    Message_R = ErrorMessage_r;
}
-(NSString*)ErrorMessage_r
{
    return Message_R;
}

-(void)setErrorMessage_m:(NSString *)ErrorMessage_m
{
    Message_M = ErrorMessage_m;
}
-(NSString*)ErrorMessage_m
{
    return Message_M;
}

-(void)setErrorMessage_v:(NSString *)ErrorMessage_v
{
    Message_V = ErrorMessage_v;
}
-(NSString*)ErrorMessage_v
{
    return Message_V;
}

-(void)setClearButton:(BOOL)ClearButton
{
    if (ClearButton == YES) {
        self.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    else
        self.clearButtonMode = UITextFieldViewModeNever;
}

//   end of properties




-(IBAction)textfieldChange:(id)sender;
{
    CustomeTextField *textFildText = (CustomeTextField*)sender;
    IsValid = [self valid:textFildText.text];
    
    
    
}

-(IBAction)didEndEditingAndExit:(id)sender
{
    CustomeTextField *tagtextfield = (CustomeTextField *)sender;
    
reset:
    GlobalArray = [ArrayTextField copy];
    for ( int i = 0; i< GlobalArray.count ; i++) {
        if ([[ArrayTextField objectAtIndex:i]valueForKey:@"tabindex"] == NULL) {
            [ArrayTextField removeObjectAtIndex:i];
            goto reset;
        }
        
    }
    
    [ArrayTextField sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"tabindex" ascending:YES]]];
    
    for (int j=0; j<=ArrayTextField.count-1; j++) {
        
        
        if (tagtextfield.TabIndex.intValue == j) {
            [[ArrayTextField objectAtIndex:j] becomeFirstResponder];
        }
        
        
    }
    
}

-(IBAction)keyboardStroke:(id)sender;
{
    UITextField *txt = (UITextField*)sender;
    
    if ([txt.text isEqualToString:@""]) {
        IsValid =NO;
    }
    if (strMaxLength != nil)
    {
        
        if (txt.text.length > [strMaxLength integerValue])
        {
            NSString *str = [NSString stringWithFormat:@"%@",txt.text];
            
            str = [str substringToIndex:[strMaxLength integerValue]];
            [txt setText:str];
        }
    }
    
    // IsValid = [self valid:txt.text];
    
    
    if ([CustomeDatatype isEqualToString:@"numeric"] ||
        [CustomeDatatype isEqualToString:@"phonenumber"] || [CustomeDatatype isEqualToString:@"age"])
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
        NSString *filtered = [[txt.text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        txt.text = filtered;
    }
    
    if ([CustomeDatatype isEqualToString:@"character"]) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "] invertedSet];
        NSString *filtered = [[txt.text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        txt.text =filtered;
    }
    
    if ([CustomeDatatype isEqualToString:@"character+punctuation"]) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+|=-{}[]:>?<,./~`"] invertedSet];
        NSString *filtered = [[txt.text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        txt.text =filtered;
    }
    if ([CustomeDatatype isEqualToString:@"numeric+punctuation"]) {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@".1234567890"] invertedSet];
        NSString *filtered = [[txt.text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        txt.text =filtered;
    }
    if ([CustomeDatatype isEqualToString:@"character+numeric"]) {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1023456789 "] invertedSet];
        NSString *filtered = [[txt.text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        txt.text =filtered;
        
    }
    if ([CustomeDatatype isEqualToString:@"email"]) {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1023456789.@_"] invertedSet];
        NSString *filtered = [[txt.text componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        txt.text =filtered;
        
    }
    
    
    
    
}

-(BOOL) valid:(NSString*) emailString
{
    
    BOOL Temp = false ;
    
    if (strMin != nil)
    {
        Temp = (emailString.length >= [strMin integerValue]) ? YES:NO;
    }
    
    
    if ([CustomeDatatype isEqualToString:@"email"])
    {
        regexExpression = @"^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regexExpression options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
        Temp = regExMatches == 0 ? NO:YES;
    }
    if ([CustomeDatatype isEqualToString:@"phonenumber"]) {
        regexExpression = @"^+(?:[0-9] ?){6,14}[0-9]$";
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regexExpression options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
        Temp = regExMatches == 0 ? NO:YES;
    }
    if ([CustomeDatatype isEqualToString:@"URL"]) {
        regexExpression =  @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regexExpression options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
        Temp = regExMatches == 0 ? NO:YES;
    }
    if ([CustomeDatatype isEqualToString:@"username"]) {
        regexExpression = @"^[^0-9][a-zA-Z][a-zA-Z0-9._]{1,18}[a-zA-Z0-9]$";
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regexExpression options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
        Temp = regExMatches == 0 ? NO:YES;
    }
    
    if (strRegex != nil)
    {
        
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:strRegex options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
        Temp = regExMatches == 0 ? NO:YES;
    }
    if ([CustomeDatatype isEqualToString:@"age"])
    {
        int age = emailString.intValue;
        if (age >= 18 && age <=60)
        {
            Temp = YES;
        }
    }
    
    if ([CustomeDatatype isEqualToString:@"password"]) {
        password = emailString;
        
    }
    if ([CustomeDatatype isEqualToString:@"confirmPassword"]) {
        Temp = (emailString == password) ? YES:NO;
        
    }
    if (isrequired == YES)
    {
        
        if ([emailString isEqualToString:@""]) {
            Temp = NO;
            
        }
    }
    
    
    
    return Temp;
}

#pragma mark ButtonValidation


+(BOOL)Validate;
{
    static NSString *checkPass;
reset:
    GlobalArray = [ArrayTextField copy];
    for ( int i = 0; i< GlobalArray.count ; i++) {
        if ([[ArrayTextField objectAtIndex:i]valueForKey:@"tabindex"] == NULL) {
            [ArrayTextField removeObjectAtIndex:i];
            
            goto reset;
            
        }
    }
    [ArrayTextField sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"tabindex" ascending:YES]]];
    
    
    
    for ( int i = 0; i< ArrayTextField.count; i++) {
        
        
        
        CustomeTextField *UserTextField = [ArrayTextField objectAtIndex:i];
        
        if ([UserTextField.text length] == (unsigned)nil) {
            [self DisplayToast:UserTextField.ErrorMessage_m];
            return FALSE ;
        }
        
        
        if ([UserTextField IsRequired] == YES)
        {
            if ([UserTextField.text length] <= 0)
            {
                [self DisplayToast:UserTextField.ErrorMessage_r];
                return FALSE ;
            }
        }
        
        if ([UserTextField.text length] < [UserTextField.MinValue integerValue])
        {
            [self DisplayToast:UserTextField.ErrorMessage_m];
            return FALSE ;
        }
        
        NSString *expressionregex;
        if ([[UserTextField DataType] isEqualToString:@"email"])
        {
            expressionregex = @"^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
            NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:expressionregex options:NSRegularExpressionCaseInsensitive error:nil];
            
            NSUInteger regExMatches = [regEx numberOfMatchesInString:UserTextField.text options:0 range:NSMakeRange(0, [UserTextField.text length])];
            if(regExMatches == 0 ){
                [self DisplayToast:UserTextField.ErrorMessage_v];
                return FALSE ;
            }
        }
        if ([[UserTextField DataType] isEqualToString:@"phonenumber"]) {
            expressionregex = @"^+(?:[0-9] ?){6,14}[0-9]$";
            NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:expressionregex options:NSRegularExpressionCaseInsensitive error:nil];
            
            NSUInteger regExMatches = [regEx numberOfMatchesInString:UserTextField.text options:0 range:NSMakeRange(0, [UserTextField.text length])];
            if(regExMatches == 0 ){
                [self DisplayToast:UserTextField.ErrorMessage_v];
                return FALSE ;
            }
        }
        if ([[UserTextField DataType] isEqualToString:@"URL"]) {
            expressionregex =  @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
            NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:expressionregex options:NSRegularExpressionCaseInsensitive error:nil];
            
            NSUInteger regExMatches = [regEx numberOfMatchesInString:UserTextField.text options:0 range:NSMakeRange(0, [UserTextField.text length])];
            if(regExMatches == 0 ){
                [self DisplayToast:UserTextField.ErrorMessage_v];
                return FALSE ;
            }
        }
        if ([[UserTextField DataType] isEqualToString:@"username"]) {
            expressionregex = @"^[^0-9][a-zA-Z][a-zA-Z0-9._]{1,18}[a-zA-Z0-9]$";
            NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:expressionregex options:NSRegularExpressionCaseInsensitive error:nil];
            
            NSUInteger regExMatches = [regEx numberOfMatchesInString:UserTextField.text options:0 range:NSMakeRange(0, [UserTextField.text length])];
            if(regExMatches == 0 ){
                [self DisplayToast:UserTextField.ErrorMessage_v];
                return FALSE ;
            }
        }
        
        
        
        if ([UserTextField regex] != nil)
        {
            
            NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:[UserTextField regex] options:NSRegularExpressionCaseInsensitive error:nil];
            
            NSUInteger regExMatches = [regEx numberOfMatchesInString:UserTextField.text options:0 range:NSMakeRange(0, [UserTextField.text length])];
            
            return   regExMatches == 0 ? NO:YES;
        }
        
        
        if ([[UserTextField DataType] isEqualToString:@"password"]) {
            checkPass = UserTextField.text;
            
        }
        if ([[UserTextField DataType] isEqualToString:@"confirmPassword"]) {
            
            if (UserTextField.text != checkPass)
            {
                
                [self DisplayToast:UserTextField.ErrorMessage_v];
                return FALSE ;
            }
        }
        
        
        if ([[UserTextField DataType] isEqualToString:@"age"])
        {
            int age = UserTextField.text.intValue;
            if (age >= 18 && age <=60)
            {
                return FALSE ;
            }
        }
        
        
    }
    
    return YES ;
}

+(void)DisplayToast :(NSString *)message{
    
    //    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ERROR" message:message delegate:Nil cancelButtonTitle:@"ok" otherButtonTitles:Nil];
    //    [alert show];
    // [ alert release];
    if (message == nil) {
        message = @"nill message";
    }
    
//    [iToast displayTost:message];
    
}

+(BOOL)validate : (UITextField *)TextfieldText
{
    static NSString *checkPass;
    NSMutableArray *arrayCustomeTextfiled = [[NSMutableArray alloc]init];
    
    
    for ( int i = 0; i< ArrayTextField.count ; i++) {
        
        if ([ArrayTextField objectAtIndex:i] ==TextfieldText ) {
            [arrayCustomeTextfiled addObject:[ArrayTextField objectAtIndex:i] ];
            
        }
    }
    CustomeTextField *textfield = [arrayCustomeTextfiled objectAtIndex:0];
    if ([textfield.text length] == (unsigned)nil) {
        [self DisplayToast:textfield.ErrorMessage_m];
        return FALSE ;
    }
    
    if ([textfield IsRequired] == YES)
    {
        if ([textfield.text length] <= 0)
        {
            [self DisplayToast:textfield.ErrorMessage_r];
            return FALSE ;
        }
    }
    
    if ([textfield.text length] < [textfield.MinValue integerValue])
    {
        [self DisplayToast:textfield.ErrorMessage_m];
        return FALSE ;
        
    }

    NSString *expressionregex;
    if ([[textfield DataType] isEqualToString:@"email"])
    {
        expressionregex = @"^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:expressionregex options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:textfield.text options:0 range:NSMakeRange(0, [textfield.text length])];
        if(regExMatches == 0 ){
            [self DisplayToast:textfield.ErrorMessage_v];
            return FALSE ;
        }
    }
    if ([[textfield DataType] isEqualToString:@"phonenumber"]) {
        expressionregex = @"^+(?:[0-9] ?){6,14}[0-9]$";
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:expressionregex options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:textfield.text options:0 range:NSMakeRange(0, [textfield.text length])];
        if(regExMatches == 0 ){
            [self DisplayToast:textfield.ErrorMessage_v];
            return FALSE ;
        }
    }
    if ([[textfield DataType] isEqualToString:@"URL"]) {
        expressionregex =  @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:expressionregex options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:textfield.text options:0 range:NSMakeRange(0, [textfield.text length])];
        if(regExMatches == 0 ){
            [self DisplayToast:textfield.ErrorMessage_v];
            return FALSE ;
        }
    }
    if ([[textfield DataType] isEqualToString:@"username"]) {
        expressionregex = @"^[^0-9][a-zA-Z][a-zA-Z0-9._]{1,18}[a-zA-Z0-9]$";
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:expressionregex options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:textfield.text options:0 range:NSMakeRange(0, [textfield.text length])];
        if(regExMatches == 0 ){
            [self DisplayToast:textfield.ErrorMessage_v];
            return FALSE ;
        }
    }
    
    
    
    if ([textfield regex] != nil)
    {
        
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:[textfield regex] options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger regExMatches = [regEx numberOfMatchesInString:textfield.text options:0 range:NSMakeRange(0, [textfield.text length])];
        
        return   regExMatches == 0 ? NO:YES;
    }
    
    
        if ([[textfield DataType] isEqualToString:@"password"]) {
            checkPass = textfield.text;
    
        }
        if ([[textfield DataType] isEqualToString:@"confirmPassword"]) {
    
            if (textfield.text != checkPass)
            {
    
                [self DisplayToast:textfield.ErrorMessage_v];
                return FALSE ;
            }
        }
    
    
    if ([[textfield DataType] isEqualToString:@"age"])
    {
        int age = textfield.text.intValue;
        if (age >= 18 && age <=60)
        {
            return FALSE ;
        }
    }
    
    
    
    
    return YES ;
}






@end
BOOL IsValid;
NSString *CustomeBackGround;
NSMutableArray *GlobalArray;
NSMutableArray *ArrayTextField;
UIColor *CustomeFontColor;
UIFont *CustomeFont;
