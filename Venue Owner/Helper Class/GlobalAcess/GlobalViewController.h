//
//  [GlobalViewController.h
//  MakeUpArtist
//
//  Created by Ashesh Shah on 21/10/13.
//  Copyright (c) 2013 Ashesh Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Social/Social.h>

#import "AppDelegate.h"

#import "ValidationAndJsonViewController.h"
#import "UIView+Toast.h"
#import "MBProgressHUD.h"
#import "iToast.h"
#import "PECropViewController.h"
#import "SlideNavigationController.h"
#import "MenuViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "UIImageView+WebCache.h"

#import "NSDictionary+NullReplacement.h"
#import "NSArray+ReplaceNull.h"

#import "LoginVC.h"
#import "RegistrationVC1.h"
#import "RegistrationVC2.h"
#import "DashboardVC.h"
#import "DashboardMainTableCell.h"
#import "EventPostVC.h"
#import "CreateEventVC.h"
#import "DiningSpecialVC.h"
#import "TakePhotoVC.h"
#import "DashboardTableCell.h"
#import "EventPostTableCell.h"
#import "DiningSpecialTableCell.h"
#import "ForgotPasswordVC.h"
#import "ThemesVC.h"
#import "ThemesTableCell.h"
#import "EventDescriptionVC.h"
#import "ManageDiningSpecialVC.h"
#import "ManageDiningSpecialDishTableCell1.h"
#import "ManageDiningSpecialDishTableCell2.h"
#import "ManageDiningSpecialDateTableCell.h"
#import "DiningSpecialPostVC.h"
#import "DiningSpecialPostTableCell.h"
#import "DashboardVenueAdminListCell.h"
#import "EventLinkVC.h"
#import "EvenLinkTableCell.h"
#import "DashboardImageTableCell.h"
#import "UpcomingEventsVC.h"
#import "UpcomingEventsTableCell.h"

//#define BASEAPI @"http://levidev.goingout2.com/venueadminapi/venueadmin/"
#define BASEAPI @"https://mobiledev.goingout2.com/venueadminapi/venueadmin/"
#define IMAGEURL @"https://mobiledev.goingout2.com"

#define  FONTREGULAR  @"HelveticaNeue"


#define THIS [AppDelegate sharedInstance]

#define  LOADINGSHOW     MBProgressHUD* progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];progressHUD.labelText = @"Loading";progressHUD.mode = MBProgressHUDAnimationFade; progressHUD.tag = 10000;

#define  LOADINGHIDE     [[self.view viewWithTag:10000] removeFromSuperview];

#define  LOADINGSHOW1     LabeledActivityIndicatorView *aiv = [[LabeledActivityIndicatorView alloc]initWithController:self andText:@"Loading"];[aiv show];aiv.tag = 10000;

#define  LOADINGHIDE1     [[self.view viewWithTag:10000] removeFromSuperview];


#define SYSVERSION [[[UIDevice currentDevice]systemVersion] floatValue]

#define CurrenscreenWidth  [UIScreen mainScreen].bounds.size.width
#define CurrenscreenHeight  [UIScreen mainScreen].bounds.size.height

#define Global ValidationAndJsonViewController

#define NOINTERNET UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"There are some issues with the connection, please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];[alert show];

#define PROBLEM [Global displayTost:@"There are some issues with the connection, please try again later"];

@interface GlobalViewController : UIViewController
@end


