//
//  VlidationAndJsonViewController.h


#import <UIKit/UIKit.h>

@interface ValidationAndJsonViewController : UIViewController
{
    UIDatePicker *datepikar;
}

#pragma mark - Json

+(NSMutableArray*)parseData:(NSDictionary*)dictJson key:(NSString*)strKey d:(void(^)(NSMutableArray *arrResponse)) block;

+(NSDictionary *)ArrayJsonData:(NSString *)ParsingText strbase:(NSString *)basestr passToken:(BOOL)isPassToken;

+(NSDictionary *)getParsingDataUsingAsync:(NSDictionary *)Dict isPassToken:(BOOL)passToken GetKey:(NSString *)key d:(void(^)(NSDictionary *ResponseArray)) block;

+(NSDictionary *)requestForPostWithImage:(NSDictionary *)dictJson isPassToken:(BOOL)passToken withPhoto:(UIImageView *)imageToUpload GetKey:(NSString *)key d:(void(^)(NSDictionary *dictJson)) block;

+(NSDictionary *)requestForPostWithImage:(NSDictionary *)dictJson isPassToken:(BOOL)passToken withPhoto:(UIImageView *)imageToUpload GetKey:(NSString *)key;
+ (NSData *)httpBodyForParameters:(NSDictionary *)parameters;
+ (NSString *)percentEscapeString:(NSString *)string;

#pragma mark - Other

+(void)isNetworkRechable:(void(^)(BOOL connected))block;

+(BOOL)validateEmail:(NSString *)inputText;

+(void)displayTost:(NSString *)erroemessage;
+(void)displayTost1:(NSString *)erroemessage;

+(BOOL)validatePhone:(NSString *)candidate;

+ (CGSize)text:(NSString*)text sizeWithFont:(UIFont*)font;

+ (CGSize)text:(NSString*)text sizeWithFont:(UIFont*)font constrainedToSize:(CGSize)size;

+(NSString *)getmonthname:(NSString *)getdate dateformat:(NSString *)getdateformat dateformat2:(NSString *)getdateformat2;

+(BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate;

+(UIImage *)scaleAndRotateImage:(UIImage *)image;
+(NSString *) findUniqueSavePath;

+(NSString*)enocodeString:(NSString*)strToEncode;
+(NSString*)deocodeString:(NSString*)strToDecode;

+ (NSString*)timeAgoString:(NSString *)timestampString;

+(NSDate*)dateFromString:(NSString*)strDate strFormatter:(NSString*)strDateFormatter;
+(NSString*)stringFromDate:(NSDate*)date strFormatter:(NSString*)strDateFormatter;

+(void)checkVersionJson;

@end
