//
//  VlidationAndJsonViewController.m

#import "ValidationAndJsonViewController.h"
#import "GlobalViewController.h"

@interface ValidationAndJsonViewController ()

@end

@implementation ValidationAndJsonViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark - Network Reachable

+(void)isNetworkRechable:(void(^)(BOOL connected))block
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         BOOL con = NO;
         NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
         
         if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
             
             con = YES;
         }
         
         if (block) {
             [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
             block(con);
         }
         
     }];
}

#pragma mark - JSon Parsing

+(NSDictionary *)requestForPostWithImage:(NSDictionary *)dictJson isPassToken:(BOOL)passToken withPhoto:(UIImageView *)imageToUpload GetKey:(NSString *)key d:(void(^)(NSDictionary *dictJson)) block
{
    NSString *url = [NSString stringWithFormat:@"%@%@",BASEAPI,key];
    
    NSLog(@"Request = %@%@",url,dictJson);
    
    __block NSDictionary *jsonDict;
    
    [Global isNetworkRechable:^(BOOL connected) {
        if (connected)
        {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            
            if (passToken == true) {
                
                [manager.requestSerializer setValue:[THIS.dictUserDetail valueForKey:@"KEY"] forHTTPHeaderField:@"KEY"];
                [manager.requestSerializer setValue:[THIS.dictUserDetail valueForKey:@"TOKEN"] forHTTPHeaderField:@"TOKEN"];
            }
            
            AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
            [securityPolicy setValidatesDomainName:NO];
            [securityPolicy setAllowInvalidCertificates:YES];
            manager.securityPolicy = securityPolicy;

            [manager POST:url parameters:dictJson constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
             {
                 if(imageToUpload.image){
                     UIImage *imgUser = [Global scaleAndRotateImage:imageToUpload.image];
                     NSData *imageData = UIImageJPEGRepresentation(imgUser, 0.3);
                     
                     NSString *str_time = [Global findUniqueSavePath];
                     
                     [formData appendPartWithFileData:imageData name:@"photo" fileName:str_time mimeType:@"image/png"];
                 }
             }
                  success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 NSError *err;
                 jsonDict = [NSJSONSerialization JSONObjectWithData:[operation responseData] options: NSJSONReadingMutableContainers error: &err];
                 jsonDict = [jsonDict dictionaryByReplacingNullsWithBlanks];
                 NSLog(@"Response = %@", jsonDict);
                 block(jsonDict);
             }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 NSLog(@"Error %@",error);
                 NSLog(@"HTTP Processing error: %@", error.localizedDescription); //this error is triggered
                 NSLog(@"HTTP Processing error: %@", error.debugDescription);
                 block(nil);
             }];
        }
        else
        {
            jsonDict = [[NSMutableDictionary alloc]init];
            block(jsonDict);
            NOINTERNET
        }
    }];
    
    return jsonDict;
}

+(NSDictionary *)getParsingDataUsingAsync:(NSDictionary *)dictJson isPassToken:(BOOL)passToken GetKey:(NSString *)key d:(void(^)(NSDictionary *ResponseArray)) block
{
    NSString *url = [NSString stringWithFormat:@"%@%@",BASEAPI,key];
    
    NSLog(@"Request = %@%@",url,dictJson);
    
    __block NSDictionary *jsonDict;
    
    [Global isNetworkRechable:^(BOOL connected) {
        if (connected)
        {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
            
            if (passToken == true) {
                
                [manager.requestSerializer setValue:[THIS.dictUserDetail valueForKey:@"KEY"] forHTTPHeaderField:@"KEY"];
                [manager.requestSerializer setValue:[THIS.dictUserDetail valueForKey:@"TOKEN"] forHTTPHeaderField:@"TOKEN"];
            }
            
            AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
            [securityPolicy setValidatesDomainName:NO];
            [securityPolicy setAllowInvalidCertificates:YES];
            manager.securityPolicy = securityPolicy;
            
            [manager POST:url parameters:dictJson success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 NSError *err;
                 jsonDict = [NSJSONSerialization JSONObjectWithData:[operation responseData] options: NSJSONReadingMutableContainers error: &err];
                 jsonDict = [jsonDict dictionaryByReplacingNullsWithBlanks];
                 NSLog(@"Response = %@", jsonDict);
                 block(jsonDict);
             }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 NSLog(@"Error %@",error);
                 NSLog(@"HTTP Processing error: %@", error.localizedDescription); //this error is triggered
                 NSLog(@"HTTP Processing error: %@", error.debugDescription);
                 block(nil);
             }];
        }
        else
        {
            jsonDict = [[NSMutableDictionary alloc]init];
            block(jsonDict);
            NOINTERNET
        }
    }];
    
    return jsonDict;
}

+(NSDictionary *)ArrayJsonData:(NSString *)ParsingText strbase:(NSString *)basestr passToken:(BOOL)isPassToken
{
    if ([AppDelegate CheckNetworkReachability])
    {
        NSLog(@"requestString = %@%@%@",BASEAPI,basestr,ParsingText);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",BASEAPI,basestr,ParsingText]]];
        
        request.timeoutInterval=240.0;
        [request setHTTPMethod: @"POST"];
        [request setHTTPBody: nil];
        
        if (isPassToken) {
            
            [request addValue:[THIS.dictUserDetail valueForKey:@"KEY"] forHTTPHeaderField: @"KEY"];
            [request addValue:[THIS.dictUserDetail valueForKey:@"TOKEN"] forHTTPHeaderField: @"TOKEN"];
        }
        
        NSError *err = nil;
        NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&err ];
        
        if(!err)
        {
            NSString *resultString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
            
            NSData *jsonData = [resultString dataUsingEncoding:NSUTF8StringEncoding];
            
            NSLog(@"Response: %@", resultString);
            
            NSDictionary *dictResult = [NSJSONSerialization JSONObjectWithData:jsonData options: NSJSONReadingMutableContainers error: &err];
            
            if ([dictResult isKindOfClass:[NSDictionary class]])
            {
                dictResult = [dictResult dictionaryByReplacingNullsWithBlanks];
            }
            
            NSLog(@"Response: %@", dictResult);
            
            if(dictResult != nil){
                return dictResult;
            }
        }else{
            NSLog(@"Error = %@",err);
        }
    }
    else
    {
        NOINTERNET
    }
    
    return nil;
}

+(NSDictionary *)requestForPostWithImage:(NSDictionary *)dictJson isPassToken:(BOOL)passToken withPhoto:(UIImageView *)imageToUpload GetKey:(NSString *)key
{
    if ([AppDelegate CheckNetworkReachability])
    {
        UIImage *imgUser = [Global scaleAndRotateImage:imageToUpload.image];
        NSData *imageData = UIImagePNGRepresentation(imgUser);
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",BASEAPI,key];
        
        NSLog(@"Request = %@%@",urlString,dictJson);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[Global httpBodyForParameters:dictJson]];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
//        NSString *contentType = [NSString stringWithFormat:@"application/x-www-form-urlencoded; boundary=%@",boundary];
//        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:[THIS.dictUserDetail valueForKey:@"KEY"] forHTTPHeaderField: @"KEY"];
        [request addValue:[THIS.dictUserDetail valueForKey:@"TOKEN"] forHTTPHeaderField: @"TOKEN"];
        
        if (imageToUpload.image) {
            NSString *str_time = [Global findUniqueSavePath];
            
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\"%@\"\r\n", str_time]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [request setHTTPBody:body];
        }
        
        NSError *err = nil;
        NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&err ];
        
        if(!err)
        {
            NSString *resultString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
            
            NSData *jsonData = [resultString dataUsingEncoding:NSUTF8StringEncoding];
            
            NSLog(@"Response: %@", resultString);
            
            NSDictionary *dictResult = [NSJSONSerialization JSONObjectWithData:jsonData options: NSJSONReadingMutableContainers error: &err];
            
            if ([dictResult isKindOfClass:[NSDictionary class]])
            {
                dictResult = [dictResult dictionaryByReplacingNullsWithBlanks];
            }
            
            NSLog(@"Response: %@", dictResult);
            
            if(dictResult != nil){
                return dictResult;
            }
        }else{
            NSLog(@"Error = %@",err);
        }
    }
    else
    {
        NOINTERNET
    }
    
    return nil;
}

+ (NSData *)httpBodyForParameters:(NSDictionary *)parameters {
    NSMutableArray *parameterArray = [NSMutableArray array];
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"%@=%@", [self percentEscapeString:key], [self percentEscapeString:obj]];
        [parameterArray addObject:param];
    }];
    
    NSString *string = [parameterArray componentsJoinedByString:@"&"];
    
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *)percentEscapeString:(NSString *)string {
    NSCharacterSet *allowed = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~"];
    return [string stringByAddingPercentEncodingWithAllowedCharacters:allowed];
}

#pragma mark - Toast

+(void)displayTost:(NSString*)erroemessage
{
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageFont = [UIFont systemFontOfSize:14.0];//fontWithName:@"Zapfino" size:14.0];
    style.messageColor = [UIColor whiteColor];
    style.messageAlignment = NSTextAlignmentCenter;
    style.titleAlignment = NSTextAlignmentCenter;
    style.backgroundColor = [UIColor colorWithRed:78.0/255.0 green:158.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    [THIS.window makeToast:erroemessage duration:2.5 position:CSToastPositionBottom style:style];
}

+(void)displayTost1:(NSString*)erroemessage
{
    iToast  *iT = [iToast makeText:erroemessage];
    [iT setGravity:iToastGravityCenter];
    [iT show];
}

#pragma mark - Validation

+(BOOL)validateEmail:(NSString *)inputText
{
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        NSInteger indexOfDot = aRange.location;
        //NSLog(@"aRange.location:%d - %d",aRange.location, indexOfDot);
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            //NSLog(@"topleveldomains:%@",topLevelDomain);
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
            /*else {
             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
             }*/
            
        }
    }
    return FALSE;
}

+ (BOOL) validatePhone: (NSString *) candidate {
    NSString *phoneRegex = @"^+(?:[0-9] ?){6,14}[0-9]$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:candidate];
}




#pragma mark - compare date

+(BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate
{
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}

#pragma mark -Compress image Method


#pragma mark - Label Size calculation method

+ (CGSize)text:(NSString*)text sizeWithFont:(UIFont*)font
{
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName: font}];
    return CGSizeMake(ceilf(size.width), ceilf(size.height));
}

+ (CGSize)text:(NSString*)text sizeWithFont:(UIFont*)font constrainedToSize:(CGSize)size
{
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping; //e.g.
    
    CGRect textRect = [text boundingRectWithSize:size
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraph}
                                         context:nil];
    return CGSizeMake(ceilf(textRect.size.width), ceilf(textRect.size.height));
}

#pragma mark - ImageView

+(NSString *) findUniqueSavePath
{
    NSString *path;
    NSDateFormatter *df=[[NSDateFormatter alloc] init];
//    df.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [df setDateFormat:@"ddMMyyyhhmmss"];
    path=[NSString stringWithFormat:@"%@.png",[df stringFromDate:[NSDate date]]];
    
    return path;
}

+(UIImage *)scaleAndRotateImage:(UIImage *)image
{
    static int kMaxResolution = 350;
    
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        } else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
        case UIImageOrientationUp:
            transform = CGAffineTransformIdentity;
            break;
        case UIImageOrientationUpMirrored:
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
        case UIImageOrientationDown:
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
        case UIImageOrientationLeftMirrored:
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
        case UIImageOrientationLeft:
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
        case UIImageOrientationRightMirrored:
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
        case UIImageOrientationRight:
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
    }
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    } else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    CGContextConcatCTM(context, transform);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

#pragma mark - EncodeDecode

+(NSString*)enocodeString:(NSString*)strToEncode
{
    NSData *plainData = [strToEncode dataUsingEncoding:NSUTF8StringEncoding];
    NSString *strBase64Encode = [plainData base64EncodedStringWithOptions:0];
    
    return strBase64Encode;
}

+(NSString*)deocodeString:(NSString*)strToDecode
{
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[NSString stringWithFormat:@"%@",strToDecode] options:0];
    NSString *strBase64Decoded = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    
    return strBase64Decoded;
}

+ (NSString*)timeAgoString:(NSString *)timestampString
{
    NSInteger k = 60*[timestampString integerValue];
    
    NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:-k];
    
    NSCalendar *gregorian =
    [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    int unitFlags = NSCalendarUnitYear |
    NSCalendarUnitMonth |
    NSCalendarUnitDay |
    NSCalendarUnitHour |
    NSCalendarUnitMinute |
    NSCalendarUnitSecond;
    NSDateComponents *comps =
    [gregorian components:unitFlags
                 fromDate:newDate toDate:[NSDate date] options:0];
    
    NSString *timeAgoString = [NSString string];
    if ([comps year] > 0) {
        timeAgoString = [NSString stringWithFormat:@"%li years ago", (long)[comps year]];
    }
    else if ([comps month] > 0) {
        timeAgoString = [NSString stringWithFormat:@"%li month ago", (long)[comps month]];
    }
    else if ([comps day] > 0) {
        timeAgoString = [NSString stringWithFormat:@"%li days ago", (long)[comps day]];
    }
    else if ([comps hour] > 0) {
        timeAgoString = [NSString stringWithFormat:@"%li hours ago", (long)[comps hour]];
    }
    else if ([comps minute] > 0) {
        timeAgoString = [NSString stringWithFormat:@"%li mins ago", (long)[comps minute]];
    }
    else if ([comps second] > 0) {
        timeAgoString = [NSString stringWithFormat:@"%li secs ago", (long)[comps second]];
    }
    else
    {
        timeAgoString = [NSString stringWithFormat:@"Just secs ago"];
    }
    
    return timeAgoString;
}

#pragma mark - SringToDate

+(NSString *)getmonthname:(NSString *)getdate dateformat:(NSString *)getdateformat dateformat2:(NSString *)getdateformat2
{
    if ([getdate isEqualToString:@""]) {
        return @"";
    }
    NSDateFormatter *df=[[NSDateFormatter alloc] init];
    [df setDateFormat:getdateformat];
    
    NSDate *date = [df dateFromString:getdate];
    [df setDateFormat:getdateformat2];
    
    NSString *dateString = [df stringFromDate:date];
    return dateString;
}

+(NSDate*)dateFromString:(NSString*)strDate strFormatter:(NSString*)strDateFormatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:strDateFormatter];
    
    NSDate *date = [dateFormatter dateFromString:strDate];
    
    return date;
}

+(NSString*)stringFromDate:(NSDate*)date strFormatter:(NSString*)strDateFormatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:strDateFormatter];
    
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    return strDate;
}

@end
