//
//  CustomeTextField.h
//  DesinableTextField
//
//  Created by asheshshah on 24/12/15.
//  Copyright © 2015 Asheshshah. All rights reserved.
//

#import <UIKit/UIKit.h>




//@protocol DelegateTextField <NSObject>
//
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO
//- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;          // return YES
//- (void)textFieldDidEndEditing:(UITextField *)textField;             // may be c
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
//
//- (BOOL)textFieldShouldClear:(UITextField *)textField;               // called when clear button pressed. return NO to ignore (no notifications)
//- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
//
//
//@end

extern BOOL IsValid;
extern NSString *CustomeBackGround;
extern UIFont *CustomeFont;
extern UIColor *CustomeFontColor;
extern NSString *CustomeFontType;
extern NSMutableArray *GlobalArray;
extern NSMutableArray *ArrayTextField;

@interface CustomeTextField : UITextField<UITextFieldDelegate>
{
    UITextField *textfield;
    NSString *strMaxLength;
    NSString *strRegex;
    BOOL isrequired;
    NSString *CustomeDatatype;
    BOOL isvalidate;
    NSString *tabindex;
    NSString *strMin;
    NSString *regexExpression;
    NSString *Message_R;
    NSString *Message_V;
    NSString *Message_M;
    NSString *txtBounds;
    NSString *editBounds;
    
   
}


@property (copy,nonatomic)IBInspectable NSString *TextBounds ;
@property (nonatomic)IBInspectable NSString *EditBounds;

@property (copy,nonatomic)IBInspectable NSString *MinValue;
@property (nonatomic)IBInspectable NSString *ErrorMessage_m;
@property (copy,nonatomic,readwrite)IBInspectable NSString *MaxValue;



@property (copy,nonatomic,readwrite)IBInspectable NSString *regex;
@property (nonatomic)IBInspectable NSString *ErrorMessage_v;

@property (copy,nonatomic)IBInspectable NSString *CornerRadius;
@property (copy,nonatomic)IBInspectable NSString* DataType;



@property (nonatomic,readwrite)IBInspectable BOOL IsRequired;
@property (nonatomic)IBInspectable NSString *ErrorMessage_r;

@property (nonatomic)IBInspectable NSString *TabIndex ;

@property (nonatomic,readwrite)IBInspectable BOOL ClearButton;


-(IBAction)keyboardStroke:(id)sender;
+(BOOL)Validate;
-(void)setIsRequired:(BOOL)IsRequired;
-(BOOL)IsRequired;
-(IBAction)textfieldChange:(id)sender;
+(BOOL)validate : (UITextField *)TextfieldText;

@end
