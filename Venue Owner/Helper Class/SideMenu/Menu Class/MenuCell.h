//
//  MenuCell.h
//  RootRupe
//
//  Created by Apple on 11/07/14.
//  Copyright (c) 2014 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (nonatomic,retain)IBOutlet UIImageView *imgBackground;
@property (nonatomic,retain)IBOutlet UIImageView *imgMenu;
@property (nonatomic,retain)IBOutlet UILabel *titleLabel;

@end
