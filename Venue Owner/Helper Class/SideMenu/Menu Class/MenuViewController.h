//
//  MenuViewController.h
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalViewController.h"
#import "SlideNavigationController.h"
#import "MenuCell.h"
#import <StoreKit/StoreKit.h>

@interface MenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate>
{
    NSMutableArray *arrMenuList,*arrMenuImages;
    
    IBOutlet UIImageView *imgProfilePic;
    IBOutlet UILabel *lblUserName;
    
    NSInteger intSelectedIndex;
}

-(IBAction)btnProfileAction:(id)sender;

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tblMenuList;

@end
