//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "MenuViewController.h"
#import "GlobalViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"


@interface MenuViewController ()
{
    BOOL isOpen;
}

@end


@implementation MenuViewController
@synthesize cellIdentifier;


-(void)viewDidLoad
{
    [super viewDidLoad];
    
    arrMenuList = [[NSMutableArray alloc]initWithObjects:@"Dashboard", @"My Profile", @"Manage Events", @"Manage Dining Specials", @"Logout", @"GO Admin", nil];
    
    arrMenuImages = [[NSMutableArray alloc] initWithObjects:@"dasbord_icon", @"profile_icon", @"event_icon", @"dinig_icon", @"logout_icon", @"admin_icon", nil];
    
    [imgProfilePic.layer setCornerRadius:imgProfilePic.frame.size.height/2];
    [imgProfilePic setClipsToBounds:YES];
    
    [self reloadTable];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTable) name:@"reloadTable" object:nil];
    
    intSelectedIndex = 0;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

#pragma mark - Notification

-(void)reloadTable
{
    intSelectedIndex = THIS.intSelectedMenuIndex;
    [self.tblMenuList reloadData];
}

#pragma mark - Button TouchUp

-(IBAction)btnProfileAction:(id)sender
{
   
}

#pragma mark - UITableView Datasrouce

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrMenuList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuCell";
    
    MenuCell *cell = (MenuCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    cell.titleLabel.text = [NSString stringWithFormat:@"%@",[arrMenuList objectAtIndex:indexPath.row]];
    [cell.imgMenu setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrMenuImages objectAtIndex:indexPath.row]]]];
    
    if (intSelectedIndex == indexPath.row)
    {
        cell.imgBackground.hidden = false;
    }
    else
    {
        cell.imgBackground.hidden = true;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    intSelectedIndex = indexPath.row;
    
    [_tblMenuList reloadData];
    
    UIViewController *vc;
    
    UIViewController *parentViewController = [[[SlideNavigationController sharedInstance]viewControllers]lastObject];
    Class parentVCClass = [parentViewController class];
    NSString *className = NSStringFromClass(parentVCClass);
    
    switch (indexPath.row)
    {
        case 0:
        {
            if (![className isEqualToString:@"DashboardVC"])
            {
                vc = [[DashboardVC alloc]initWithNibName:@"DashboardVC" bundle:nil];
                [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
            }
            else
            {
                [[SlideNavigationController sharedInstance]closeMenuWithCompletion:nil];
            }
        }
            break;
            
        case 1:
        {
            if (![className isEqualToString:@"ContactView"])
            {
                
            }
            else
            {
                [[SlideNavigationController sharedInstance]closeMenuWithCompletion:nil];
            }
        }
            break;
            
        case 2:
        {
            if (![className isEqualToString:@"UpcomingEventsVC"])
            {
                vc = [[UpcomingEventsVC alloc]initWithNibName:@"UpcomingEventsVC" bundle:nil];
                [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
            }
            else
            {
                [[SlideNavigationController sharedInstance]closeMenuWithCompletion:nil];
            }
        }
            break;
            
        case 3:
        {
            if (![className isEqualToString:@"ManageDiningSpecialVC"])
            {
                vc = [[ManageDiningSpecialVC alloc]initWithNibName:@"ManageDiningSpecialVC" bundle:nil];
                [[SlideNavigationController sharedInstance] pushViewController:vc animated:NO];
            }
            else
            {
                [[SlideNavigationController sharedInstance]closeMenuWithCompletion:nil];
            }
        }
            break;
            
        case 4:
        {
            [[SlideNavigationController sharedInstance]closeMenuWithCompletion:nil];
            [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
            
            THIS.dictUserDetail = [[NSMutableDictionary alloc] init];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserDetail"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
            break;
            
    }
    
}

@end
