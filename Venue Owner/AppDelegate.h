//
//  AppDelegate.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 21/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong,nonatomic) UINavigationController *nav;

@property (strong, nonatomic) NSMutableDictionary *dictUserDetail;
@property (strong, nonatomic) NSMutableDictionary *dictCurrentDay;

@property (nonatomic, assign) NSInteger intSelectedMenuIndex;
@property (nonatomic, assign) NSInteger intSelectedVenueIndex;

+(AppDelegate*)sharedInstance;

#pragma mark - Network Reachability

+(BOOL)CheckNetworkReachability;

@end

