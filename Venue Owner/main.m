//
//  main.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 21/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
