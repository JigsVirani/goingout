//
//  DashboardImageTableCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 27/06/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardImageTableCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *imgDashboard;
@property (nonatomic, retain) IBOutlet UIButton *btnDashboard;

@end
