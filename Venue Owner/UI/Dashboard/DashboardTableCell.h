//
//  DashboardTableCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 23/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardTableCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UIImageView *imgCheckBox;

@property (nonatomic,retain) IBOutlet UILabel *lblTitle1;
@property (nonatomic,retain) IBOutlet UIButton *btnTitle2;

@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constButtonTitleHeight;

@property (nonatomic,retain) IBOutlet UIImageView *imgBG;

@end
