//
//  DashboardVenueAdminListCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 31/05/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardVenueAdminListCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lblTitle;

@end
