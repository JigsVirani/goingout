//
//  DashboardMainTableCell.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 27/05/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import "DashboardMainTableCell.h"

@implementation DashboardMainTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
