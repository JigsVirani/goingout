//
//  DashboardVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 22/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardVC : UIViewController 
{
    IBOutlet UITableView *tblFullDayInfo;
    
    IBOutlet UIView *viewShare;
    
    IBOutlet UIView *viewImageFullScreenMode;
    IBOutlet UIImageView *imgFullScreenMode;
    
    NSMutableDictionary *dictCurrentDay;
    
    BOOL isSelectedCigarNight;
    
    UIButton *btnStatsticsBar;
    
    NSInteger intSelectedVenueAdmin;
    
    BOOL isOpenVenuAdminDropDown;
    
    NSMutableArray *arrSelectedList;
    NSMutableArray *arrSelectedListText;
    
    NSString *strShareText;
}

-(IBAction)btnSharingAction:(id)sender;

-(IBAction)btnStatsBarAction:(id)sender;

-(IBAction)btnFooterAction:(id)sender;

@end
