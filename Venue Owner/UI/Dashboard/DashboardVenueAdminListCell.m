//
//  DashboardVenueAdminListCell.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 31/05/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import "DashboardVenueAdminListCell.h"

@implementation DashboardVenueAdminListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
