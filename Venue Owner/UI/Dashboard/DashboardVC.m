//
//  DashboardVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 22/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "DashboardVC.h"
#import "GlobalViewController.h"

@interface DashboardVC () <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, UIActionSheetDelegate>

@end

@implementation DashboardVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isSelectedCigarNight = false;
    isOpenVenuAdminDropDown = false;
    intSelectedVenueAdmin = 0;
    
    arrSelectedList = [[NSMutableArray alloc] init];
    
    strShareText = @"";
    
    [viewImageFullScreenMode setFrame:CGRectMake(0, 0, CurrenscreenWidth, CurrenscreenHeight)];
    [self.view addSubview:viewImageFullScreenMode];
    viewImageFullScreenMode.hidden = true;
    
    UILongPressGestureRecognizer *lpHandler = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleHoldGesture:)];
    lpHandler.minimumPressDuration = 1; //seconds
    lpHandler.delegate = self;
    [imgFullScreenMode addGestureRecognizer:lpHandler];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:true];
    
    THIS.intSelectedMenuIndex = 0;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable" object:nil];
    
    if (dictCurrentDay == nil) {
        
        LOADINGSHOW
    }
    [self performSelector:@selector(currentDayJson) withObject:nil afterDelay:0.1f];
    
    tblFullDayInfo.rowHeight = UITableViewAutomaticDimension;
    tblFullDayInfo.estimatedRowHeight = 1030;
}

#pragma mark - Web Service Call

-(void)currentDayJson
{
    NSString *strVenueId = @"";
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        strVenueId = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:intSelectedVenueAdmin] valueForKey:@"id"];
    }
    
    NSDictionary *dictJson = [Global ArrayJsonData:[NSString stringWithFormat:@"venue_id=%@",strVenueId] strbase:@"GetCurrentDayInfoForVenue?" passToken:true];
    
    LOADINGHIDE
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            dictCurrentDay = [[NSMutableDictionary alloc] initWithDictionary:[dictJson valueForKey:@"data"]];
            [self setData];
            [tblFullDayInfo reloadData];
            [tblFullDayInfo setNeedsLayout];
            
            THIS.dictCurrentDay = [[NSMutableDictionary alloc] initWithDictionary:dictCurrentDay];
        }
        else if ([dictJson valueForKey:@"message"])
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }
    else
    {
        PROBLEM
    }
}

-(void)getPhotoJson
{
    NSString *strVenueId = @"";
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        strVenueId = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:intSelectedVenueAdmin] valueForKey:@"id"];
    }
    
    NSDictionary *dictJson = [Global ArrayJsonData:[NSString stringWithFormat:@"/getphoto?id=52122&size=logo"] strbase:@"" passToken:true];
    
    LOADINGHIDE
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            
        }
        else if ([dictJson valueForKey:@"message"])
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }
    else
    {
        PROBLEM
    }
}

#pragma mark - Srt Data

-(void) setData
{
    
}

#pragma mark - Button TouchUp

-(IBAction)btnMenuAction:(id)sender
{
    [[SlideNavigationController sharedInstance] openMenu:MenuLeft withCompletion:nil];
}

-(IBAction)btnSharingAction:(id)sender
{
    if ([sender tag] == 1)
    {
        LOADINGSHOW
        [self performSelector:@selector(fbShare) withObject:nil afterDelay:0.1f];
    }
    else if ([sender tag] == 2)
    {
        LOADINGSHOW
        [self performSelector:@selector(twitterShare) withObject:nil afterDelay:0.1f];
    }
    [viewShare setHidden:YES];
}

-(IBAction)btnStatsBarAction:(id)sender
{
    btnStatsticsBar = sender;
    
    [tblFullDayInfo reloadData];
}

-(IBAction)btnFooterAction:(id)sender
{
    if ([sender tag] == 1)
    {
        EventPostVC *eventPost = [[EventPostVC alloc] initWithNibName:@"EventPostVC" bundle:nil];
        [self.navigationController pushViewController:eventPost animated:YES];
    }
    else if([sender tag] == 2)
    {
        CreateEventVC *createEvent = [[CreateEventVC alloc] initWithNibName:@"CreateEventVC" bundle:nil];
        [self.navigationController pushViewController:createEvent animated:YES];
    }
    else if([sender tag] == 3)
    {
        DiningSpecialVC *diningSpecial = [[DiningSpecialVC alloc] initWithNibName:@"DiningSpecialVC" bundle:nil];
        [self.navigationController pushViewController:diningSpecial animated:YES];
    }
    else if ([sender tag] == 4)
    {
        TakePhotoVC *takePhoto = [[TakePhotoVC alloc] initWithNibName:@"TakePhotoVC" bundle:nil];
        [self.navigationController pushViewController:takePhoto animated:YES];
    }
}

-(IBAction)btnAnnouncementAction:(id)sender
{
    if (isSelectedCigarNight == true) {
        isSelectedCigarNight = false;
    }else{
        isSelectedCigarNight = true;
        
        [Global displayTost1:@"Copied"];
    }
    
    [tblFullDayInfo reloadData];
    
    if (isSelectedCigarNight == true) {
        [viewShare setHidden:false];
        
        NSString *strAnnouncementText = [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"body"];
        if (![strAnnouncementText isEqualToString:@""]) {
            
            strAnnouncementText = [strAnnouncementText stringByAppendingString:@"\n"];
        }
        strShareText = [strAnnouncementText stringByAppendingString:strShareText];
    
    }else if (arrSelectedList.count != 0) {
        [viewShare setHidden:false];
    }else{
        [viewShare setHidden:true];
    }
    
    [tblFullDayInfo reloadData];
}

-(IBAction)btnOpenVenuAdminDropDownAction:(id)sender{
    
    if (isOpenVenuAdminDropDown == true) {
        isOpenVenuAdminDropDown = false;
    }else{
        isOpenVenuAdminDropDown = true;
    }
    
    [tblFullDayInfo reloadData];
}

-(IBAction)btnOpenURLAction:(UIButton*)sender{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sender.titleLabel.text] options:@{} completionHandler:nil];
    
//    if #available(iOS 10.0, *) {
//        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
//    } else {
//        UIApplication.shared.openURL(url!)
//    }
//    [[UIApplication sharedApplication] openuURL:[NSURL URLWithString:sender.titleLabel.text]];
}

-(IBAction)btnOpenPhoto:(UIButton*)sender{
    viewImageFullScreenMode.hidden = false;
    
    imgFullScreenMode.image = nil;
    
    [imgFullScreenMode sd_setImageWithURL:[NSURL URLWithString:sender.accessibilityIdentifier]];
    imgFullScreenMode.accessibilityIdentifier = sender.accessibilityIdentifier;
}

-(IBAction)btnClosePhoto:(UIButton*)sender{
    viewImageFullScreenMode.hidden = true;
}

#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tblFullDayInfo) {
        if (dictCurrentDay != nil) {
            return 1;
        }
        return 0;
    }else if (tableView.tag == 1001){
        return [[THIS.dictUserDetail valueForKey:@"venues"] count];
    }else if (tableView.tag == 1002){
        
        //Dashboard Image
        BOOL isNeedTocheckForPhoto = true;
        
        if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] isKindOfClass:[NSDictionary class]] && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"body"]) {
            
            if (![[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"photo_id"] isEqualToString:@""]) {
                
                return 1;
            }
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] count] != 0){
            
            NSMutableArray *arrEvents = [[NSMutableArray alloc] initWithArray:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"]];
            
            /*NSMutableArray *arrTemp = [[NSMutableArray alloc] init];
            for (int i=0; i < [arrEvents count]; i++) {
                
                if ([[Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"] isEqualToString:[Global stringFromDate:[NSDate date] strFormatter:@"yyyyMMdd"]]) {
                
                    [arrTemp addObject:[arrEvents objectAtIndex:i]];
                }
            }
            
            if (arrTemp.count != 0) {
                return arrTemp.count;
            }*/
            
            int intNearest = 0;
            for (int i=1; i < [arrEvents count]; i++) {
                
                NSString *strFirstDate = [Global getmonthname:[[arrEvents objectAtIndex:intNearest] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                NSString *strSecondDate = [Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                
                if ([strFirstDate integerValue] < [strSecondDate integerValue]) {
                    
                    intNearest = i;
                }
            }
            
            if (![[[arrEvents objectAtIndex:intNearest] valueForKey:@"photo_id"] isEqualToString:@""]) {
                
                return 1;
            }
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] count] != 0){
            
            if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] objectAtIndex:0] valueForKey:@"photo_id"] isEqualToString:@""]) {
                
                return 1;
            }
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] count] != 0){
            
            int intReturnCount = 0;
            for (int i=0; i < [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] count]; i++) {
                
                if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] objectAtIndex:i] valueForKey:@"photo_id"] isEqualToString:@""]){
                    intReturnCount = intReturnCount + 1;
                    
                    isNeedTocheckForPhoto = false;
                }
            }
            return intReturnCount;
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] count] != 0){
            
            int intReturnCount = 0;
            for (int i=0; i < [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] count]; i++) {
                
                if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] objectAtIndex:i] valueForKey:@"photo_id"] isEqualToString:@""]){
                    intReturnCount = intReturnCount + 1;
                    
                    isNeedTocheckForPhoto = false;
                }
            }
            return intReturnCount;
        }
        
        if (isNeedTocheckForPhoto == true) {
            return 0;
        }
    }else if (tableView.tag == 1000) {
        
        if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] count] != 0){
            
            NSMutableArray *arrEvents = [[NSMutableArray alloc] initWithArray:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"]];
            
            NSMutableArray *arrTemp = [[NSMutableArray alloc] init];
            
             for (int i=0; i < [arrEvents count]; i++) {
             
                 if ([[Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"] isEqualToString:[Global stringFromDate:[NSDate date] strFormatter:@"yyyyMMdd"]]) {
                 
                     [arrTemp addObject:[arrEvents objectAtIndex:i]];
                 }
             }
             
             if (arrTemp.count != 0) {
                 return arrTemp.count + 3;
             }
        }
        return 4;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tblFullDayInfo) {
        
        static NSString *CellIdentifier = @"DashboardMainTableCell";
        DashboardMainTableCell *cell;
        
        if (cell == nil)
        {
            cell = [(DashboardMainTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblTodaysDate.text = [NSString stringWithFormat:@"Today, %@",[Global stringFromDate:[NSDate date] strFormatter:@"MMMM dd yyyy"]];
        
        if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] isKindOfClass:[NSDictionary class]]) {
            cell.lblAtmosphere.text = [NSString stringWithFormat:@"%@ %.0f", [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] valueForKey:@"weather"], [[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] valueForKey:@"temp_f"] floatValue]];
            cell.lblWeatherDescription.text = @"";//[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] valueForKey:@"wind_string"];
            [cell.imgWeatherIcon sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] valueForKey:@"icon_url"]]]];
        }
        
        if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
            
            [cell.btnVenueAdminName addTarget:self action:@selector(btnOpenVenuAdminDropDownAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lblVenueAdminName.text = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:intSelectedVenueAdmin] valueForKey:@"name"];
            
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"logo_id"] isEqualToString:@""]){
                
                [cell.imgVenueAdmin sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@&size=logo", IMAGEURL, [[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"logo_id"]]]];
                
                NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc]initWithDictionary:[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:intSelectedVenueAdmin]];
                [dictTemp setValue:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"logo_id"] forKey:@"imageVenue"];
                NSMutableArray * arrTemp = [[NSMutableArray alloc]initWithArray:[THIS.dictUserDetail valueForKey:@"venues"]];
                [arrTemp replaceObjectAtIndex:intSelectedVenueAdmin withObject:dictTemp];
                [THIS.dictUserDetail setValue:arrTemp forKey:@"venues"];
                
                NSLog(@"%@",[THIS.dictUserDetail valueForKey:@"venues"]);
            }
            
            if (isOpenVenuAdminDropDown == true) {
                
                cell.viewVenueAdminList.hidden = false;
            }else{
                cell.viewVenueAdminList.hidden = true;
            }
            
            cell.viewVenueAdminList.layer.cornerRadius = 5.0;
            cell.viewVenueAdminList.layer.borderColor = [[UIColor blackColor] CGColor];
            cell.viewVenueAdminList.layer.borderWidth = 1.0;
            
            cell.tblVenueAdminList.tag = 1001;
            cell.tblVenueAdminList.delegate = self;
            cell.tblVenueAdminList.dataSource = self;
            
            [cell.tblVenueAdminList reloadData];
        }else{
            cell.viewVenueAdminList.hidden = false;
        }
        
        [cell.btnAnnouncement addTarget:self action:@selector(btnAnnouncementAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if (isSelectedCigarNight == false)
        {
            [cell.btnAnnouncement setSelected:FALSE];
    
            [cell.imgAnnouncementCopy setImage:[UIImage imageNamed:@"page-1.png"]];
            [cell.btnAnnouncement setBackgroundColor:[UIColor clearColor]];
    
            [cell.lblAnnouncement setTextColor:[UIColor whiteColor]];
        }
        else
        {
            [cell.btnAnnouncement setSelected:TRUE];
    
            [cell.btnAnnouncement setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.5]];
            [cell.imgAnnouncementCopy setImage:[UIImage imageNamed:@"page.png"]];
            
            [viewShare setHidden:NO];
        }
        
        [cell.btnSatats1 addTarget:self action:@selector(btnStatsBarAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSatats2 addTarget:self action:@selector(btnStatsBarAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSatats3 addTarget:self action:@selector(btnStatsBarAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSatats4 addTarget:self action:@selector(btnStatsBarAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSatats5 addTarget:self action:@selector(btnStatsBarAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSatats6 addTarget:self action:@selector(btnStatsBarAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSatats7 addTarget:self action:@selector(btnStatsBarAction:) forControlEvents:UIControlEventTouchUpInside];
        
        NSMutableDictionary *dictStastics = [[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"statistics"];
        
        if (btnStatsticsBar.currentTitle != nil) {
            
            cell.lblImpressions.text = btnStatsticsBar.currentTitle;
        
            cell.imgStatsBarArrow.center = btnStatsticsBar.center;
            cell.imgStatsBarArrow.frame = CGRectMake(cell.imgStatsBarArrow.frame.origin.x, 133, cell.imgStatsBarArrow.frame.size.width, cell.imgStatsBarArrow.frame.size.height);
            
            if (![[[dictStastics valueForKey:[NSString stringWithFormat:@"%ld",(long)btnStatsticsBar.tag]] valueForKey:@"impressions"] isEqualToString:@""]) {
                
                cell.lblImpressions.text = [NSString stringWithFormat:@"%@ Impressions",[[dictStastics valueForKey:[NSString stringWithFormat:@"%ld",(long)btnStatsticsBar.tag]] valueForKey:@"impressions"]];
            }else{
                cell.lblImpressions.text = @"0 Impressions";
            }
            if (![[[dictStastics valueForKey:[NSString stringWithFormat:@"%ld",(long)btnStatsticsBar.tag]] valueForKey:@"views"] isEqualToString:@""]) {
                
                cell.lblPageViews.text = [NSString stringWithFormat:@"%@ Page Views",[[dictStastics valueForKey:[NSString stringWithFormat:@"%ld",(long)btnStatsticsBar.tag]] valueForKey:@"views"]];
            }else{
                cell.lblPageViews.text = @"0 Page Views";
            }
        }
        
        cell.constImageSatats1Height.constant = 0;
        cell.constImageSatats2Height.constant = 0;
        cell.constImageSatats3Height.constant = 0;
        cell.constImageSatats4Height.constant = 0;
        cell.constImageSatats5Height.constant = 0;
        cell.constImageSatats6Height.constant = 0;
        cell.constImageSatats7Height.constant = 0;
        
        if (dictStastics != nil){
            cell.lblSatats1.text = [[dictStastics valueForKey:@"1"] valueForKey:@"day"];
            cell.lblSatats2.text = [[dictStastics valueForKey:@"2"] valueForKey:@"day"];
            cell.lblSatats3.text = [[dictStastics valueForKey:@"3"] valueForKey:@"day"];
            cell.lblSatats4.text = [[dictStastics valueForKey:@"4"] valueForKey:@"day"];
            cell.lblSatats5.text = [[dictStastics valueForKey:@"5"] valueForKey:@"day"];
            cell.lblSatats6.text = [[dictStastics valueForKey:@"6"] valueForKey:@"day"];
            cell.lblSatats7.text = [[dictStastics valueForKey:@"7"] valueForKey:@"day"];
            
            [cell.btnSatats1 setTitle:[[dictStastics valueForKey:@"1"] valueForKey:@"impressions"] forState:UIControlStateNormal];
            [cell.btnSatats2 setTitle:[[dictStastics valueForKey:@"2"] valueForKey:@"impressions"] forState:UIControlStateNormal];
            [cell.btnSatats3 setTitle:[[dictStastics valueForKey:@"3"] valueForKey:@"impressions"] forState:UIControlStateNormal];
            [cell.btnSatats4 setTitle:[[dictStastics valueForKey:@"4"] valueForKey:@"impressions"] forState:UIControlStateNormal];
            [cell.btnSatats5 setTitle:[[dictStastics valueForKey:@"5"] valueForKey:@"impressions"] forState:UIControlStateNormal];
            [cell.btnSatats6 setTitle:[[dictStastics valueForKey:@"6"] valueForKey:@"impressions"] forState:UIControlStateNormal];
            [cell.btnSatats7 setTitle:[[dictStastics valueForKey:@"7"] valueForKey:@"impressions"] forState:UIControlStateNormal];
            
            NSInteger intHighestChart = 0;
            NSInteger intGraphNo = 0;
            
            if ([[[dictStastics valueForKey:@"1"] valueForKey:@"impressions"] integerValue] != 0) {
                intHighestChart = [[[dictStastics valueForKey:@"1"] valueForKey:@"impressions"] integerValue];
                intGraphNo = 1;
            }
            if ([[[dictStastics valueForKey:@"2"] valueForKey:@"impressions"] integerValue] != 0) {
                
                if (intHighestChart < [[[dictStastics valueForKey:@"2"] valueForKey:@"impressions"] integerValue]) {
                    intHighestChart = [[[dictStastics valueForKey:@"2"] valueForKey:@"impressions"] integerValue];
                    intGraphNo = 2;
                }
            }
            if ([[[dictStastics valueForKey:@"3"] valueForKey:@"impressions"] integerValue] != 0) {
                if (intHighestChart < [[[dictStastics valueForKey:@"3"] valueForKey:@"impressions"] integerValue]) {
                    intHighestChart = [[[dictStastics valueForKey:@"3"] valueForKey:@"impressions"] integerValue];
                    intGraphNo = 3;
                }
            }
            if ([[[dictStastics valueForKey:@"4"] valueForKey:@"impressions"] integerValue] != 0) {
                if (intHighestChart < [[[dictStastics valueForKey:@"4"] valueForKey:@"impressions"] integerValue]) {
                    intHighestChart = [[[dictStastics valueForKey:@"4"] valueForKey:@"impressions"] integerValue];
                    intGraphNo = 4;
                }
            }
            if ([[[dictStastics valueForKey:@"5"] valueForKey:@"impressions"] integerValue] != 0) {
                if (intHighestChart < [[[dictStastics valueForKey:@"5"] valueForKey:@"impressions"] integerValue]) {
                    intHighestChart = [[[dictStastics valueForKey:@"5"] valueForKey:@"impressions"] integerValue];
                    intGraphNo = 5;
                }
            }
            if ([[[dictStastics valueForKey:@"6"] valueForKey:@"impressions"] integerValue] != 0) {
                if (intHighestChart < [[[dictStastics valueForKey:@"6"] valueForKey:@"impressions"] integerValue]) {
                    intHighestChart = [[[dictStastics valueForKey:@"6"] valueForKey:@"impressions"] integerValue];
                    intGraphNo = 6;
                }
            }
            if ([[[dictStastics valueForKey:@"7"] valueForKey:@"impressions"] integerValue] != 0) {
                if (intHighestChart < [[[dictStastics valueForKey:@"7"] valueForKey:@"impressions"] integerValue]) {
                    intHighestChart = [[[dictStastics valueForKey:@"7"] valueForKey:@"impressions"] integerValue];
                    intGraphNo = 7;
                }
            }
            
            float floatGap = [[[dictStastics valueForKey:[NSString stringWithFormat:@"%ld",(long)intGraphNo]] valueForKey:@"impressions"] integerValue] / 100.0;
            
            if (![[[dictStastics valueForKey:@"1"] valueForKey:@"impressions"] isEqualToString:@""]) {
                cell.constImageSatats1Height.constant = [[[dictStastics valueForKey:@"1"] valueForKey:@"impressions"] floatValue] / floatGap;
            }
            if (![[[dictStastics valueForKey:@"2"] valueForKey:@"impressions"] isEqualToString:@""]) {
                cell.constImageSatats2Height.constant = [[[dictStastics valueForKey:@"2"] valueForKey:@"impressions"] floatValue] / floatGap;
            }
            if (![[[dictStastics valueForKey:@"3"] valueForKey:@"impressions"] isEqualToString:@""]) {
                cell.constImageSatats3Height.constant = [[[dictStastics valueForKey:@"3"] valueForKey:@"impressions"] floatValue] / floatGap;
            }
            if (![[[dictStastics valueForKey:@"4"] valueForKey:@"impressions"] isEqualToString:@""]) {
                cell.constImageSatats4Height.constant = [[[dictStastics valueForKey:@"4"] valueForKey:@"impressions"] floatValue] / floatGap;
            }
            if (![[[dictStastics valueForKey:@"5"] valueForKey:@"impressions"] isEqualToString:@""]) {
                cell.constImageSatats5Height.constant = [[[dictStastics valueForKey:@"5"] valueForKey:@"impressions"] floatValue] / floatGap;
            }
            if (![[[dictStastics valueForKey:@"6"] valueForKey:@"impressions"] isEqualToString:@""]) {
                cell.constImageSatats6Height.constant = [[[dictStastics valueForKey:@"6"] valueForKey:@"impressions"] floatValue] / floatGap;
            }
            if (![[[dictStastics valueForKey:@"7"] valueForKey:@"impressions"] isEqualToString:@""]) {
                cell.constImageSatats7Height.constant = [[[dictStastics valueForKey:@"7"] valueForKey:@"impressions"] floatValue] / floatGap;
            }
            
            if (btnStatsticsBar.currentTitle == nil){
                
                if (![[[dictStastics valueForKey:@"7"] valueForKey:@"impressions"] isEqualToString:@""]) {
                    
                    cell.lblImpressions.text = [NSString stringWithFormat:@"%@ Impressions",[[dictStastics valueForKey:@"7"] valueForKey:@"impressions"]];
                }else{
                    cell.lblImpressions.text = @"0 Impressions";
                }
                if (![[[dictStastics valueForKey:@"7"] valueForKey:@"views"] isEqualToString:@""]) {
                    
                    cell.lblPageViews.text = [NSString stringWithFormat:@"%@ Page Views",[[dictStastics valueForKey:@"7"] valueForKey:@"views"]];
                }else{
                    cell.lblPageViews.text = @"0 Page Views";
                }
                
                float delayInSeconds = 0.001;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (float_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    cell.imgStatsBarArrow.center = cell.btnSatats7.center;
                    cell.imgStatsBarArrow.frame = CGRectMake(cell.imgStatsBarArrow.frame.origin.x, 133, cell.imgStatsBarArrow.frame.size.width, cell.imgStatsBarArrow.frame.size.height);
                });
            }
        }
        
        cell.tblList.delegate = self;
        cell.tblList.dataSource = self;
        cell.tblList.rowHeight = UITableViewAutomaticDimension;
        cell.tblList.estimatedRowHeight = 69;
        [cell.tblList reloadData];
        
        CGFloat floatHeight = 0.0;
        
        NSMutableArray *arrEvents = [[NSMutableArray alloc] init];
        NSMutableArray *arrTodaysEvent = [[NSMutableArray alloc] init];
        
        if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] count] != 0) {
            
            arrEvents = [[NSMutableArray alloc] initWithArray:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"]];
            
            for (int i=0; i < [arrEvents count]; i++) {
                
                if ([[Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"] isEqualToString:[Global stringFromDate:[NSDate date] strFormatter:@"yyyyMMdd"]]) {
                    
                    [arrTodaysEvent addObject:[arrEvents objectAtIndex:i]];
                }
            }
        }
        
        BOOL isNeedToCheck = true;
        if (arrTodaysEvent.count == 0) {
            [arrTodaysEvent addObject:@" "];
            isNeedToCheck = false;
        }
        
        
        if (arrEvents.count != 0) {
            
            NSString *strEventTitle = @"";
            NSString *strEventURL = @"";
            if (isNeedToCheck == true && arrTodaysEvent.count != 0) {
                
                for (int i=0; i < arrTodaysEvent.count; i++) {
                    strEventTitle = [[arrTodaysEvent objectAtIndex:i] valueForKey:@"title"];
                    strEventURL = [[arrTodaysEvent objectAtIndex:i] valueForKey:@"url"];
                    
                    UIFont *font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                    
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"TODAY: " attributes:attrsDictionary];
                    
                    font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                    attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                    NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:strEventTitle attributes:attrsDictionary];
                    
                    [attrString appendAttributedString:newAttString];
                    
                    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:70.0/255.0 green:141.0/255.0 blue:221.0/255.0 alpha:1.0] range:[attrString.string rangeOfString:strEventTitle]];
                    
                    if (![strEventURL isEqualToString:@""]){
                        
                        floatHeight = floatHeight + [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + 15 + 10;
                    }
                    if ([self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] < 45) {
                        floatHeight = floatHeight + 45;
                    }else{
                        floatHeight = floatHeight + [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + [self getLabelHeight:[NSString stringWithFormat:@"https://mobiledev.goingout2.com%@",strEventURL] withFont:[UIFont fontWithName:@"Helvetica" size:12.0]] + 10;
                    }
                }
                
            }else{
                
                int intNearest = 0;
                for (int i=1; i < [arrEvents count]; i++) {
                    
                    NSString *strFirstDate = [Global getmonthname:[[arrEvents objectAtIndex:intNearest] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                    NSString *strSecondDate = [Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                    
                    if ([strFirstDate integerValue] < [strSecondDate integerValue]) {
                        
                        intNearest = i;
                    }
                }
                
                strEventTitle = [[arrEvents objectAtIndex:intNearest] valueForKey:@"title"];
                strEventURL = [[arrEvents objectAtIndex:intNearest] valueForKey:@"url"];
                
                UIFont *font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"TODAY: " attributes:attrsDictionary];
                
                font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:strEventTitle attributes:attrsDictionary];
                
                [attrString appendAttributedString:newAttString];
                
                [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:70.0/255.0 green:141.0/255.0 blue:221.0/255.0 alpha:1.0] range:[attrString.string rangeOfString:strEventTitle]];
                
                if (![strEventURL isEqualToString:@""]){
                    
                    floatHeight = floatHeight + [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + 15 + 10;
                }
                if ([self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] < 45) {
                    floatHeight = floatHeight + 45;
                }else{
                    floatHeight = floatHeight + [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + [self getLabelHeight:[NSString stringWithFormat:@"https://mobiledev.goingout2.com%@",strEventURL] withFont:[UIFont fontWithName:@"Helvetica" size:12.0]] + 10;
                }
            }
        }
        
        if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] count] != 0) {
            
            NSString *strDiningSpecial = @"";
            
            for (int i = 0; i < [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] count]; i++) {
                
                if (![strDiningSpecial isEqualToString:@""]) {
                    
                    strDiningSpecial = [strDiningSpecial stringByAppendingString:@", "];
                }
                
                strDiningSpecial = [strDiningSpecial stringByAppendingString:[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] objectAtIndex:i] valueForKey:@"name"]];
            }
            
            UIFont *font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
            NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
            
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"DINING SPECIALS: " attributes:attrsDictionary];
            
            font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
            attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
            NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:strDiningSpecial attributes:attrsDictionary];
            
            [attrString appendAttributedString:newAttString];
            
            [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:70.0/255.0 green:141.0/255.0 blue:221.0/255.0 alpha:1.0] range:[attrString.string rangeOfString:strDiningSpecial]];
            
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials_page_url"] isEqualToString:@""]) {
                
                floatHeight = floatHeight + [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + 15 + 10;
            }
            if ([self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] < 45) {
                floatHeight = floatHeight + 45;
            }else{
                floatHeight = floatHeight + [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + [self getLabelHeight:[NSString stringWithFormat:@"https://mobiledev.goingout2.com%@",[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials_page_url"]] withFont:[UIFont fontWithName:@"Helvetica" size:13.0]] + 10;
            }
        }
        if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"] isEqualToString:@""]) {
            
            NSMutableAttributedString *atrDining = [[NSMutableAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"] isEqualToString:@""]) {
                
                atrDining = [self setAttributedText:@"Dining: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"]];
            }
            
            NSAttributedString *atrFoodDeals = [[NSAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"food_deals"] isEqualToString:@""]) {
                
                if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"] isEqualToString:@""]) {
                    
                    atrFoodDeals = [self setAttributedText:@"\nFood Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"food_deals"]];
                }else{
                    atrFoodDeals = [self setAttributedText:@"Food Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"food_deals"]];
                }
            }
            [atrDining appendAttributedString:atrFoodDeals];
            
            if ([self getLabelHeight:atrDining.string withFont:[UIFont fontWithName:@"Helvetica" size:14.0]] < 45) {
                
                floatHeight = floatHeight + 45;
            }else{
                floatHeight = floatHeight + [self getLabelHeight:atrDining.string] + 8.0;
            }
        }
        if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"] isEqualToString:@""]) {
            
            NSMutableAttributedString *atrNightlife = [[NSMutableAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"] isEqualToString:@""]) {
                
                atrNightlife = [self setAttributedText:@"Nightlife: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"]];
            }
            
            NSAttributedString *atrDrinkDeals = [[NSAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"drink_deals"] isEqualToString:@""]) {
                
                if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"] isEqualToString:@""]){
                    atrDrinkDeals = [self setAttributedText:@"\nDrink Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"drink_deals"]];
                }else{
                    atrDrinkDeals = [self setAttributedText:@"Drink Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"drink_deals"]];
                }
            }
            [atrNightlife appendAttributedString:atrDrinkDeals];
            
            if ([self getLabelHeight:atrNightlife.string withFont:[UIFont fontWithName:@"Helvetica" size:14.0]] < 45) {
                
                floatHeight = floatHeight + 45;
            }else{
                floatHeight = floatHeight + [self getLabelHeight:atrNightlife.string] + 8.0;
            }
        }
        
        cell.constTableHeight.constant = floatHeight;
//        cell.constTableHeight.constant = cell.tblList.contentSize.height;
        
        cell.tblDashboard.dataSource = self;
        cell.tblDashboard.delegate = self;
        [cell.tblDashboard reloadData];
        
        BOOL isNeedTocheckForPhoto = true;
        
        if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] isKindOfClass:[NSDictionary class]] && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"body"]) {
            
            cell.lblAnnouncement.text = [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"body"];
            
            if (![[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"photo_id"] isEqualToString:@""]) {
                
                isNeedTocheckForPhoto = false;
            }
            
            if ([self getBubbleLabelHeight:[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"body"] withFont:[UIFont fontWithName:@"Helvetica" size:13.0]] < 34){
                
                cell.constViewBubbleHeight.constant = 47;
            }else{
                cell.constViewBubbleHeight.constant = [self getBubbleLabelHeight:[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"body"] withFont:[UIFont fontWithName:@"Helvetica" size:13.0]] + 20;
            }
        }else{
            cell.constViewBubbleHeight.constant = 0;
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] count] != 0){
            
            NSMutableArray *arrEvents = [[NSMutableArray alloc] initWithArray:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"]];
            
            /*NSMutableArray *arrTemp = [[NSMutableArray alloc] init];
            for (int i=0; i < [arrEvents count]; i++) {
                
                if ([[Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"] isEqualToString:[Global stringFromDate:[NSDate date] strFormatter:@"yyyyMMdd"]]) {
                
                    [arrTemp addObject:[arrEvents objectAtIndex:i]];
                }
            }
            
            if (arrTemp.count != 0) {
                cell.constTableDashboardHeight.constant = arrTemp.count * 320;
            }else{*/
                int intNearest = 0;
                for (int i=1; i < [arrEvents count]; i++) {
                    
                    NSString *strFirstDate = [Global getmonthname:[[arrEvents objectAtIndex:intNearest] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                    NSString *strSecondDate = [Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                    
                    if ([strFirstDate integerValue] < [strSecondDate integerValue]) {
                        
                        intNearest = i;
                    }
                }
                
                if (![[[arrEvents objectAtIndex:intNearest] valueForKey:@"photo_id"] isEqualToString:@""]) {
                    
                    isNeedTocheckForPhoto = false;
                }
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] count] != 0){
            
            if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] objectAtIndex:0] valueForKey:@"photo_id"] isEqualToString:@""]) {
                
                isNeedTocheckForPhoto = false;
            }
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] count] != 0){
            
            int intReturnCount = 0;
            for (int i=0; i < [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] count]; i++) {
                
                if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] objectAtIndex:i] valueForKey:@"photo_id"] isEqualToString:@""]){
                    intReturnCount = intReturnCount + 1;
                    
                    isNeedTocheckForPhoto = false;
                }
            }
            
            isNeedTocheckForPhoto = false;
            
            cell.constTableDashboardHeight.constant = intReturnCount * 320;
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] count] != 0){
            
            int intReturnCount = 0;
            for (int i=0; i < [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] count]; i++) {
                
                if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] objectAtIndex:i] valueForKey:@"photo_id"] isEqualToString:@""]){
                    intReturnCount = intReturnCount + 1;
                    
                    isNeedTocheckForPhoto = false;
                }
            }
            
            cell.constTableDashboardHeight.constant = intReturnCount * 320;
        }
        
        if (isNeedTocheckForPhoto == true) {
            cell.constTableDashboardHeight.constant = 0;
        }
        
        return cell;
    }else if (tableView.tag == 1001){
        
        static NSString *CellIdentifier = @"DashboardVenueAdminListCell";
        DashboardVenueAdminListCell *cell;
        
        if (cell == nil)
        {
            cell = [(DashboardVenueAdminListCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell.lblTitle.text = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:indexPath.row] valueForKey:@"name"];
        
        return cell;
        
    }else if (tableView.tag == 1002){
        static NSString *CellIdentifier = @"DashboardImageTableCell";
        DashboardImageTableCell *cell;
        
        if (cell == nil)
        {
            cell = [(DashboardImageTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        BOOL isNeedTocheckForPhoto = true;
        
        if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] isKindOfClass:[NSDictionary class]] && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"body"]) {
            
            if (![[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"photo_id"] isEqualToString:@""]) {
                
                [cell.imgDashboard sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@size=m_vpp_banner", IMAGEURL, [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"photo_id"]]]];
                
                isNeedTocheckForPhoto = false;
                
                cell.btnDashboard.accessibilityIdentifier = [NSString stringWithFormat:@"%@/getphoto?id=%@size=m_vpp_banner", IMAGEURL, [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"photo_id"]];
                [cell.btnDashboard addTarget:self action:@selector(btnOpenPhoto:) forControlEvents:UIControlEventTouchUpInside];
            }
            
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] count] != 0){
            
            NSMutableArray *arrEvents = [[NSMutableArray alloc] initWithArray:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"]];
            
            int intNearest = 0;
            for (int i=1; i < [arrEvents count]; i++) {
                
                NSString *strFirstDate = [Global getmonthname:[[arrEvents objectAtIndex:intNearest] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                NSString *strSecondDate = [Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                
                if ([strFirstDate integerValue] < [strSecondDate integerValue]) {
                    
                    intNearest = i;
                }
            }
            
            if (![[[arrEvents objectAtIndex:intNearest] valueForKey:@"photo_id"] isEqualToString:@""]) {
                
                [cell.imgDashboard sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@size=vpp_medium", IMAGEURL, [[arrEvents objectAtIndex:intNearest] valueForKey:@"photo_id"]]]];
                
                isNeedTocheckForPhoto = false;
                
                cell.btnDashboard.accessibilityIdentifier = [NSString stringWithFormat:@"%@/getphoto?id=%@size=vpp_medium", IMAGEURL, [[arrEvents objectAtIndex:intNearest] valueForKey:@"photo_id"]];
                [cell.btnDashboard addTarget:self action:@selector(btnOpenPhoto:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] count] != 0){
            
            if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] objectAtIndex:0] valueForKey:@"photo_id"] isEqualToString:@""]) {
                
                [cell.imgDashboard sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@size=vpp_medium", IMAGEURL, [[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] objectAtIndex:0] valueForKey:@"photo_id"]]]];
                
                isNeedTocheckForPhoto = false;
                
                cell.btnDashboard.accessibilityIdentifier = [NSString stringWithFormat:@"%@/getphoto?id=%@size=vpp_medium", IMAGEURL, [[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] objectAtIndex:0] valueForKey:@"photo_id"]];
                [cell.btnDashboard addTarget:self action:@selector(btnOpenPhoto:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] count] != 0){
            
            NSMutableArray *arrTempImage = [[NSMutableArray alloc] init];
            for (int i=0; i < [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] count]; i++) {
                
                if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] objectAtIndex:i] valueForKey:@"photo_id"] isEqualToString:@""]){
                    
                    [arrTempImage addObject:[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"basic_week_day_photo"] objectAtIndex:i]];
                    
                    isNeedTocheckForPhoto = false;
                }
            }
            
            if (arrTempImage.count != 0) {
                [cell.imgDashboard sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@size=vpp_medium", IMAGEURL, [[arrTempImage objectAtIndex:indexPath.row] valueForKey:@"photo_id"]]]];
                
                cell.btnDashboard.accessibilityIdentifier = [NSString stringWithFormat:@"%@/getphoto?id=%@size=vpp_medium", IMAGEURL, [[arrTempImage objectAtIndex:indexPath.row] valueForKey:@"photo_id"]];
                [cell.btnDashboard addTarget:self action:@selector(btnOpenPhoto:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
        if (isNeedTocheckForPhoto == true && [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] count] != 0){
            
            NSMutableArray *arrTempImage = [[NSMutableArray alloc] init];
            for (int i=0; i < [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] count]; i++) {
                
                if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] objectAtIndex:i] valueForKey:@"photo_id"] isEqualToString:@""]){
                    
                    [arrTempImage addObject:[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"calendar_date_photo"] objectAtIndex:i]];
                    
                    isNeedTocheckForPhoto = false;
                }
            }
            
            if (arrTempImage.count != 0) {
                [cell.imgDashboard sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@size=vpp_medium", IMAGEURL, [[arrTempImage objectAtIndex:indexPath.row] valueForKey:@"photo_id"]]]];
                
                cell.btnDashboard.accessibilityIdentifier = [NSString stringWithFormat:@"%@/getphoto?id=%@size=vpp_medium", IMAGEURL, [[arrTempImage objectAtIndex:indexPath.row] valueForKey:@"photo_id"]];
                [cell.btnDashboard addTarget:self action:@selector(btnOpenPhoto:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        
        return cell;
        
    }else{
        
        static NSString *CellIdentifier = @"DashboardTableCell";
        DashboardTableCell *cell;
        
        if (cell == nil)
        {
            cell = [(DashboardTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell.constButtonTitleHeight.constant = 0;
        
        NSUInteger indexSelectedList = [arrSelectedList indexOfObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        
        NSMutableArray *arrEvents = [[NSMutableArray alloc] init];
        NSMutableArray *arrTodaysEvent = [[NSMutableArray alloc] init];
        
        if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] count] != 0) {
            
            arrEvents = [[NSMutableArray alloc] initWithArray:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"]];
            
            for (int i=0; i < [arrEvents count]; i++) {
                
                if ([[Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"] isEqualToString:[Global stringFromDate:[NSDate date] strFormatter:@"yyyyMMdd"]]) {
                    
                    [arrTodaysEvent addObject:[arrEvents objectAtIndex:i]];
                }
            }
        }
        
        BOOL isNeedToCheck = true;
        if (arrTodaysEvent.count == 0) {
            [arrTodaysEvent addObject:@" "];
            isNeedToCheck = false;
        }
        
        if (indexPath.row < arrTodaysEvent.count) {
            
            strShareText = @"";
            
            if (arrEvents.count != 0) {
                
                cell.imgCheckBox.hidden = false;
                
                NSString *strEventTitle = @"";
                NSString *strEventURL = @"";
                if (isNeedToCheck == true && arrTodaysEvent.count != 0) {
                    
                    strEventTitle = [[arrTodaysEvent objectAtIndex:indexPath.row] valueForKey:@"title"];
                    strEventURL = [[arrTodaysEvent objectAtIndex:indexPath.row] valueForKey:@"url"];
                }else{
                    
                    int intNearest = 0;
                    for (int i=1; i < [arrEvents count]; i++) {
                        
                        NSString *strFirstDate = [Global getmonthname:[[arrEvents objectAtIndex:intNearest] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                        NSString *strSecondDate = [Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                        
                        if ([strFirstDate integerValue] < [strSecondDate integerValue]) {
                            
                            intNearest = i;
                        }
                    }
                    
                    strEventTitle = [[arrEvents objectAtIndex:intNearest] valueForKey:@"title"];
                    strEventURL = [[arrEvents objectAtIndex:intNearest] valueForKey:@"url"];
                }
                
                UIFont *font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"TODAY: " attributes:attrsDictionary];
                
                font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:[strEventTitle uppercaseString] attributes:attrsDictionary];
                
                [attrString appendAttributedString:newAttString];
                
                [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:70.0/255.0 green:141.0/255.0 blue:221.0/255.0 alpha:1.0] range:[attrString.string rangeOfString:[strEventTitle uppercaseString]]];
                
                [cell.lblTitle1 setAttributedText:attrString];
                
                if (indexSelectedList != NSNotFound) {
                    
                    if (![strShareText isEqualToString:@""]) {
                        strShareText = [strShareText stringByAppendingString:@"\n"];
                    }
                    strShareText = [strShareText stringByAppendingString:attrString.string];
                }
                
                if (![strEventURL isEqualToString:@""]) {
                    
                    [cell.btnTitle2 setTitle:[NSString stringWithFormat:@"https://mobiledev.goingout2.com%@",strEventURL] forState:UIControlStateNormal];
                    [cell.btnTitle2 addTarget:self action:@selector(btnOpenURLAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    cell.constButtonTitleHeight.constant = 15.0;
                    
                    if (indexSelectedList != NSNotFound) {
                        strShareText = [strShareText stringByAppendingString:[NSString stringWithFormat:@"\n%@",cell.btnTitle2.currentTitle]];
                    }
                }
            }
            if (arrEvents.count == 0) {
                cell.imgCheckBox.hidden = true;
            }
            else{
                cell.imgCheckBox.hidden = false;
            }
        }
        
        if (indexPath.row == (int)arrTodaysEvent.count) {
            
            if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] count] != 0) {
                
                NSString *strDiningSpecial = @"";
                
                for (int i = 0; i < [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] count]; i++) {
                    
                    if (![strDiningSpecial isEqualToString:@""]) {
                        
                        strDiningSpecial = [strDiningSpecial stringByAppendingString:@", "];
                    }
                    
                    strDiningSpecial = [strDiningSpecial stringByAppendingString:[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] objectAtIndex:i] valueForKey:@"name"]];
                }
                
                UIFont *font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"DINING SPECIALS: " attributes:attrsDictionary];
                
                font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:[strDiningSpecial uppercaseString] attributes:attrsDictionary];
                
                [attrString appendAttributedString:newAttString];
                
                [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:70.0/255.0 green:141.0/255.0 blue:221.0/255.0 alpha:1.0] range:[attrString.string rangeOfString:[strDiningSpecial uppercaseString]]];
                
                [cell.lblTitle1 setAttributedText:attrString];
                
                if (indexSelectedList != NSNotFound) {
                    
                    if (![strShareText isEqualToString:@""]) {
                        strShareText = [strShareText stringByAppendingString:@"\n"];
                    }
                    strShareText = [strShareText stringByAppendingString:attrString.string];
                }
                
                if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials_page_url"] isEqualToString:@""]) {
                    
                    [cell.btnTitle2 setTitle:[NSString stringWithFormat:@"https://mobiledev.goingout2.com%@",[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials_page_url"]] forState:UIControlStateNormal];
                    [cell.btnTitle2 addTarget:self action:@selector(btnOpenURLAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    cell.constButtonTitleHeight.constant = 15.0;
                    
                    if (indexSelectedList != NSNotFound) {
                        strShareText = [strShareText stringByAppendingString:[NSString stringWithFormat:@"\n%@",cell.btnTitle2.currentTitle]];
                    }
                }
                
                cell.imgCheckBox.hidden = false;
            }else{
                cell.imgCheckBox.hidden = true;
            }
        }
        
        if (indexPath.row == (int)arrTodaysEvent.count+1) {
            NSMutableAttributedString *atrDining = [[NSMutableAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"] isEqualToString:@""]) {
                
                atrDining = [self setAttributedText:@"Dining: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"]];
            }
            
            NSAttributedString *atrFoodDeals = [[NSAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"food_deals"] isEqualToString:@""]) {
            
                if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"] isEqualToString:@""]) {
                    
                    atrFoodDeals = [self setAttributedText:@"\nFood Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"food_deals"]];
                }else{
                    atrFoodDeals = [self setAttributedText:@"Food Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"food_deals"]];
                }
            }
            [atrDining appendAttributedString:atrFoodDeals];

            cell.lblTitle1.attributedText = atrDining;
            
            if (indexSelectedList != NSNotFound) {
                if (![strShareText isEqualToString:@""]) {
                    strShareText = [strShareText stringByAppendingString:@"\n"];
                }
                strShareText = [strShareText stringByAppendingString:atrDining.string];
            }
            
            if (![atrDining.string isEqualToString:@""]) {
                cell.imgCheckBox.hidden = false;
            }else{
                cell.imgCheckBox.hidden = true;
            }
        }
        
        if (indexPath.row == (int)arrTodaysEvent.count+2) {
            NSMutableAttributedString *atrNightlife = [[NSMutableAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"] isEqualToString:@""]) {
                
                atrNightlife = [self setAttributedText:@"Nightlife: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"]];
            }
            
            NSAttributedString *atrDrinkDeals = [[NSAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"drink_deals"] isEqualToString:@""]) {
                
                if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"] isEqualToString:@""]){
                    atrDrinkDeals = [self setAttributedText:@"\nDrink Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"drink_deals"]];
                }else{
                    atrDrinkDeals = [self setAttributedText:@"Drink Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"drink_deals"]];
                }
            }
            [atrNightlife appendAttributedString:atrDrinkDeals];
            
            cell.lblTitle1.attributedText = atrNightlife;
            
            if (indexSelectedList != NSNotFound) {
                if (![strShareText isEqualToString:@""]) {
                    strShareText = [strShareText stringByAppendingString:@"\n"];
                }
                strShareText = [strShareText stringByAppendingString:atrNightlife.string];
            }
            
            if (![atrNightlife.string isEqualToString:@""]) {
                cell.imgCheckBox.hidden = false;
            }else{
                cell.imgCheckBox.hidden = true;
            }
        }
        
        if (indexSelectedList != NSNotFound)
        {
            [cell.imgCheckBox setImage:[UIImage imageNamed:@"page.png"]];
            [cell.imgBG setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.3]];
        }
        else
        {
            [cell.imgCheckBox setImage:[UIImage imageNamed:@"page-1.png"]];
            [cell.imgBG setBackgroundColor:[UIColor clearColor]];
        }
        
        if (indexPath.row == (int)arrTodaysEvent.count+2) {
            if (isSelectedCigarNight == true) {
                [viewShare setHidden:false];
                
                NSString *strAnnouncementText = [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"announcement"] valueForKey:@"body"];
                if (![strAnnouncementText isEqualToString:@""]) {
                    
                    strAnnouncementText = [strAnnouncementText stringByAppendingString:@"\n"];
                }
                strShareText = [strAnnouncementText stringByAppendingString:strShareText];
            }
        }
        
        if (![strShareText isEqualToString:@""]) {
            
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = strShareText;
        }
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == tblFullDayInfo) {
        return UITableViewAutomaticDimension;
    }else if (tableView.tag == 1001){
        return 35;
    }else if (tableView.tag == 1002){
        return 320;
    }else if (tableView.tag == 1000) {
        
//        return UITableViewAutomaticDimension;
        
        NSMutableArray *arrEvents = [[NSMutableArray alloc] init];
        NSMutableArray *arrTodaysEvent = [[NSMutableArray alloc] init];
        
        if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] count] != 0) {
            
            arrEvents = [[NSMutableArray alloc] initWithArray:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"]];
            
            for (int i=0; i < [arrEvents count]; i++) {
                
                if ([[Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"] isEqualToString:[Global stringFromDate:[NSDate date] strFormatter:@"yyyyMMdd"]]) {
                    
                    [arrTodaysEvent addObject:[arrEvents objectAtIndex:i]];
                }
            }
        }
        
        if (arrTodaysEvent.count == 0) {
            [arrTodaysEvent addObject:@" "];
        }
        
        if (indexPath.row < arrTodaysEvent.count) {
            
            if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] count] != 0) {
                
                NSMutableArray *arrEvents = [[NSMutableArray alloc] initWithArray:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"]];
                
                int intNearest = 0;
                for (int i=1; i < [arrEvents count]; i++) {
                    
                    NSString *strFirstDate = [Global getmonthname:[[arrEvents objectAtIndex:intNearest] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                    NSString *strSecondDate = [Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                    
                    if ([strFirstDate integerValue] < [strSecondDate integerValue]) {
                        
                        intNearest = i;
                    }
                }
                
                UIFont *font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"TODAY: " attributes:attrsDictionary];
                
                font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] objectAtIndex:intNearest] valueForKey:@"title"] attributes:attrsDictionary];
                
                [attrString appendAttributedString:newAttString];
                
                [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:70.0/255.0 green:141.0/255.0 blue:221.0/255.0 alpha:1.0] range:[attrString.string rangeOfString:[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] objectAtIndex:intNearest] valueForKey:@"title"]]];
                
                if (![[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"] objectAtIndex:intNearest] valueForKey:@"url"] isEqualToString:@""]){
                    
                    return [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + 15 + 10;
                }
                else if ([self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] < 45) {
                    
                    return 45;
                }else{
                    return [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + 10;
                }
            }
            return 0;
        }
        
        if (indexPath.row == (int)arrTodaysEvent.count) {
            if ([[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] count] != 0) {
                
                NSString *strDiningSpecial = @"";
                
                for (int i = 0; i < [[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] count]; i++) {
                    
                    if (![strDiningSpecial isEqualToString:@""]) {
                        
                        strDiningSpecial = [strDiningSpecial stringByAppendingString:@", "];
                    }
                    
                    strDiningSpecial = [strDiningSpecial stringByAppendingString:[[[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials"] objectAtIndex:i] valueForKey:@"name"]];
                }
                
                UIFont *font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"DINING SPECIALS: " attributes:attrsDictionary];
                
                font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
                attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
                NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:strDiningSpecial attributes:attrsDictionary];
                
                [attrString appendAttributedString:newAttString];
                
                [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:70.0/255.0 green:141.0/255.0 blue:221.0/255.0 alpha:1.0] range:[attrString.string rangeOfString:strDiningSpecial]];
                
                if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_specials_page_url"] isEqualToString:@""]) {
                    
                    return [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + 15 + 10;
                }
                else if ([self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] < 45) {
                    
                    return 45;
                }else{
                    return [self getLabelHeight:attrString.string withFont:[UIFont fontWithName:@"LeagueGothic-Regular" size:22.0]] + 10;
                }
            }
            return 0;
        }
        
        if (indexPath.row == (int)arrTodaysEvent.count+1) {
            
            NSMutableAttributedString *atrDining = [[NSMutableAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"] isEqualToString:@""]) {
                
                atrDining = [self setAttributedText:@"Dining: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"]];
            }
            
            NSAttributedString *atrFoodDeals = [[NSAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"food_deals"] isEqualToString:@""]) {
                
                if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"dining_description"] isEqualToString:@""]) {
                    
                    atrFoodDeals = [self setAttributedText:@"\nFood Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"food_deals"]];
                }else{
                    atrFoodDeals = [self setAttributedText:@"Food Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"food_deals"]];
                }
            }
            [atrDining appendAttributedString:atrFoodDeals];
            
            if ([self getLabelHeight:atrDining.string withFont:[UIFont fontWithName:@"Helvetica" size:14.0]] < 45) {
                
                return 45;
            }else{
                return [self getLabelHeight:atrDining.string] + 8;
            }
        }
        
        if (indexPath.row == (int)arrTodaysEvent.count+2) {
            NSMutableAttributedString *atrNightlife = [[NSMutableAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"] isEqualToString:@""]) {
                
                atrNightlife = [self setAttributedText:@"Nightlife: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"]];
            }
            
            NSAttributedString *atrDrinkDeals = [[NSAttributedString alloc] init];
            if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"drink_deals"] isEqualToString:@""]) {
                
                if (![[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"nightlife_description"] isEqualToString:@""]){
                    atrDrinkDeals = [self setAttributedText:@"\nDrink Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"drink_deals"]];
                }else{
                    atrDrinkDeals = [self setAttributedText:@"Drink Deals: " str:[[dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"drink_deals"]];
                }
            }
            [atrNightlife appendAttributedString:atrDrinkDeals];

            if ([self getLabelHeight:atrNightlife.string withFont:[UIFont fontWithName:@"Helvetica" size:14.0]] < 45) {
                
                return 45;
            }else{
                return [self getLabelHeight:atrNightlife.string] + 20;
            }
        }
        
        return 0;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1001) {
        intSelectedVenueAdmin = indexPath.row;
        isOpenVenuAdminDropDown = false;
        THIS.intSelectedVenueIndex = indexPath.row;
        LOADINGSHOW
        [self performSelector:@selector(currentDayJson) withObject:nil afterDelay:0.1f];
    }else if (tableView.tag == 1000) {
        
        NSUInteger index = [arrSelectedList indexOfObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        
        if (index != NSNotFound) {
            [arrSelectedList removeObjectAtIndex:index];
        }else{
            [arrSelectedList addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            
            [Global displayTost1:@"Copied"];
        }
        
        strShareText = @"";
        [tableView reloadData];
        
        if (isSelectedCigarNight == true) {
            [viewShare setHidden:false];
        }else if (arrSelectedList.count != 0) {
            [viewShare setHidden:false];
        }else{
            [viewShare setHidden:true];
        }
    }
}

#pragma mark - Gesture

- (void) handleHoldGesture:(UILongPressGestureRecognizer *)gestureRecognizer{
    if(UIGestureRecognizerStateBegan == gestureRecognizer.state){
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Save Image",nil];
        
        [actionSheet showInView:viewImageFullScreenMode];
    }
    
    if(UIGestureRecognizerStateChanged == gestureRecognizer.state){
        NSLog(@"Changed");
    }
    
    if(UIGestureRecognizerStateEnded == gestureRecognizer.state){
        NSLog(@"Ended");
    }
}

#pragma mark - Image Action

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        
        LOADINGSHOW
        [self performSelector:@selector(saveImage) withObject:nil afterDelay:0.1f];
    }
}

- (void) saveImage{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgFullScreenMode.accessibilityIdentifier]];
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    NSData* theData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgFullScreenMode.accessibilityIdentifier]];
    
    UIImage *img = [UIImage imageWithData:theData];
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    [library writeImageToSavedPhotosAlbum:[img CGImage] orientation:(ALAssetOrientation)[img imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error){
        if (error) {
            NSLog(@"error");
            [Global displayTost:@"Failed to save image"];
        } else {
            NSLog(@"url %@", assetURL);
        }
    }];
    
    LOADINGHIDE
}

#pragma mark - Sharing

-(void)fbShare
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *fbShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [fbShare setInitialText:[NSString stringWithFormat:@"%@",strShareText]];
        
        [fbShare setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             strShareText = @"";
             isSelectedCigarNight = false;
             arrSelectedList = [[NSMutableArray alloc] init];
             [tblFullDayInfo reloadData];
             if (result == SLComposeViewControllerResultCancelled)
             {
                 [Global displayTost:@"You have cancelled Facebook sharing"];
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 [Global displayTost:@"Successfully posted on Facebook"];
             }
         }];
        [self.navigationController presentViewController:fbShare animated:YES completion:^{
            LOADINGHIDE
        }];
        
    }
    else
    {
        LOADINGHIDE
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Facebook integration is not available.  A Facebook account must be set up on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)twitterShare
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *twitShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [twitShare setInitialText:[NSString stringWithFormat:@"%@",strShareText]];
        
        [twitShare setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             strShareText = @"";
             isSelectedCigarNight = false;
             arrSelectedList = [[NSMutableArray alloc] init];
             [tblFullDayInfo reloadData];
             if (result == SLComposeViewControllerResultCancelled)
             {
                 [Global displayTost:@"You have cancelled Twitter sharing"];
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 [Global displayTost:@"Successfully posted on Twitter"];
             }
         }];
        [self.navigationController presentViewController:twitShare animated:YES completion:^{
            LOADINGHIDE
        }];
        
    }
    else
    {
        LOADINGHIDE
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Twitter integration is not available.  A Twitter account must be set up on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - Attributed Text

-(NSMutableAttributedString *)setAttributedText: (NSString *)str1 str:(NSString *) str2
{
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:14.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:str1 attributes:attrsDictionary];
    
    font = [UIFont fontWithName:@"Helvetica" size:14.0];
    attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:str2 attributes:attrsDictionary];
    
    [attrString appendAttributedString:newAttString];
    
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:[attrString.string rangeOfString:str1]];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:[attrString.string rangeOfString:str2]];
    
    return attrString;
}

- (CGFloat)getLabelHeight:(NSString*)string
{
    CGSize constraint = CGSizeMake(CurrenscreenWidth-67, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [string boundingRectWithSize:constraint
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:14.0]}
                                              context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

- (CGFloat)getLabelHeight:(NSString*)string withFont:(UIFont*)font
{
    CGSize constraint = CGSizeMake(CurrenscreenWidth-67, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [string boundingRectWithSize:constraint
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:font}
                                              context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

- (CGFloat)getBubbleLabelHeight:(NSString*)string withFont:(UIFont*)font
{
    CGSize constraint = CGSizeMake(CurrenscreenWidth-47, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [string boundingRectWithSize:constraint
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:font}
                                              context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

@end
