//
//  DashboardMainTableCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 27/05/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardMainTableCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *lblTodaysDate;
@property (nonatomic,retain) IBOutlet UILabel *lblAtmosphere;
@property (nonatomic,retain) IBOutlet UILabel *lblWeatherDescription;
@property (nonatomic,retain) IBOutlet UIImageView *imgWeatherIcon;

@property (nonatomic,retain) IBOutlet UIImageView *imgVenueAdmin;
@property (nonatomic,retain) IBOutlet UILabel *lblVenueAdminName;
@property (nonatomic,retain) IBOutlet UIButton *btnVenueAdminName;

@property (nonatomic,retain) IBOutlet UIView *viewVenueAdminList;
@property (nonatomic,retain) IBOutlet UITableView *tblVenueAdminList;

@property (nonatomic,retain) IBOutlet UIImageView *imgStatsBarArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblImpressions;
@property (nonatomic,retain) IBOutlet UILabel *lblPageViews;

@property (nonatomic,retain) IBOutlet UIButton *btnAnnouncement;
@property (nonatomic,retain) IBOutlet UIImageView *imgAnnouncementCopy;
@property (nonatomic,retain) IBOutlet UILabel *lblAnnouncement;

@property (nonatomic,retain) IBOutlet UIImageView *imgImpressions;

@property (nonatomic,retain) IBOutlet UITableView *tblList;
@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constTableHeight;

@property (nonatomic,retain) IBOutlet UIButton *btnSatats1;
@property (nonatomic,retain) IBOutlet UIButton *btnSatats2;
@property (nonatomic,retain) IBOutlet UIButton *btnSatats3;
@property (nonatomic,retain) IBOutlet UIButton *btnSatats4;
@property (nonatomic,retain) IBOutlet UIButton *btnSatats5;
@property (nonatomic,retain) IBOutlet UIButton *btnSatats6;
@property (nonatomic,retain) IBOutlet UIButton *btnSatats7;

@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constImageSatats1Height;
@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constImageSatats2Height;
@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constImageSatats3Height;
@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constImageSatats4Height;
@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constImageSatats5Height;
@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constImageSatats6Height;
@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constImageSatats7Height;

@property (nonatomic,retain) IBOutlet UILabel *lblSatats1;
@property (nonatomic,retain) IBOutlet UILabel *lblSatats2;
@property (nonatomic,retain) IBOutlet UILabel *lblSatats3;
@property (nonatomic,retain) IBOutlet UILabel *lblSatats4;
@property (nonatomic,retain) IBOutlet UILabel *lblSatats5;
@property (nonatomic,retain) IBOutlet UILabel *lblSatats6;
@property (nonatomic,retain) IBOutlet UILabel *lblSatats7;

@property (nonatomic,retain) IBOutlet UITableView *tblDashboard;
@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constTableDashboardHeight;
//@property (nonatomic,retain) IBOutlet UIImageView *imgEventImage;
//@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constImageEventHeight;

@property (nonatomic,retain) IBOutlet NSLayoutConstraint *constViewBubbleHeight;

@end
