//
//  ThemesTableCell.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 27/10/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "ThemesTableCell.h"

@implementation ThemesTableCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
