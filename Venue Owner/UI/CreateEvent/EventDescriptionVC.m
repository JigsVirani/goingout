//
//  EventDescriptionVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 28/10/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "EventDescriptionVC.h"
#import "GlobalViewController.h"

@interface EventDescriptionVC ()

@end

@implementation EventDescriptionVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [scrlEventDescroition setContentSize:CGSizeMake(CurrenscreenWidth, 602)];
    
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        lblVenueAdminName.text = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"name"];
        
        if (![[THIS.dictUserDetail valueForKey:@"logo"] isEqualToString:@""]){
            [imgVenueAdmin sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@&size=logo", IMAGEURL, [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"imageVenue"]]]];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    
    if (_dictEventDetail != nil) {
        
        NSLog(@"%@",_dictEventDetail);
        
        NSString *strDate = @"";
        
        if ([_dictEventDetail valueForKey:@"photo"]) {
            imgEventPhoto.image = [_dictEventDetail valueForKey:@"photo"];
        }else{
            constEventPhotoHeight.constant = 0;
        }
        
        NSString *strSelectedDate = [NSString stringWithFormat:@"%@ %@", [Global stringFromDate:[NSDate date] strFormatter: @"yyyy"], [_dictEventDetail valueForKey:@"date"]];
        if ([[Global stringFromDate:[NSDate date] strFormatter: @"yyyy-MM-dd"] isEqualToString: [Global getmonthname:strSelectedDate dateformat:@"yyyy EEEE, MMMM dd hh:mm a" dateformat2:@"yyyy-MM-dd"]]) {
            
            [self setTitleAttributedString];
            strDate = [Global getmonthname:[_dictEventDetail valueForKey:@"date"] dateformat:@"EEEE, MMMM dd hh:mm a" dateformat2:@"hh:mm a"];
        }else{
            lblEventTitle.text = [[_dictEventDetail valueForKey:@"title"] uppercaseString];
            [lblEventTitle setTextColor:[UIColor whiteColor]];
            strDate = [_dictEventDetail valueForKey:@"date"];
        }
        
        if ([_dictEventDetail valueForKey:@"SelectedTheme"]) {
            lblThemes.text = [_dictEventDetail valueForKey:@"SelectedTheme"];
        }else{
            imgCheckBoxTheme.hidden = true;
            lblThemes.text = @"";
        }
        
        lblEventDescription.text = [NSString stringWithFormat:@"%@. %@", strDate, [_dictEventDetail valueForKey:@"description"]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Button TouchUp

-(IBAction)btnMenuAction:(id)sender
{
    [[SlideNavigationController sharedInstance] openMenu:MenuLeft withCompletion:nil];
}

-(IBAction)btnEditAction:(id)sender
{
    CreateEventVC *vc = [[CreateEventVC alloc] initWithNibName:@"CreateEventVC" bundle:nil];
    vc.dictEditEventDetail = _dictEventDetail;
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)btnDashboardAction:(id)sender
{
    for (id controller in [self.navigationController viewControllers])
    {
        if ([controller isKindOfClass:[DashboardVC class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
            return;
        }
    }
}

#pragma mark - Attributed Text

-(void)setAttributedText
{
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"11:30PM - 1:00AM. Different House 1000 Main. The Tornadoes are the best alt. rock band and their lead singer Tommy Jam is the sexiest on the planet. Come see them and take advantage of all out normal ass drink deals cause we know you like to party." attributes:attrsDictionary];
    
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] range:[attrString.string rangeOfString:@"11:30PM - 1:00AM. Different House 1000 Main."]];
    
    [lblEventDescription setAttributedText:attrString];
}

-(void) setTitleAttributedString{
    
    UIFont *font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"TODAY: " attributes:attrsDictionary];
    
    font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
    attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:[[_dictEventDetail valueForKey:@"title"] uppercaseString] attributes:attrsDictionary];
    
    [attrString appendAttributedString:newAttString];
    
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:[attrString.string rangeOfString:[[_dictEventDetail valueForKey:@"title"] uppercaseString]]];
    
    [lblEventTitle setAttributedText:attrString];
}

@end
