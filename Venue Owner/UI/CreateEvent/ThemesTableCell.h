//
//  ThemesTableCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 27/10/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThemesTableCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lblTitle;
@property (nonatomic, retain) IBOutlet UIImageView *imgSelected;

@end
