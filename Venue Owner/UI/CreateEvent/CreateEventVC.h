//
//  CreateEventVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 23/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalViewController.h"

@interface CreateEventVC : UIViewController <UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, PECropViewControllerDelegate>
{
    IBOutlet UIScrollView *scrlCreateEvent;
    
    IBOutlet UIButton *btnPost;
    
    IBOutlet UITextField *txtEventTitle;
    IBOutlet UITextView *txtViewDescription;
    
    IBOutlet UIButton *btnUploadPhoto;
    IBOutlet UIButton *btnUploadPhoto1;
    IBOutlet UIButton *btnDateTime;
    IBOutlet UIButton *btnThemes;
    
    IBOutlet UIImageView *imgPhoto;
    
    IBOutlet UIView *viewPhoto;
    IBOutlet UIView *viewPhotoFooter;
    
    IBOutlet UIButton *btnFacebook;
    IBOutlet UIButton *btnTwitter;
    IBOutlet UIButton *btnInsta;
    
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIView *viewDatePicker;
    
    IBOutlet UIImageView *imgArrowDate;
    IBOutlet UIImageView *imgArrowThemes;
    
    IBOutlet UIImageView *imgVenueAdmin;
    IBOutlet UILabel *lblVenueAdminName;
}

-(IBAction)btnCancelAction:(id)sender;
-(IBAction)btnPostAction:(id)sender;

-(IBAction)btnCrossAction:(id)sender;
-(IBAction)btnUploadPhotoAction:(id)sender;
-(IBAction)btnDateTimeAction:(id)sender;
-(IBAction)btnThemesAction:(id)sender;

-(IBAction)btnDoneDatePicker:(id)sender;

-(IBAction)btnSharingAction:(id)sender;

-(IBAction)btnFooterAction:(id)sender;

@property (retain, nonatomic) NSString *strSelectedTheme;
@property (retain, nonatomic) NSString *strSelectedThemeId;

@property (nonatomic,retain) NSMutableDictionary *dictEditEventDetail;
@property (nonatomic) BOOL isFromUpcomingEvents;
@property (nonatomic) BOOL isNeedToClone;
@property (nonatomic) BOOL isFromTheme;

@end
