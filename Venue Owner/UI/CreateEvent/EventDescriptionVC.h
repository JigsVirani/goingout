//
//  EventDescriptionVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 28/10/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDescriptionVC : UIViewController
{
    IBOutlet UIScrollView *scrlEventDescroition;
    
    IBOutlet UIImageView *imgVenueAdmin;
    IBOutlet UIImageView *imgEventPhoto;
    IBOutlet UIImageView *imgCheckBoxTheme;
    
    IBOutlet UILabel *lblVenueAdminName;
    IBOutlet UILabel *lblEventTitle;
    IBOutlet UILabel *lblThemes;
    IBOutlet UILabel *lblEventDescription;
    
    IBOutlet NSLayoutConstraint *constEventPhotoHeight;
}

-(IBAction)btnMenuAction:(id)sender;
-(IBAction)btnEditAction:(id)sender;

-(IBAction)btnDashboardAction:(id)sender;

@property (nonatomic,retain) NSMutableDictionary *dictEventDetail;

@end
