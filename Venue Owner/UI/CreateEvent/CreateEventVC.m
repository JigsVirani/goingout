//
//  CreateEventVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 23/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "CreateEventVC.h"

@interface CreateEventVC ()

@end

@implementation CreateEventVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    viewPhoto.hidden = TRUE;
    viewPhotoFooter.frame = CGRectMake(0, viewPhoto.frame.origin.y, viewPhotoFooter.frame.size.width, viewPhotoFooter.frame.size.height);
    [scrlCreateEvent setContentSize:CGSizeMake(CurrenscreenWidth, viewPhotoFooter.frame.size.height + viewPhotoFooter.frame.origin.y)];
    
    [txtEventTitle addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        lblVenueAdminName.text = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"name"];
        
        if (![[THIS.dictUserDetail valueForKey:@"logo"] isEqualToString:@""]){
            [imgVenueAdmin sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@&size=logo", IMAGEURL, [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"imageVenue"]]]];
        }
    }
    
    if (_dictEditEventDetail != nil) {
        
        if ([_dictEditEventDetail valueForKey:@"photo"] && [[_dictEditEventDetail valueForKey:@"photo"] isKindOfClass:[UIImage class]]) {
            imgPhoto.image = [_dictEditEventDetail valueForKey:@"photo"];
            viewPhoto.hidden = FALSE;
            viewPhotoFooter.frame = CGRectMake(0, viewPhoto.frame.origin.y+viewPhoto.frame.size.height, viewPhotoFooter.frame.size.width, viewPhotoFooter.frame.size.height);
            [scrlCreateEvent setContentSize:CGSizeMake(CurrenscreenWidth, viewPhotoFooter.frame.size.height + viewPhotoFooter.frame.origin.y)];
        }
        
        txtEventTitle.text = [_dictEditEventDetail valueForKey:@"title"];
        
        if ([_dictEditEventDetail valueForKey:@"SelectedTheme"] && ![[_dictEditEventDetail valueForKey:@"SelectedTheme"] isEqualToString:@""]) {
            
            _strSelectedTheme = [_dictEditEventDetail valueForKey:@"SelectedTheme"];
            
            [btnThemes setTitle:_strSelectedTheme forState:UIControlStateNormal];
            
            CGRect idealFrame = [btnThemes.currentTitle boundingRectWithSize:btnThemes.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:btnThemes.titleLabel.font } context:nil];
            imgArrowThemes.frame = CGRectMake(idealFrame.size.width+30, imgArrowThemes.frame.origin.y, imgArrowThemes.frame.size.width, imgArrowThemes.frame.size.height);
        }
        txtViewDescription.text = [_dictEditEventDetail valueForKey:@"description"];
        
        if ([_dictEditEventDetail valueForKey:@"date"]) {
            
            if (_isNeedToClone == false) {
                NSString *strDateTime = [_dictEditEventDetail valueForKey:@"date"];
                if (![[_dictEditEventDetail valueForKey:@"start"] isEqualToString:@""]) {
                    strDateTime = [Global getmonthname:[NSString stringWithFormat:@"%@ %@",strDateTime , [_dictEditEventDetail valueForKey:@"start"]] dateformat:@"yyyy-MM-dd HH:mm:ss" dateformat2:@"EEEE, MMMM dd hh:mm a"];
                    
                }
                [btnDateTime setTitle:strDateTime forState:UIControlStateNormal];
                [btnPost setTitle:@"Save" forState:UIControlStateNormal];
            }else{
                [btnDateTime setTitle:@"Date/Time" forState:UIControlStateNormal];
                [btnPost setTitle:@"Post" forState:UIControlStateNormal];
            }
            
            CGRect idealFrame = [btnDateTime.currentTitle boundingRectWithSize:btnDateTime.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:btnDateTime.titleLabel.font } context:nil];
            imgArrowDate.frame = CGRectMake(idealFrame.size.width+30, imgArrowDate.frame.origin.y, imgArrowDate.frame.size.width, imgArrowDate.frame.size.height);
            
            btnPost.enabled = false;
        }
        
        
        if (_isFromUpcomingEvents == true) {
            
            if (_isNeedToClone == true) {
                
//                txtEventTitle.enabled = false;
//                btnUploadPhoto1.enabled = false;
//                btnThemes.enabled = false;
//                [txtViewDescription setEditable:false];
//                btnFacebook.enabled = false;
//                btnTwitter.enabled = false;
//                btnInsta.enabled = false;
            }else{
                btnDateTime.enabled = false;
            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (_strSelectedTheme != nil && ![_strSelectedTheme isEqualToString:@""])
    {
        [btnThemes setTitle:_strSelectedTheme forState:UIControlStateNormal];
        
        CGRect idealFrame = [btnThemes.currentTitle boundingRectWithSize:btnThemes.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:btnThemes.titleLabel.font } context:nil];
        imgArrowThemes.frame = CGRectMake(idealFrame.size.width+30, imgArrowThemes.frame.origin.y, imgArrowThemes.frame.size.width, imgArrowThemes.frame.size.height);
        
        if (_isFromTheme == true) {
            [self validatePost];
            _isFromTheme = false;
        }
    }
}

#pragma mark - Web Service Call

-(void)createEventJson
{
    NSString *strDate = @"";
    NSString *strTime = @"";
    
    if (![btnDateTime.currentTitle isEqualToString:@"Date/Time"]) {
        strDate = [NSString stringWithFormat:@"%@-%@", [Global stringFromDate:[NSDate date] strFormatter:@"yyyy"],[Global getmonthname:btnDateTime.currentTitle dateformat:@"EEEE, MMMM dd HH:mm a" dateformat2:@"MM-dd"]];
        
        strTime = [NSString stringWithFormat:@"%@",[Global getmonthname:btnDateTime.currentTitle dateformat:@"EEEE, MMMM dd hh:mm a" dateformat2:@"HH:mm:ss"]];
    }
    
    NSString *strTheme1 = @"";
    NSString *strTheme2 = @"";
    
    if (![[btnThemes currentTitle] isEqualToString:@"Themes"]) {
        
        NSArray *arrTemp = [_strSelectedThemeId componentsSeparatedByString:@", "];
        
        strTheme1 = [arrTemp objectAtIndex:0];
        if (arrTemp.count > 1) {
            strTheme2 = [arrTemp objectAtIndex:1];
        }
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setValue:txtEventTitle.text forKey:@"title"];
    [dict setValue:txtViewDescription.text forKey:@"description"];
    [dict setValue:[btnFacebook isSelected] == true ? @"1" : @"0" forKey:@"send_now_fb"];
    [dict setValue:[btnTwitter isSelected] == true ? @"1" : @"0" forKey:@"send_now_tw"];
    [dict setValue:[btnInsta isSelected] == true ? @"1" : @"0" forKey:@"send_now_fs"];
    [dict setValue:strTheme1 forKey:@"theme_id_1"];
    [dict setValue:strTheme2 forKey:@"theme_id_2"];
    
    NSString *strApiType = @"";
    if ([_dictEditEventDetail valueForKey:@"id"]) {
        
        if (_isFromUpcomingEvents == true) {
            [dict setValue:strDate forKey:@"date"];
        }else{
            [dict setValue:[NSString stringWithFormat:@"%@-%@", [Global stringFromDate:[NSDate date] strFormatter:@"yyyy"],[Global getmonthname:btnDateTime.currentTitle dateformat:@"EEEE, MMMM dd HH:mm a" dateformat2:@"MM-dd"]] forKey:@"date"];
        }
        
        if (_isNeedToClone == true) {
            [dict setValue:[[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"id"] forKey:@"venue_id"];
            strApiType = @"createevent";
        }else{
            [dict setValue:[_dictEditEventDetail valueForKey:@"id"] forKey:@"event_id"];
            strApiType = @"UpdateEvent";
        }
        
    }else{
        
        [dict setValue:strDate forKey:@"date"];
        
        [dict setValue:[[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"id"] forKey:@"venue_id"];
        strApiType = @"createevent";
    }
    
    [dict setValue:strTime forKey:@"start"];
    
    NSDictionary *dictJson = [Global requestForPostWithImage:dict isPassToken:true withPhoto:imgPhoto GetKey:strApiType];
    
    LOADINGHIDE
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
            
            if (_isFromUpcomingEvents == true) {
                
                if (_isNeedToClone == true) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"notification" object:nil];
                }else{
                    [_dictEditEventDetail setObject:[dict valueForKey:@"date"] forKey:@"date"];
                    [_dictEditEventDetail setObject:[dict valueForKey:@"start"] forKey:@"start"];
                    [_dictEditEventDetail setObject:txtEventTitle.text forKey:@"title"];
                    [_dictEditEventDetail setObject:txtViewDescription.text forKey:@"description"];
                    
                    if (![btnThemes.currentTitle isEqualToString:@"Themes"]) {
                        [_dictEditEventDetail setObject:strTheme1 forKey:@"theme1_id"];
                        [_dictEditEventDetail setObject:strTheme2 forKey:@"theme2_id"];
                     
                        NSArray *arrTemp = [btnThemes.currentTitle componentsSeparatedByString:@", "];
                        
                        [_dictEditEventDetail setObject:[arrTemp objectAtIndex:0] forKey:@"theme1_name"];
                        if (arrTemp.count > 1) {
                            [_dictEditEventDetail setObject:[arrTemp objectAtIndex:1] forKey:@"theme2_name"];
                        }
                        
                    }
                }
                
                [self.navigationController popViewControllerAnimated:true];
                
                return;
            }
            
            NSMutableDictionary *dictEvent = [[NSMutableDictionary alloc] init];
            
            if (_dictEditEventDetail != nil) {
                dictEvent = _dictEditEventDetail;
            }
            
            [dictEvent setValue:txtEventTitle.text forKey:@"title"];
            [dictEvent setValue:txtViewDescription.text forKey:@"description"];
            [dictEvent setValue:btnDateTime.currentTitle forKey:@"date"];
            [dictEvent setValue:_strSelectedTheme forKey:@"SelectedTheme"];
            [dictEvent setValue:strTheme1 forKey:@"theme_id_1"];
            [dictEvent setValue:strTheme2 forKey:@"theme_id_2"];
            [dictEvent setValue:strTime forKey:@"start"];
            
            if ([dictJson valueForKey:@"event_id"]) {
                [dictEvent setValue:[dictJson valueForKey:@"event_id"] forKey:@"id"];
            }
            
            if (imgPhoto.image) {
                [dictEvent setValue:imgPhoto.image forKey:@"photo"];
            }
    
            EventDescriptionVC *eventDescription = [[EventDescriptionVC alloc] initWithNibName:@"EventDescriptionVC" bundle:nil];
            eventDescription.dictEventDetail = [[NSMutableDictionary alloc] initWithDictionary:dictEvent];
            [self.navigationController pushViewController:eventDescription animated:YES];
        }
        else
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }
    else
    {
        PROBLEM
    }
}

#pragma mark - Button TouchUp

-(IBAction)btnCancelAction:(id)sender
{
    [self.view endEditing:YES];
    
    if (_dictEditEventDetail == nil) {
        for (id controller in [self.navigationController viewControllers]) {
            if ([controller isKindOfClass:[DashboardVC class]]) {
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
    }else{
        [self.navigationController popViewControllerAnimated:true];
    }
}

-(IBAction)btnPostAction:(id)sender
{
    [self.view endEditing:YES];
    
    if (txtEventTitle.text.length == 0) {
        
        [Global displayTost:@"Please enter event title"];
    }else if([btnDateTime.currentTitle isEqualToString:@"Date/Time"]){
        
        [Global displayTost:@"Please select event date"];
    }else{
        
        LOADINGSHOW
        [self performSelector:@selector(createEventJson) withObject:nil afterDelay:0.1];
    }
}

-(IBAction)btnFooterAction:(id)sender
{
    [self.view endEditing:YES];
    if ([sender tag] == 1)
    {
        for (id controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[EventPostVC class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
        EventPostVC *eventPostVC = [[EventPostVC alloc] initWithNibName:@"EventPostVC" bundle:nil];
        [self.navigationController pushViewController:eventPostVC animated:YES];
    }
    else if ([sender tag] == 3)
    {
        for (id controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[DiningSpecialVC class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
        DiningSpecialVC *diningSpecial = [[DiningSpecialVC alloc] initWithNibName:@"DiningSpecialVC" bundle:nil];
        [self.navigationController pushViewController:diningSpecial animated:YES];
    }
    else if ([sender tag] == 4)
    {
        TakePhotoVC *takePhoto = [[TakePhotoVC alloc] initWithNibName:@"TakePhotoVC" bundle:nil];
        [self.navigationController pushViewController:takePhoto animated:YES];
    }
}

-(IBAction)btnCrossAction:(id)sender
{
    [self.view endEditing:YES];
    
    [imgPhoto setImage:nil];
    viewPhoto.hidden = TRUE;
    viewPhotoFooter.frame = CGRectMake(0, viewPhoto.frame.origin.y, viewPhotoFooter.frame.size.width, viewPhotoFooter.frame.size.height);
    [scrlCreateEvent setContentSize:CGSizeMake(CurrenscreenWidth, viewPhotoFooter.frame.size.height + viewPhotoFooter.frame.origin.y)];
    
    [self validatePost];
}

-(IBAction)btnUploadPhotoAction:(id)sender
{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take A New Photo",@"Choose From Gallery",nil];
    
    [actionSheet showInView:self.view];
}

-(IBAction)btnSharingAction:(id)sender
{
    [self.view endEditing:true];
    
    if ([sender tag] == 1)
    {
        if ([btnFacebook isSelected] == TRUE)
        {
            btnFacebook.selected = FALSE;
        }
        else
        {
            btnFacebook.selected = true;
        }
        
    }
    else if ([sender tag] == 2)
    {
        btnTwitter = sender;
        if ([btnTwitter isSelected] == TRUE)
        {
            btnTwitter.selected = FALSE;
        }
        else
        {
            btnTwitter.selected = true;
        }
    }
    else if ([sender tag] == 3)
    {
        btnInsta = sender;
        if ([btnInsta isSelected] == TRUE)
        {
            btnInsta.selected = FALSE;
        }
        else
        {
            btnInsta.selected = true;
        }
    }
}

-(IBAction)btnThemesAction:(id)sender
{
    [self.view endEditing:true];
    
    ThemesVC *themes = [[ThemesVC alloc] initWithNibName:@"ThemesVC" bundle:nil];
    
    if (_strSelectedTheme != nil) {
        themes.strSelectedTheme = _strSelectedTheme;
        themes.strSelectedThemeId = _strSelectedThemeId;
    }
    
    [self.navigationController pushViewController:themes animated:YES];
}

-(IBAction)btnDateTimeAction:(id)sender;
{
    [self.view endEditing:true];
    
    [self.view addSubview:viewDatePicker];
    viewDatePicker.frame = CGRectMake(0, self.view.frame.size.height-viewDatePicker.frame.size.height, self.view.frame.size.width, viewDatePicker.frame.size.height);
    
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.minimumDate = [NSDate date];
    datePicker.minuteInterval = 15;
}

-(IBAction)btnDoneDatePicker:(id)sender
{
    [viewDatePicker removeFromSuperview];
    
    if ([sender tag] == 2)
    {
        [btnDateTime setTitle:[Global stringFromDate:datePicker.date strFormatter:@"EEEE, MMMM dd hh:mm a"] forState:UIControlStateNormal];
        
        CGRect idealFrame = [btnDateTime.currentTitle boundingRectWithSize:btnDateTime.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:btnDateTime.titleLabel.font } context:nil];
        imgArrowDate.frame = CGRectMake(idealFrame.size.width+30, imgArrowDate.frame.origin.y, imgArrowDate.frame.size.width, imgArrowDate.frame.size.height);
        
        [self validatePost];
    }
}

#pragma mark - Image Action

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            [Global displayTost:@"Camera preview not available on this device"];
        }
    }
    else if (buttonIndex == 1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        UIImage *image;
        
        controller.image = img;
        image = img;
                
        CGFloat width = image.size.width;
        CGFloat height = image.size.height;
        CGFloat length = MIN(width, height);
        controller.imageCropRect = CGRectMake((width - length) / 2,
                                              (height - length) / 2,
                                              length,
                                              length);
        
        controller.cropRect = CGRectMake(0, 0, CurrenscreenWidth, 248);
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        
        [self presentViewController:navigationController animated:YES completion:NULL];
    }];
    
    
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    imgPhoto.image = croppedImage;
        
    [btnUploadPhoto setTitle:@"" forState:UIControlStateNormal];
    
    viewPhoto.hidden = FALSE;
    viewPhotoFooter.frame = CGRectMake(0, viewPhoto.frame.origin.y+viewPhoto.frame.size.height, viewPhotoFooter.frame.size.width, viewPhotoFooter.frame.size.height);
    [scrlCreateEvent setContentSize:CGSizeMake(CurrenscreenWidth, viewPhotoFooter.frame.size.height + viewPhotoFooter.frame.origin.y)];
    
    [self validatePost];
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - TextField Delegate

-(void)textFieldDidChange:(UITextField *)txtFld
{
    [self validatePost];
}

#pragma mark - TextView Delegate
- (void)textViewDidChange:(UITextView *)textView;
{
    [self validatePost];
}

#pragma mark - Validate Post

-(void) validatePost
{
    if (txtEventTitle.text.length != 0 && ![btnDateTime.currentTitle isEqualToString:@"Date/Time"]) {
        [btnPost.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0]];
        [btnPost setTitleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        btnPost.enabled = true;
    }else{
        [btnPost.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [btnPost setTitleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.7] forState:UIControlStateNormal];
        btnPost.enabled = false;
    }
}

#pragma mark - Sharing

-(void)fbShare
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *fbShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        NSString *strText = @"";
        if (txtEventTitle.text.length != 0)
        {
            strText = [NSString stringWithFormat:@"Event Title : %@",txtEventTitle.text];
        }
        if (txtViewDescription.text.length != 0)
        {
            strText = [strText stringByAppendingString:[NSString stringWithFormat:@"Event Description : %@",txtViewDescription.text]];
        }
        
        [fbShare setInitialText:strText];
        [fbShare addImage:imgPhoto.image];
        
        [fbShare setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             if (result == SLComposeViewControllerResultCancelled)
             {
                 [Global displayTost:@"You have cancelled Facebook sharing"];
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 [Global displayTost:@"Successfully posted on Facebook"];
             }
             
             btnFacebook.selected = false;
         }];
        [self.navigationController presentViewController:fbShare animated:YES completion:^{
            LOADINGHIDE
        }];
        
    }
    else
    {
        LOADINGHIDE
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Facebook integration is not available.  A Facebook account must be set up on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        btnFacebook.selected = false;
    }
}

-(void)twitterShare
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *twitShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString *strText = @"";
        if (txtEventTitle.text.length != 0)
        {
            strText = [NSString stringWithFormat:@"Event Title : %@",txtEventTitle.text];
        }
        if (txtViewDescription.text.length != 0)
        {
            strText = [strText stringByAppendingString:[NSString stringWithFormat:@"Event Description : %@",txtViewDescription.text]];
        }
        
        [twitShare setInitialText:strText];
        [twitShare addImage:imgPhoto.image];
        
        [twitShare setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             if (result == SLComposeViewControllerResultCancelled)
             {
                 [Global displayTost:@"You have cancelled Twitter sharing"];
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 [Global displayTost:@"Successfully posted on Twitter"];
             }
             
             btnTwitter.selected = false;
         }];
        [self.navigationController presentViewController:twitShare animated:YES completion:^{
            LOADINGHIDE
        }];
        
    }
    else
    {
        LOADINGHIDE
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Twitter integration is not available.  A Twitter account must be set up on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        btnTwitter.selected = false;
    }
}

@end
