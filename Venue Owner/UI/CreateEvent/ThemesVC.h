//
//  ThemesVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 27/10/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThemesVC : UIViewController
{
    IBOutlet UILabel *lblTitle;
    IBOutlet UILabel *lblSelectionText;
    
    IBOutlet UIButton *btnOK;
    
    IBOutlet UITableView *tblThemesList;
    
    NSMutableArray *arrThemeList;
}

@property (nonatomic, assign) NSInteger intFromDiningSpecial;
@property (nonatomic, assign) NSInteger intSelectedIndex;
@property (nonatomic, retain) NSString *strSelectedTheme;
@property (nonatomic, retain) NSString *strSelectedThemeId;
@property (nonatomic) NSInteger intFromManageDiningSpecial;


@end
