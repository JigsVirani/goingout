//
//  ThemesVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 27/10/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "ThemesVC.h"
#import "GlobalViewController.h"

@interface ThemesVC ()

@end

@implementation ThemesVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialization];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Initialization

-(void)initialization
{
    tblThemesList.rowHeight = UITableViewAutomaticDimension;
    tblThemesList.estimatedRowHeight = 44;
    
    if (_intFromManageDiningSpecial == 1) {
        btnOK.hidden = true;
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    if (_intFromDiningSpecial == 1)
    {
        lblTitle.text = @"Dining Specials";
        lblSelectionText.text = @"Select one";
        lblSelectionText.hidden = true;
        
        LOADINGSHOW
        [self performSelector:@selector(existingDiningSpecialListJson) withObject:nil afterDelay:0.1f];
    }
    else if (_intFromDiningSpecial == 2)
    {
        lblTitle.text = @"Meals";
        lblSelectionText.text = @"Select when the Dining Special is offered";
        
        [dict setObject:@"All Day*" forKey:@"Title"];
        arrThemeList = [[NSMutableArray alloc] initWithObjects:dict, nil];
        
        dict = [[NSMutableDictionary alloc] init];
        [dict setObject:@"Breakfast" forKey:@"Title"];
        [arrThemeList addObject:dict];
        
        dict = [[NSMutableDictionary alloc] init];
        [dict setObject:@"Brunch" forKey:@"Title"];
        [arrThemeList addObject:dict];
        
        dict = [[NSMutableDictionary alloc] init];
        [dict setObject:@"Lunch" forKey:@"Title"];
        [arrThemeList addObject:dict];
        
        dict = [[NSMutableDictionary alloc] init];
        [dict setObject:@"Dinner" forKey:@"Title"];
        [arrThemeList addObject:dict];
        
        dict = [[NSMutableDictionary alloc] init];
        [dict setObject:@"Late Night" forKey:@"Title"];
        [arrThemeList addObject:dict];
    }
    else
    {
        LOADINGSHOW
        [self performSelector:@selector(themeListJson) withObject:nil afterDelay:0.1];
    }
}

#pragma mark - Web Service Call

-(void)themeListJson
{
    NSDictionary *dictJson = [Global ArrayJsonData:@"" strbase:@"listEventThemes" passToken:true];
    
    LOADINGHIDE
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            arrThemeList = [[NSMutableArray alloc] init];
            
            NSArray *arrTemp = [[[dictJson valueForKey:@"data"] valueForKey:@"themes"] allKeys];
            
            NSArray *arrSelectedTheme = [_strSelectedTheme componentsSeparatedByString:@", "];
            
            for (int i=0; i < arrTemp.count; i++) {
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:[[[dictJson valueForKey:@"data"] valueForKey:@"themes"] valueForKey:[arrTemp objectAtIndex:i]] forKey:@"Title"];
                [dict setValue:[arrTemp objectAtIndex:i] forKey:@"Theme_id"];
                
                if (arrSelectedTheme.count != 0) {
                    
                    if ([[dict valueForKey:@"Title"] isEqualToString:[arrSelectedTheme objectAtIndex:0]]) {
                        
                        [dict setObject:@"True" forKey:@"isSelected"];
                    }
                    if (arrSelectedTheme.count > 1 && [[dict valueForKey:@"Title"] isEqualToString:[arrSelectedTheme objectAtIndex:1]]){
                        [dict setObject:@"True" forKey:@"isSelected"];
                    }
                }
                
                [arrThemeList addObject:dict];
            }
            
            NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Title" ascending:YES];
            [arrThemeList sortUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
            
            [tblThemesList reloadData];
        }
        else
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }
    else
    {
        PROBLEM
    }
}

-(void)existingDiningSpecialListJson
{
    NSString *strVenueId = @"";
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        strVenueId = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"id"];
    }
    NSDictionary *dictJson = [Global ArrayJsonData:[NSString stringWithFormat:@"venue_id=%@&",strVenueId] strbase:@"ListDiningSpecialsOfVenue?" passToken:true];//&by_date=1
    
    LOADINGHIDE
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            arrThemeList = [[NSMutableArray alloc] init];
            for (int i=0; i < [[dictJson valueForKey:@"data"] count]; i++) {
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[[dictJson valueForKey:@"data"] objectAtIndex:i]];
                [dict setValue:[dict valueForKey:@"name"] forKey:@"Title"];
                [arrThemeList addObject:dict];
            }
            
            [tblThemesList reloadData];
        }
        else
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }
    else
    {
        PROBLEM
    }
}

#pragma mark - Button TouchUp

-(IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnOKAction:(id)sender
{
    if ([[arrThemeList valueForKey:@"isSelected"] indexOfObject:@"True"] != NSNotFound)
    {
        if (_intFromDiningSpecial == 1 || _intFromDiningSpecial == 2)
        {
            NSUInteger index = [[arrThemeList valueForKey:@"isSelected"] indexOfObject:@"True"];
            
            if (index != NSNotFound)
            {
                for (UIViewController* viewController in self.navigationController.viewControllers)
                {
                    if ([viewController isKindOfClass:[DiningSpecialVC class]] )
                    {
                        DiningSpecialVC *diningSpecail = (DiningSpecialVC*)viewController;
                        diningSpecail.intSelectedIndex = _intSelectedIndex;
                        if (_intFromDiningSpecial == 1)
                        {
                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected == %@ ",@"True"];
                            diningSpecail.arrExistingDiningSpecial = [[NSMutableArray alloc] initWithArray:[arrThemeList filteredArrayUsingPredicate:predicate]];
                            
//                            diningSpecail.dictExistingDiningSpecial = [[NSMutableDictionary alloc] initWithDictionary:[arrThemeList objectAtIndex:index]];
                        }
                        else
                        {
                            diningSpecail.strSelectedMeals = @"";
                            for (int i=1; i < arrThemeList.count; i++)
                            {
                                if ([[[arrThemeList objectAtIndex:i] valueForKey:@"isSelected"] isEqualToString:@"True"])
                                {
                                    if (diningSpecail.strSelectedMeals.length != 0)
                                    {
                                        diningSpecail.strSelectedMeals = [NSString stringWithFormat:@"%@, ", diningSpecail.strSelectedMeals];
                                    }
                                    diningSpecail.strSelectedMeals = [NSString stringWithFormat:@"%@%@", diningSpecail.strSelectedMeals, [[arrThemeList objectAtIndex:i] valueForKey:@"Title"]];
                                }
                            }
                        }
                        [self.navigationController popToViewController:diningSpecail animated:YES];
                        break;
                    }
                }
            }
        }
        else
        {
            for (UIViewController* viewController in self.navigationController.viewControllers)
            {
                if ([viewController isKindOfClass:[CreateEventVC class]] )
                {
                    CreateEventVC *createEvent = (CreateEventVC*)viewController;
                    
                    createEvent.strSelectedTheme = @"";
                    createEvent.strSelectedThemeId = @"";
                    createEvent.isFromTheme = true;
                    for (int i=0; i < arrThemeList.count; i++)
                    {
                        if ([[[arrThemeList objectAtIndex:i] valueForKey:@"isSelected"] isEqualToString:@"True"])
                        {
                            if (createEvent.strSelectedTheme.length != 0)
                            {
                                createEvent.strSelectedTheme = [NSString stringWithFormat:@"%@, ", createEvent.strSelectedTheme];
                                createEvent.strSelectedThemeId = [NSString stringWithFormat:@"%@, ", createEvent.strSelectedThemeId];
                            }
                            createEvent.strSelectedTheme = [NSString stringWithFormat:@"%@%@", createEvent.strSelectedTheme, [[arrThemeList objectAtIndex:i] valueForKey:@"Title"]];
                            createEvent.strSelectedThemeId = [NSString stringWithFormat:@"%@%@", createEvent.strSelectedThemeId, [[arrThemeList objectAtIndex:i] valueForKey:@"Theme_id"]];
                        }
                    }
                    
                    [self.navigationController popToViewController:createEvent animated:YES];
                    
                    break;
                }
            }
        }
    }
    else
    {
        [Global displayTost:@"Please select atleast one option to proceed"];
    }
}

#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrThemeList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ThemesTableCell";
    
    ThemesTableCell *cell;
    if (cell==nil){
        cell=[(ThemesTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if (_intFromDiningSpecial == 1) {
        
        NSString *strTitle = [NSString stringWithFormat:@"%@\n",[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"Title"]];
        
        NSString *strAll = @"";
        
        NSString *strMealType = @"";
        
        if ([[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"meal_breakfast"] boolValue] == true) {
            strMealType = [strMealType stringByAppendingString:@"Breakfast"];
        }
        
        if ([[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"meal_lunch"] boolValue] == true) {
            
            if (![strMealType isEqualToString:@""]) {
                strMealType = [strMealType stringByAppendingString:@", "];
            }
            strMealType = [strMealType stringByAppendingString:@"Lunch"];
        }
        
        if ([[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"meal_brunch"] boolValue] == true) {
            
            if (![strMealType isEqualToString:@""]) {
                strMealType = [strMealType stringByAppendingString:@", "];
            }
            strMealType = [strMealType stringByAppendingString:@"Brunch"];
        }
        
        if ([[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"meal_dinner"] boolValue] == true) {
            
            if (![strMealType isEqualToString:@""]) {
                strMealType = [strMealType stringByAppendingString:@", "];
            }
            strMealType = [strMealType stringByAppendingString:@"Dinner"];
        }
        
        if ([[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"meal_latenight"] boolValue] == true) {
            
            if (![strMealType isEqualToString:@""]) {
                strMealType = [strMealType stringByAppendingString:@", "];
            }
            strMealType = [strMealType stringByAppendingString:@"Late night"];
        }
        
        if (![strMealType isEqualToString:@""]) {
            strMealType = [strMealType stringByAppendingString:@". "];
        }
        
        if (![strMealType isEqualToString:@""]) {
            strMealType = [NSString stringWithFormat:@"Meals: %@",strMealType];
        }
        
        NSString *strPrice = @"";
        if ([[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"price"] integerValue] != 0 && ![[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"price"] isEqualToString:@""]) {
            
            float myVal = [[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"price"] floatValue];
            int wholePart = (int)myVal;
            float fractionalPart = fmodf(myVal, wholePart);
            
            if (fractionalPart > 0.0) {
                strPrice = [NSString stringWithFormat:@"Price: $%.2f",[[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"price"] floatValue]];
            }else{
                strPrice = [NSString stringWithFormat:@"Price: $%ld",[[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"price"] integerValue]];
            }
            
//            strPrice = [NSString stringWithFormat:@"Price: %@$",[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"price"]];
        }
        
        NSString *strDescription = @"";
        if (![[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"description"] isEqualToString:@""]) {
            strDescription = [NSString stringWithFormat:@"Description: %@", [[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"description"]];
        }
        
        strAll = [NSString stringWithFormat:@"%@%@",strAll,strMealType];
        if (![strPrice isEqualToString:@""]) {
            
            strAll = [strAll stringByAppendingString:[NSString stringWithFormat:@"\n%@",strPrice]];
        }
        if (![strDescription isEqualToString:@""]) {
            strAll = [strAll stringByAppendingString:[NSString stringWithFormat:@"\n%@",strDescription]];
        }
        
        cell.lblTitle.attributedText = [self setAttributedText:strTitle str:strAll];
        
//        cell.lblTitle.text = strAll;
    }else{
        cell.lblTitle.text = [[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"Title"];
    }
    
    if ([[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"isSelected"] isEqualToString:@"True"])
    {
        cell.backgroundColor = [UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0];
        cell.imgSelected.hidden = false;
    }
    else
    {
        cell.backgroundColor = [UIColor clearColor];
        cell.imgSelected.hidden = true;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"isSelected"] || [[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"isSelected"] isEqualToString:@"False"])
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected ==%@",@"True"];
        NSArray *arrFilter = [arrThemeList filteredArrayUsingPredicate:predicate];
        
        if (_intFromDiningSpecial == 1)
        {
            if (arrFilter.count >= 1 && _intFromManageDiningSpecial == 1)
            {
                return;
            }
        }
        else if (_intFromDiningSpecial != 2)
        {
            if (arrFilter.count >= 2)
            {
                return;
            }
        }
    }
    
    if (_intFromDiningSpecial == 2)
    {
        if (indexPath.row == 0)
        {
            if ([[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"isSelected"] && [[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"isSelected"] isEqualToString:@"True"])
            {
                for (int i=0; i < arrThemeList.count; i++)
                {
                    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] initWithDictionary:[arrThemeList objectAtIndex:i]];
                    [dictData setObject:@"False" forKey:@"isSelected"];
                    [arrThemeList replaceObjectAtIndex:i withObject:dictData];
                }
            }
            else
            {
                for (int i=0; i < arrThemeList.count; i++)
                {
                    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] initWithDictionary:[arrThemeList objectAtIndex:i]];
                    [dictData setObject:@"True" forKey:@"isSelected"];
                    [arrThemeList replaceObjectAtIndex:i withObject:dictData];
                }
            }
            [self highlightOKButton];
            [tblThemesList reloadData];
            
            return;
        }
        else
        {
            NSMutableDictionary *dictData = [[NSMutableDictionary alloc] initWithDictionary:[arrThemeList objectAtIndex:0]];
            [dictData setObject:@"False" forKey:@"isSelected"];
            [arrThemeList replaceObjectAtIndex:0 withObject:dictData];
        }
    }
    
    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] initWithDictionary:[arrThemeList objectAtIndex:indexPath.row]];
    
    if ([[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"isSelected"] && [[[arrThemeList objectAtIndex:indexPath.row] valueForKey:@"isSelected"] isEqualToString:@"True"])
    {
        [dictData setObject:@"False" forKey:@"isSelected"];
    }
    else
    {
        [dictData setObject:@"True" forKey:@"isSelected"];
    }
    
    [arrThemeList replaceObjectAtIndex:indexPath.row withObject:dictData];
    
    [tblThemesList reloadData];
    [self highlightOKButton];
    
    if (_intFromManageDiningSpecial == 1) {
        [self btnOKAction:btnOK];
    }
}

#pragma mark - Highlight OK Button

-(void) highlightOKButton
{
    if ([[arrThemeList valueForKey:@"isSelected"] indexOfObject:@"True"] != NSNotFound)
    {
        [btnOK.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0]];
        [btnOK setTitleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    }
    else
    {
        [btnOK.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [btnOK setTitleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.7] forState:UIControlStateNormal];
    }
}

-(NSMutableAttributedString *)setAttributedText: (NSString *)str1 str:(NSString *) str2
{
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:14.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:str1 attributes:attrsDictionary];
    
    font = [UIFont fontWithName:@"Helvetica" size:14.0];
    attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:str2 attributes:attrsDictionary];
    
    [attrString appendAttributedString:newAttString];
    
//    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:[attrString.string rangeOfString:str1]];
//    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:[attrString.string rangeOfString:str2]];
    
    return attrString;
}

@end
