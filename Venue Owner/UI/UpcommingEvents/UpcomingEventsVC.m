//
//  UpcommingEventsVC.m
//  Venue Owner
//
//  Created by Sandeep on 23/08/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import "UpcomingEventsVC.h"
#import "GlobalViewController.h"

@interface UpcomingEventsVC ()

@end

@implementation UpcomingEventsVC

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        lblVenueAdminName.text = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"name"];
        
        if (![[THIS.dictUserDetail valueForKey:@"logo"] isEqualToString:@""]){
            [imgVenueAdmin sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@&size=logo", IMAGEURL, [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"imageVenue"]]]];
        }
    }
    
    tblEventList.rowHeight = UITableViewAutomaticDimension;
    tblEventList.estimatedRowHeight = 44;
    
    LOADINGSHOW
    [self performSelector:@selector(upcomingEventListJson) withObject:nil afterDelay:0.1f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification) name:@"notification" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    [tblEventList reloadData];
}

#pragma mark - Web Service Call

-(void)upcomingEventListJson {
    NSString *strVenueId = @"";
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        strVenueId = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"id"];
    }
    
    NSDictionary *dictJson = [Global ArrayJsonData:[NSString stringWithFormat:@"venue_id=%@",strVenueId] strbase:@"ListEvents?" passToken:true];
    
    LOADINGHIDE
    if (dictJson != nil) {
        if ([[dictJson valueForKey:@"success"] boolValue] == true) {
            arrEventList = [[NSMutableArray alloc] init];
            
            for (NSInteger i = [[[dictJson valueForKey:@"data"] valueForKey:@"events"] count]-1; i >= 0; i--) {
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[[[dictJson valueForKey:@"data"] valueForKey:@"events"] objectAtIndex:i]];
                [arrEventList addObject:dict];
            }
            
            NSLog(@"%@",arrEventList);
            
            [tblEventList reloadData];
        }else{
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }else{
        PROBLEM
    }
}

-(void)deleteEventJson {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:[[arrEventList objectAtIndex:tblEventList.tag] valueForKey:@"id"] forKey:@"event_id"];
    
    NSDictionary *dictJson = [Global requestForPostWithImage:dict isPassToken:true withPhoto:nil GetKey:@"DeleteEvent"];
    
    LOADINGHIDE
    if (dictJson != nil) {
        if ([[dictJson valueForKey:@"success"] boolValue] == true) {
            [Global displayTost:[dictJson valueForKey:@"message"]];
            [arrEventList removeObjectAtIndex:tblEventList.tag];
            [tblEventList reloadData];
        }else{
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }else{
        PROBLEM
    }
}

#pragma mark - Button TouchUp
-(IBAction)btnCloseAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark: Cell Events
-(IBAction)btnCloneEventAction:(UIButton*)sender {
    CreateEventVC *vc = [[CreateEventVC alloc] initWithNibName:@"CreateEventVC" bundle:nil];
    vc.dictEditEventDetail = [[NSMutableDictionary alloc] init];
    vc.dictEditEventDetail = [arrEventList objectAtIndex:sender.tag];
    vc.isFromUpcomingEvents = true;
    vc.isNeedToClone = true;

    NSString *strThemes = @"";
    NSString *strThemesId = @"";
    if (![[[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme1_name"] isEqualToString:@""]) {
        strThemes = [[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme1_name"];
        strThemesId = [[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme1_id"];
    }
    if (![[[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme2_name"] isEqualToString:@""]) {
        
        if (strThemesId.length != 0) {
            strThemes = [strThemes stringByAppendingString:@", "];
            strThemesId = [strThemesId stringByAppendingString:@", "];
        }
        
        strThemes = [strThemes stringByAppendingString:[[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme2_name"]];
        strThemesId = [strThemesId stringByAppendingString:[[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme2_id"]];
    }
    
    vc.strSelectedTheme = strThemes;
    vc.strSelectedThemeId = strThemesId;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)btnEditEventAction:(UIButton*)sender {
    
    NSLog(@"%ld", (long)sender.tag);
    
    CreateEventVC *vc = [[CreateEventVC alloc] initWithNibName:@"CreateEventVC" bundle:nil];
    vc.dictEditEventDetail = [[NSMutableDictionary alloc] init];
    vc.dictEditEventDetail = [arrEventList objectAtIndex:sender.tag];
    vc.isFromUpcomingEvents = true;
    vc.isNeedToClone = false;
    
    NSString *strThemes = @"";
    NSString *strThemesId = @"";
    if (![[[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme1_name"] isEqualToString:@""]) {
        strThemes = [[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme1_name"];
        strThemesId = [[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme1_id"];
    }
    if (![[[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme2_name"] isEqualToString:@""]) {
        
        if (strThemesId.length != 0) {
            strThemes = [strThemes stringByAppendingString:@", "];
            strThemesId = [strThemesId stringByAppendingString:@", "];
        }
        
        strThemes = [strThemes stringByAppendingString:[[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme2_name"]];
        strThemesId = [strThemesId stringByAppendingString:[[arrEventList objectAtIndex:sender.tag] valueForKey:@"theme2_id"]];
    }
    
    vc.strSelectedTheme = strThemes;
    vc.strSelectedThemeId = strThemesId;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)btnDeleteAction:(UIButton*)sender {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Are you sure you want to delete this Event?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"CANCEL"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"YES"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   
                                   tblEventList.tag = sender.tag;
                                   LOADINGSHOW
                                   [self performSelector:@selector(deleteEventJson) withObject:nil afterDelay:0.1];
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return arrEventList.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0){
        static NSString *CellIdentifier = @"ManageDiningSpecialDishTableCell1";
        
        ManageDiningSpecialDishTableCell1 *cell;
        
        if (cell == nil)
        {
            cell = [(ManageDiningSpecialDishTableCell1 *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        
        cell.lblTitle.text = @"ADD EVENT";
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }else{
        static NSString *CellIdentifier = @"UpcomingEventsTableCell";
        UpcomingEventsTableCell *cell;
        if (cell == nil) {
            cell = [(UpcomingEventsTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell.lblTitle.text = [NSString stringWithFormat:@"%@  %@", [Global getmonthname:[[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"MM/dd/yy"],[[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"title"]];
        
//        if ([[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"isSelected"] && [[[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"isSelected"] isEqualToString:@"Yes"]) {
//            
//            NSString *str = @"";
//            if (![[[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"date"] isEqualToString:@""]) {
//                
//                str = [NSString stringWithFormat:@"Date: %@",[Global getmonthname:[[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"MM-dd-yyyy"]];
//            }
//            if (![[[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"description"] isEqualToString:@""]) {
//                
//                str = [NSString stringWithFormat:@"%@\nDescription: %@",str,[[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"description"]];
//            }
//            cell.lblDescription.text = str;
//        }else{
            cell.lblDescription.text = @"";
//        }
        
        cell.btnClone.tag = indexPath.row - 1;
        [cell.btnClone addTarget:self action:@selector(btnCloneEventAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnEdit.tag = indexPath.row - 1;
        [cell.btnEdit addTarget:self action:@selector(btnEditEventAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnDelete.tag = indexPath.row - 1;
        [cell.btnDelete addTarget:self action:@selector(btnDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        CreateEventVC *createEvent = [[CreateEventVC alloc] initWithNibName:@"CreateEventVC" bundle:nil];
        [self.navigationController pushViewController:createEvent animated:YES];
    }else{
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrEventList objectAtIndex:indexPath.row - 1]];
        if ([[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"isSelected"] && [[[arrEventList objectAtIndex:indexPath.row - 1] valueForKey:@"isSelected"] isEqualToString:@"Yes"])
        {
            [dict setValue:@"No" forKey:@"isSelected"];
        }
        else
        {
            [dict setValue:@"Yes" forKey:@"isSelected"];
        }
        [arrEventList replaceObjectAtIndex:indexPath.row - 1 withObject:dict];
        [tblEventList reloadData];
    }
}

#pragma mark - NSNotification

-(void)notification{
    
    LOADINGSHOW
    [self performSelector:@selector(upcomingEventListJson) withObject:nil afterDelay:0.1f];
}

@end
