//
//  UpcomingEventsTableCell.m
//  Venue Owner
//
//  Created by Sandeep on 23/08/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import "UpcomingEventsTableCell.h"

@implementation UpcomingEventsTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
