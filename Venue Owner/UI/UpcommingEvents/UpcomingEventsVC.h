//
//  UpcommingEventsVC.h
//  Venue Owner
//
//  Created by Sandeep on 23/08/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingEventsVC : UIViewController {
    
    IBOutlet UITableView *tblEventList;
    
    IBOutlet UIImageView *imgVenueAdmin;
    IBOutlet UILabel *lblVenueAdminName;
    
    NSMutableArray *arrEventList;
}

@end
