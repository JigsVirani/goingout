//
//  UpcomingEventsTableCell.h
//  Venue Owner
//
//  Created by Sandeep on 23/08/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingEventsTableCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *lblTitle;
@property (nonatomic,retain) IBOutlet UILabel *lblDescription;

@property (nonatomic, retain) IBOutlet UIButton *btnClone;
@property (nonatomic, retain) IBOutlet UIButton *btnEdit;
@property (nonatomic, retain) IBOutlet UIButton *btnDelete;

@end
