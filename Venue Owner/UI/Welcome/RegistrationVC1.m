//
//  RegistrationVC1.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 22/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "RegistrationVC1.h"
#import "RegistrationVC2.h"
#import "GlobalViewController.h"

@interface RegistrationVC1 ()

@end

@implementation RegistrationVC1

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Buttpon TouchUp

-(IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnOptionAction:(id)sender
{
    [btnBotson setSelected:false];
    [btnRhodeIsland setSelected:false];
    if ([sender tag] == 1)
    {
        [imgBotson setImage:[UIImage imageNamed:@"Check-Box-Mark.png"]];
        [imgRhodeIsland setImage:[UIImage imageNamed:@"check-mark-box.png"]];
        
        [btnBotson setSelected:true];
    }
    else
    {
        [imgBotson setImage:[UIImage imageNamed:@"check-mark-box.png"]];
        [imgRhodeIsland setImage:[UIImage imageNamed:@"Check-Box-Mark.png"]];
        
        [btnRhodeIsland setSelected:true];
    }
}

-(IBAction)btnNextAction:(id)sender
{
    [self.view endEditing:true];
    if ([btnBotson isSelected] == false && [btnRhodeIsland isSelected] == false) {
        
        [Global displayTost:@"Please select region type"];
    }else if (txtVenueName.text.length == 0){
        
        [Global displayTost:@"Please enter venue name"];
    }else if (txtPhone.text.length == 0){
        
        [Global displayTost:@"Please enter phone number"];
    }
    else if (txtPhone.text.length < 10){
        
        [Global displayTost:@"Please enter valid phone number"];
    }else{
        
        RegistrationVC2 *registration2 = [[RegistrationVC2 alloc] initWithNibName:@"RegistrationVC2" bundle:nil];
        
        registration2.dictRegistration = [[NSMutableDictionary alloc] init];
        
        if ([btnBotson isSelected] == true){
            
            [registration2.dictRegistration setObject:@"1" forKey:@"region_id"];
        }else{
            
            [registration2.dictRegistration setObject:@"2" forKey:@"region_id"];
        }
        
        [registration2.dictRegistration setObject:txtVenueName.text forKey:@"name"];
        [registration2.dictRegistration setObject:txtPhone.text forKey:@"phone"];
        
        [self.navigationController pushViewController:registration2 animated:YES];
    }
    
    
}

-(IBAction)btnLoginAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtVenueName)
    {
        [txtPhone becomeFirstResponder];
    }
    else if (textField == txtPhone)
    {
        [txtPhone resignFirstResponder];
    }
    
    return TRUE;
}

@end
