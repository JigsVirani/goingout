//
//  RegistrationVC2.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 22/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "RegistrationVC2.h"
#import "DashboardVC.h"
#import "EventPostVC.h"

@interface RegistrationVC2 ()

@end

@implementation RegistrationVC2

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Web Service Call

-(void)registrationJson
{
    NSDictionary *dict = @{@"name": [_dictRegistration valueForKey:@"name"], @"phone": [_dictRegistration valueForKey:@"phone"], @"region_id": [_dictRegistration valueForKey:@"region_id"], @"position": [btnOwner isSelected] == true ? @"owner" : @"venue manager", @"first_name": txtFirstName.text, @"last_name": txtLastName.text, @"email_address": txtEmail.text, @"password": txtPassword.text, @"password_again": txtPasswordAgain.text};
    
    LOADINGSHOW
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
   ^{
       [Global getParsingDataUsingAsync:dict isPassToken:false GetKey:@"venueregistration" d:^(NSDictionary *dictJson) {
           
           LOADINGHIDE
           if (dictJson != nil)
           {
               if ([[dictJson valueForKey:@"success"] boolValue] == true)
               {
                   THIS.dictUserDetail = [[NSMutableDictionary alloc] initWithDictionary:[dictJson valueForKey:@"date"]];
                   
                   DashboardVC *dashboardVC = [[DashboardVC alloc] initWithNibName:@"DashboardVC" bundle:nil];
                   [self.navigationController pushViewController:dashboardVC animated:YES];
                   
                   [Global displayTost:[dictJson valueForKey:@"message"]];
                   
                   [[NSUserDefaults standardUserDefaults] setObject:THIS.dictUserDetail forKey:@"UserDetail"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
               }
               else
               {
                   [Global displayTost:[dictJson valueForKey:@"message"]];
               }
           }
           else
           {
               PROBLEM
           }
       }];
   });
}

#pragma mark - Buttpon TouchUp

-(IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnOptionAction:(id)sender
{
    [btnOwner setSelected:false];
    [btnManager setSelected:false];
    if ([sender tag] == 1)
    {
        [imgOwner setImage:[UIImage imageNamed:@"Check-Box-Mark.png"]];
        [imgManager setImage:[UIImage imageNamed:@"check-mark-box.png"]];
        [btnOwner setSelected:true];
    }
    else
    {
        [imgOwner setImage:[UIImage imageNamed:@"check-mark-box.png"]];
        [imgManager setImage:[UIImage imageNamed:@"Check-Box-Mark.png"]];
        [btnManager setSelected:true];
    }
}

-(IBAction)btnFinishAction:(id)sender
{
    [self.view endEditing:true];
    if ([btnOwner isSelected] == false && [btnManager isSelected] == false) {
        
        [Global displayTost:@"Please select position"];
    }else if (txtFirstName.text.length == 0){
        
        [Global displayTost:@"Please enter first name"];
    }else if (txtLastName.text.length == 0){
        
        [Global displayTost:@"Please enter last name"];
    }else if (txtEmail.text.length == 0){
        
        [Global displayTost:@"Please enter email (Username)"];
    }else if ([Global validateEmail:txtEmail.text] == false){
        
        [Global displayTost:@"Please enter valid email (Username)"];
    }else if (txtPassword.text.length == 0){
        
        [Global displayTost:@"Please enter password"];
    }else if (![txtPassword.text isEqualToString:txtPasswordAgain.text]){
        
        [Global displayTost:@"Your password is not matching"];
    }else{
        
        [self performSelector:@selector(registrationJson) withObject:nil afterDelay:0.1f];
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtFirstName)
    {
        [txtLastName becomeFirstResponder];
    }
    else if (textField == txtLastName)
    {
        [txtEmail becomeFirstResponder];
    }
    else if (textField == txtEmail)
    {
        [txtPassword becomeFirstResponder];
    }
    else if (textField == txtPassword)
    {
        [txtPasswordAgain becomeFirstResponder];
    }
    else if (textField == txtPasswordAgain)
    {
        [txtPasswordAgain resignFirstResponder];
    }
    
    return TRUE;
}

@end
