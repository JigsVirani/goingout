//
//  ForgotPasswordVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 11/10/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "GlobalViewController.h"

@interface ForgotPasswordVC ()

@end

@implementation ForgotPasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)forgotPasswordJson
{
    NSDictionary *dictJson = [Global ArrayJsonData:[NSString stringWithFormat:@"username=%@",txtEmail.text] strbase:@"ForgotPassword?" passToken:false];
    LOADINGHIDE
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }
    else
    {
        PROBLEM
    }
}

#pragma mark - Buttpon TouchUp

-(IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnResetPasswordAction:(id)sender
{
    [self.view endEditing:true];
    if (txtEmail.text.length == 0)
    {
        [Global displayTost:@"Please enter user name"];
    }
    else if(![Global validateEmail:txtEmail.text])
    {
        [Global displayTost:@"Please enter valid email address"];
    }
    else
    {
        LOADINGSHOW
        [self performSelector:@selector(forgotPasswordJson) withObject:nil afterDelay:0.1f];
    }
}

@end
