//
//  ForgotPasswordVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 11/10/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController
{
    IBOutlet UITextField *txtEmail;
}
@end
