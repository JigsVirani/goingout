//
//  RegistrationVC1.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 22/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationVC1 : UIViewController
{
    IBOutlet UITextField *txtVenueName;
    IBOutlet UITextField *txtPhone;
    
    IBOutlet UIImageView *imgBotson;
    IBOutlet UIImageView *imgRhodeIsland;
    
    IBOutlet UIButton *btnBotson;
    IBOutlet UIButton *btnRhodeIsland;
}

-(IBAction)btnBackAction:(id)sender;

-(IBAction)btnOptionAction:(id)sender;

-(IBAction)btnNextAction:(id)sender;
-(IBAction)btnLoginAction:(id)sender;

@end
