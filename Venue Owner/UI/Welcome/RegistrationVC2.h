//
//  RegistrationVC2.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 22/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationVC2 : UIViewController
{
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtPasswordAgain;
    
    IBOutlet UIImageView *imgOwner;
    IBOutlet UIImageView *imgManager;
    
    IBOutlet UIButton *btnOwner;
    IBOutlet UIButton *btnManager;
}

-(IBAction)btnBackAction:(id)sender;

-(IBAction)btnOptionAction:(id)sender;
-(IBAction)btnFinishAction:(id)sender;

@property (nonatomic, retain) NSMutableDictionary *dictRegistration;

@end
