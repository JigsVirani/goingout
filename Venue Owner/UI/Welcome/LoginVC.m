//
//  LoginVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 21/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "LoginVC.h"
#import "RegistrationVC1.h"
#import "DashboardVC.h"
#import "EventPostVC.h"
#import "GlobalViewController.h"

@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Web Service Call

-(void)loginJson
{
    NSString *strJson = [NSString stringWithFormat:@"username=%@&password=%@",txtUserName.text,txtPassword.text];
    NSDictionary *dictJson = [Global ArrayJsonData:strJson strbase:@"authenticate?" passToken:false];
    
    LOADINGHIDE
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            THIS.dictUserDetail = [[NSMutableDictionary alloc] initWithDictionary:[dictJson valueForKey:@"data"]];
            
            DashboardVC *dashboardVC = [[DashboardVC alloc] initWithNibName:@"DashboardVC" bundle:nil];
            [self.navigationController pushViewController:dashboardVC animated:YES];
            
            [Global displayTost:[dictJson valueForKey:@"message"]];
            
            [[NSUserDefaults standardUserDefaults] setObject:THIS.dictUserDetail forKey:@"UserDetail"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }
    else
    {
        PROBLEM
    }
}

#pragma mark - Button TouchUp

-(IBAction)btnForgotPasswordAction:(id)sender
{
    [self.view endEditing:true];
    
    ForgotPasswordVC *forgotPassword = [[ForgotPasswordVC alloc] initWithNibName:@"ForgotPasswordVC" bundle:nil];
    [self.navigationController pushViewController:forgotPassword animated:YES];
}

-(IBAction)btnLoginAction:(id)sender
{
    [self.view endEditing:true];
    
    if (txtUserName.text.length == 0)
    {
        [Global displayTost:@"Please enter user name"];
    }
    else if(![Global validateEmail:txtUserName.text])
    {
        [Global displayTost:@"Please enter valid email address"];
    }
    else if (txtPassword.text.length == 0)
    {
        [Global displayTost:@"Please enter user name"];
    }
    else
    {
        LOADINGSHOW
        [self performSelector:@selector(loginJson) withObject:nil afterDelay:0.1f];
    }
}

-(IBAction)btnSignUpAction:(id)sender
{
    [self.view endEditing:true];
    RegistrationVC1 *registration = [[RegistrationVC1 alloc] initWithNibName:@"RegistrationVC1" bundle:nil];
    [self.navigationController pushViewController:registration animated:YES];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtUserName)
    {
        [txtPassword becomeFirstResponder];
    }
    else if (textField == txtPassword)
    {
        [txtPassword resignFirstResponder];
    }
    
    return TRUE;
}

@end
