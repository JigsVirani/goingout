//
//  LoginVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 21/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController
{
    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtPassword;
}

-(IBAction)btnForgotPasswordAction:(id)sender;
-(IBAction)btnLoginAction:(id)sender;
-(IBAction)btnSignUpAction:(id)sender;

@end
