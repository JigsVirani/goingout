//
//  EventPostVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 23/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalViewController.h"

@interface TakePhotoVC : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    IBOutlet UITableView *tblList;
    
    IBOutlet UIScrollView *scrlAnnouncement;
    
    IBOutlet UITextView *txtViewDescription;
    
    IBOutlet UIButton *btnPost;
    IBOutlet UIButton *btnCross;
    IBOutlet UIButton *btnUploadPhoto;
    
    IBOutlet UIImageView *imgPhoto;
    
    IBOutlet UILabel *lblCountCreateAnnouncement;
    
    IBOutlet UIView *viewPhoto;
    
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIView *viewDatePicker;
    
    IBOutlet UILabel *lblTodaysDate;
    IBOutlet UILabel *lblAtmosphere;
    IBOutlet UILabel *lblWeatherDescription;
    IBOutlet UIImageView *imgWeatherIcon;
    
    IBOutlet UIImageView *imgVenueAdmin;
    IBOutlet UILabel *lblVenueAdminName;
}

-(IBAction)btnCancelAction:(id)sender;
-(IBAction)btnPostAction:(id)sender;

-(IBAction)btnDoneDatePicker:(id)sender;

-(IBAction)btnFooterAction:(id)sender;

-(IBAction)btnCrossAction:(id)sender;

-(IBAction)btnUploadPhotoAction:(id)sender;

@property (nonatomic, retain) NSString *strSelectedEventLink;

@end
