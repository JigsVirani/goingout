//
//  EventPostVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 23/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "TakePhotoVC.h"
#import "EventPostTableCell.h"
#import "UIView+Toast.h"

@interface TakePhotoVC () <UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate>
{
    UIButton *btnFB, *btnTwitter, *btnInsta;
    NSString *strExpireDate;
}

@end

@implementation TakePhotoVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    viewPhoto.hidden = TRUE;
    tblList.frame = CGRectMake(tblList.frame.origin.x, viewPhoto.frame.origin.y, tblList.frame.size.width, tblList.frame.size.height+5);
    
    [scrlAnnouncement setContentSize:CGSizeMake(CurrenscreenWidth, tblList.frame.origin.y + tblList.frame.size.height)];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take A New Photo",@"Choose From Gallery",nil];
    
    [actionSheet showInView:self.view];
    
    strExpireDate = @"";
    _strSelectedEventLink = @"";
    
    lblTodaysDate.text = [NSString stringWithFormat:@"Today, %@",[Global stringFromDate:[NSDate date] strFormatter:@"MMMM dd yyyy"]];
    
    if ([[[THIS.dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] isKindOfClass:[NSDictionary class]]) {
        lblAtmosphere.text = [NSString stringWithFormat:@"%@ %.0f", [[[THIS.dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] valueForKey:@"weather"], [[[[THIS.dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] valueForKey:@"temp_f"] floatValue]];
        lblWeatherDescription.text = @"";//[[[THIS.dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] valueForKey:@"wind_string"];
        [imgWeatherIcon sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[THIS.dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"weather"] valueForKey:@"icon_url"]]]];
    }
    
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        lblVenueAdminName.text = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"name"];
        
        if (![[THIS.dictUserDetail valueForKey:@"logo"] isEqualToString:@""]){
            [imgVenueAdmin sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@&size=logo", IMAGEURL, [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"imageVenue"]]]];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:true];
    
    [tblList reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Web Service Call

-(void)postPhotoJson
{
    NSDictionary *dict = @{@"content": lblCountCreateAnnouncement.text, @"expires": [strExpireDate isEqualToString:@""] ? @"" : [Global getmonthname:strExpireDate dateformat:@"EEEE, MMMM dd yyyy" dateformat2:@"yyyy-MM-dd"], @"send_now_fb": [btnFB isSelected] == true ? @"1" : @"0", @"send_now_tw": [btnTwitter isSelected] == true ? @"1" : @"0", @"send_now_fs": [btnInsta isSelected] == true ? @"1" : @"0", @"event_link": _strSelectedEventLink, @"venue_id": [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"id"]};
    
    NSDictionary *dictJson = [Global requestForPostWithImage:dict isPassToken:true withPhoto:imgPhoto GetKey:@"makeannouncement"];
    
    LOADINGHIDE
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
            
            for (id controller in [self.navigationController viewControllers])
            {
                if ([controller isKindOfClass:[DashboardVC class]])
                {
                    [self.navigationController popToViewController:controller animated:YES];
                    return;
                }
            }
        }
        else
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
        
    }
    else
    {
        PROBLEM
    }
}


#pragma mark - Button TouchUp

-(IBAction)btnCancelAction:(id)sender
{
    [self.view endEditing:YES];
    
    for (id controller in [self.navigationController viewControllers])
    {
        if ([controller isKindOfClass:[DashboardVC class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
            return;
        }
    }
}

-(IBAction)btnPostAction:(id)sender
{
    [self.view endEditing:YES];
    
    if ([btnPost isSelected] == true) {
        
        LOADINGSHOW
        [self performSelector:@selector(postPhotoJson) withObject:nil afterDelay:0.1];
    }
}

-(IBAction)btnFooterAction:(id)sender
{
    [self.view endEditing:YES];
    if ([sender tag] == 1)
    {
        for (id controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[EventPostVC class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
        EventPostVC *eventPostVC = [[EventPostVC alloc] initWithNibName:@"EventPostVC" bundle:nil];
        [self.navigationController pushViewController:eventPostVC animated:YES];
    }
    else if([sender tag] == 2)
    {
        for (id controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[CreateEventVC class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
        CreateEventVC *createEvent = [[CreateEventVC alloc] initWithNibName:@"CreateEventVC" bundle:nil];
        [self.navigationController pushViewController:createEvent animated:YES];
    }
    else if ([sender tag] == 3)
    {
        for (id controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[DiningSpecialVC class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
        DiningSpecialVC *diningSpecial = [[DiningSpecialVC alloc] initWithNibName:@"DiningSpecialVC" bundle:nil];
        [self.navigationController pushViewController:diningSpecial animated:YES];
    }
    else if ([sender tag] == 4)
    {
//        TakePhotoVC *takePhoto = [[TakePhotoVC alloc] initWithNibName:@"TakePhotoVC" bundle:nil];
//        [self.navigationController pushViewController:takePhoto animated:YES];
    }
}

-(IBAction)btnCrossAction:(id)sender
{
    [self.view endEditing:YES];
    [imgPhoto setImage:nil];
    
    viewPhoto.hidden = TRUE;
    tblList.frame = CGRectMake(tblList.frame.origin.x, viewPhoto.frame.origin.y, tblList.frame.size.width, 216);
    
    [scrlAnnouncement setContentSize:CGSizeMake(CurrenscreenWidth, tblList.frame.origin.y + tblList.frame.size.height)];
    
    [self validatePost];
}

-(IBAction)btnUploadPhotoAction:(id)sender
{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take A New Photo",@"Choose From Gallery",nil];
    
    [actionSheet showInView:self.view];
}

-(IBAction)btnDoneDatePicker:(id)sender
{
    [self.view endEditing:true];
    [viewDatePicker removeFromSuperview];
    
    if ([sender tag] == 2)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE, MMMM dd yyyy"];
        
        strExpireDate = [NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
        
        [tblList reloadData];
    }
}

#pragma mark:Cell Event

-(IBAction)btnSharingAction:(id)sender
{
    [self.view endEditing:true];
    if ([sender tag] == 1)
    {
        btnFB = sender;
        if ([btnFB isSelected] == TRUE)
        {
            btnFB.selected = FALSE;
        }
        else
        {
            btnFB.selected = true;
        }
        
    }
    else if ([sender tag] == 2)
    {
        btnTwitter = sender;
        if ([btnTwitter isSelected] == TRUE)
        {
            btnTwitter.selected = FALSE;
        }
        else
        {
            btnTwitter.selected = true;
        }
    }
    else if ([sender tag] == 3)
    {
        btnInsta = sender;
        if ([btnInsta isSelected] == TRUE)
        {
            btnInsta.selected = FALSE;
        }
        else
        {
            btnInsta.selected = true;
        }
        
    }
        
    [tblList reloadData];
}

-(IBAction)btnExpireDateAction
{
    [self.view endEditing:true];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    [self.view addSubview:viewDatePicker];
    viewDatePicker.frame = CGRectMake(0, self.view.frame.size.height-viewDatePicker.frame.size.height, self.view.frame.size.width, viewDatePicker.frame.size.height);
    
    datePicker.minimumDate = [NSDate date];
}

#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"EventPostTableCell";
    
    EventPostTableCell *cell;
    
    if (cell==nil)
    {
        cell=[(EventPostTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [cell.viewShare setHidden:YES];
    
    if (indexPath.row == 0)
    {
        [cell.lblTitle setText:@"Photo"];
        [cell.imgTitle setImage:[UIImage imageNamed:@"Phonto.png"]];
    }
    else if(indexPath.row == 1)
    {
        if ([_strSelectedEventLink isEqualToString:@""]) {
            [cell.lblTitle setText:@"Add Event Link"];
        }else{
            [cell.lblTitle setText:_strSelectedEventLink];
        }
        [cell.imgTitle setImage:[UIImage imageNamed:@"Add-Event-link.png"]];
    }
    else if(indexPath.row == 2)
    {
        if ([strExpireDate isEqualToString:@""])
        {
            [cell.lblTitle setText:@"Expires"];
        }
        else
        {
            [cell.lblTitle setText:[NSString stringWithFormat:@"Expires: %@ at 3:00 am",strExpireDate]];
        }
        [cell.imgTitle setImage:[UIImage imageNamed:@"Expires.png"]];
    }
    else if(indexPath.row == 3)
    {
        [cell.lblTitle setText:@"Post also to..."];
        [cell.imgTitle setImage:[UIImage imageNamed:@"Also_post_to1 copy.png"]];
        
        [cell.viewShare setHidden:NO];
    }
    
    if (btnFB.selected == true)
    {
        cell.btnFacebook.selected = true;
    }
    if (btnTwitter.selected == true)
    {
        cell.btnTwitter.selected = true;
    }
    if (btnInsta.selected == true)
    {
        cell.btnInsta.selected = true;
    }
    
    cell.btnFacebook.tag = 1;
    [cell.btnFacebook addTarget:self action:@selector(btnSharingAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnTwitter.tag = 2;
    [cell.btnTwitter addTarget:self action:@selector(btnSharingAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnInsta.tag = 3;
    [cell.btnInsta addTarget:self action:@selector(btnSharingAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        [self.view endEditing:YES];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take A New Photo",@"Choose From Gallery",nil];
        
        [actionSheet showInView:self.view];
    }
    else if (indexPath.row == 1)
    {
        EventLinkVC *eventLink = [[EventLinkVC alloc] initWithNibName:@"EventLinkVC" bundle:nil];
        eventLink.intFromAnnouncement = 2;
        [self.navigationController pushViewController:eventLink animated:YES];
    }
    else if (indexPath.row == 2)
    {
        [self btnExpireDateAction];
    }
}

#pragma mark - Image Action

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            [Global displayTost:@"Camera preview not available on this device"];
        }
    }
    else if (buttonIndex == 1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        UIImage *image;
        
        controller.image = img;
        image = img;
        
        CGFloat width = image.size.width;
        CGFloat height = image.size.height;
        CGFloat length = MIN(width, height);
        controller.imageCropRect = CGRectMake((width - length) / 2,
                                              (height - length) / 2,
                                              length,
                                              length);
        
        controller.cropRect = CGRectMake(0, 0, CurrenscreenWidth, 248);
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        
        [self presentViewController:navigationController animated:YES completion:NULL];
    }];
    
    
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    imgPhoto.image = croppedImage;
        
    [btnUploadPhoto setTitle:@"" forState:UIControlStateNormal];
    
    viewPhoto.hidden = FALSE;
    tblList.frame = CGRectMake(tblList.frame.origin.x, viewPhoto.frame.origin.y + viewPhoto.frame.size.height, tblList.frame.size.width, 216);
    
    [scrlAnnouncement setContentSize:CGSizeMake(CurrenscreenWidth, tblList.frame.origin.y + tblList.frame.size.height)];
    
    [self validatePost];
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - TextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
//    if (textView.text.length >= 160 && range.length == 0)
//    {
//        return NO;
//    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView;
{
    NSInteger intTextLength = 160-textView.text.length;
    NSNumber *num = [NSNumber numberWithInteger:intTextLength];
    [lblCountCreateAnnouncement setText:[NSString stringWithFormat:@"%@",num]];
    
    if (textView.text.length >= 150)
    {
        lblCountCreateAnnouncement.textColor = [UIColor redColor];
    }
    else if (textView.text.length >= 140)
    {
        lblCountCreateAnnouncement.textColor = [UIColor colorWithRed:255.0/255.0 green:165.0/255.0 blue:0.0/255.0 alpha:1.0];
    }
    else
    {
        lblCountCreateAnnouncement.textColor = [UIColor colorWithRed:99.0/255.0 green:193.0/255.0 blue:67.0/255.0 alpha:1.0];
    }
    
    [self validatePost];
}

#pragma mark - Validate Post

-(void) validatePost
{
    if (txtViewDescription.text.length != 0 && viewPhoto.hidden == false)
    {
        [btnPost.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0]];
        [btnPost setTitleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [btnPost setSelected:true];
    }
    else
    {
        [btnPost.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [btnPost setTitleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.7] forState:UIControlStateNormal];
        [btnPost setSelected:false];
    }
}

#pragma mark - Sharing

-(void)fbShare
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *fbShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [fbShare setInitialText:[NSString stringWithFormat:@"%@",txtViewDescription.text]];
        [fbShare addImage:imgPhoto.image];
        
        [fbShare setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             if (result == SLComposeViewControllerResultCancelled)
             {
                 [Global displayTost:@"You have cancelled Facebook sharing"];
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 [Global displayTost:@"Successfully posted on Facebook"];
             }
             
             btnFB.selected = false;
             [tblList reloadData];
         }];
        [self.navigationController presentViewController:fbShare animated:YES completion:^{
            LOADINGHIDE
        }];
        
    }
    else
    {
        LOADINGHIDE
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Facebook integration is not available.  A Facebook account must be set up on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        btnFB.selected = false;
        [tblList reloadData];
    }
}

-(void)twitterShare
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *twitShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [twitShare setInitialText:[NSString stringWithFormat:@"%@",txtViewDescription.text]];
        [twitShare addImage:imgPhoto.image];
        
        [twitShare setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             if (result == SLComposeViewControllerResultCancelled)
             {
                 [Global displayTost:@"You have cancelled Twitter sharing"];
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 [Global displayTost:@"Successfully posted on Twitter"];
             }
             
             btnTwitter.selected = false;
             [tblList reloadData];
         }];
        [self.navigationController presentViewController:twitShare animated:YES completion:^{
            LOADINGHIDE
        }];
        
    }
    else
    {
        LOADINGHIDE
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Twitter integration is not available.  A Twitter account must be set up on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        btnTwitter.selected = false;
        [tblList reloadData];
    }
}

@end
