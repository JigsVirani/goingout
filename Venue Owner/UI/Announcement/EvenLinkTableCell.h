//
//  EvenLinkTableCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 05/06/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EvenLinkTableCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *lblTitle;
@property (nonatomic,retain) IBOutlet UILabel *lblDate;
//@property (nonatomic,retain) IBOutlet UILabel *lblEventLink;

@end
