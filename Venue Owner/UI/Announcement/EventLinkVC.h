//
//  EventLinkVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 05/06/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventLinkVC : UIViewController
{
    IBOutlet UITableView *tblEventList;
    
    NSMutableArray *arrEvents;
}

@property (nonatomic, assign) NSInteger intFromAnnouncement;

@end
