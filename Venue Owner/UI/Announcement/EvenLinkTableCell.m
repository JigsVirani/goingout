//
//  EvenLinkTableCell.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 05/06/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import "EvenLinkTableCell.h"

@implementation EvenLinkTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
