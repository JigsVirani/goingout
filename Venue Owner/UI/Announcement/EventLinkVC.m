//
//  EventLinkVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 05/06/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

#import "EventLinkVC.h"
#import "GlobalViewController.h"

@interface EventLinkVC ()

@end

@implementation EventLinkVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tblEventList.rowHeight = UITableViewAutomaticDimension;
    tblEventList.estimatedRowHeight = 88;
    
    arrEvents = [[NSMutableArray alloc] initWithArray:[[THIS.dictCurrentDay valueForKey:@"venue_information"] valueForKey:@"events"]];
    
    if (arrEvents.count > 1) {
        for (int i=0; i < [arrEvents count]; i++) {
            
            for (int j=i+1; j < [arrEvents count]; j++) {
                
                NSString *strFirstDate = [Global getmonthname:[[arrEvents objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                NSString *strSecondDate = [Global getmonthname:[[arrEvents objectAtIndex:j] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"yyyyMMdd"];
                
                if ([strFirstDate integerValue] > [strSecondDate integerValue]) {
                    
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrEvents objectAtIndex:i]];
                    
                    [arrEvents replaceObjectAtIndex:i-1 withObject:[arrEvents objectAtIndex:i]];
                    [arrEvents replaceObjectAtIndex:i withObject:dict];
                }
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button TouchUp

-(IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrEvents.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"EvenLinkTableCell";
    
    EvenLinkTableCell *cell;
    
    if (cell==nil)
    {
        cell=[(EvenLinkTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    
    cell.lblTitle.text = [[arrEvents objectAtIndex:indexPath.row] valueForKey:@"title"];
    cell.lblDate.text = [Global getmonthname:[[arrEvents objectAtIndex:indexPath.row] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"MM-dd-yyyy"];
    
//    if (_intFromAnnouncement == 1) {
//        cell.lblEventLink.text = [NSString stringWithFormat:@"http://mobiledev.goingout2.com/evt/%@",[[arrEvents objectAtIndex:indexPath.row] valueForKey:@"id"]];
//    }else{
//        cell.lblEventLink.text = [[arrEvents objectAtIndex:indexPath.row] valueForKey:@"url"];
//    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_intFromAnnouncement == 1) {
        for (UIViewController* viewController in self.navigationController.viewControllers)
        {
            if ([viewController isKindOfClass:[EventPostVC class]] )
            {
                EventPostVC *diningSpecail = (EventPostVC*)viewController;
                
                diningSpecail.strSelectedEventLink = [NSString stringWithFormat:@"http://mobiledev.goingout2.com/evt/%@",[[arrEvents objectAtIndex:indexPath.row] valueForKey:@"id"]];
                
                [self.navigationController popToViewController:diningSpecail animated:YES];
                break;
            }
        }
    }else{
        for (UIViewController* viewController in self.navigationController.viewControllers)
        {
            if ([viewController isKindOfClass:[TakePhotoVC class]] )
            {
                TakePhotoVC *takePhoto = (TakePhotoVC*)viewController;
                
                takePhoto.strSelectedEventLink = [[arrEvents objectAtIndex:indexPath.row] valueForKey:@"url"];
                
                [self.navigationController popToViewController:takePhoto animated:YES];
                break;
            }
        }
    }
    
}

@end
