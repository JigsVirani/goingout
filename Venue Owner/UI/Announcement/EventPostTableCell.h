//
//  EventPostTableCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 23/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventPostTableCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lblTitle;
@property (nonatomic, retain) IBOutlet UIImageView *imgTitle;

@property (nonatomic, retain) IBOutlet UIButton *btnFacebook;
@property (nonatomic, retain) IBOutlet UIButton *btnTwitter;
@property (nonatomic, retain) IBOutlet UIButton *btnInsta;

@property (nonatomic, retain) IBOutlet UIView *viewShare;

@end
