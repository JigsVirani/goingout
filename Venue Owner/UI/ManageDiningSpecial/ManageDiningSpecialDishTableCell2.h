//
//  ManageDiningSpecialDishTableCell2.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 25/11/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageDiningSpecialDishTableCell2 : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lblTitle;
@property (nonatomic, retain) IBOutlet UILabel *lblDescription;
@property (nonatomic, retain) IBOutlet UIImageView *imgPhoto;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *constImageViewHeight;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *constImagePhotoHeight;

@property (nonatomic, retain) IBOutlet UIButton *btnEdit1;
@property (nonatomic, retain) IBOutlet UIButton *btnEdit2;
@property (nonatomic, retain) IBOutlet UIButton *btnDelete;

@end
