//
//  ManageDiningSpecialVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 24/11/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageDiningSpecialVC : UIViewController
{
    IBOutlet UIImageView *imgVenueAdmin;
    IBOutlet UILabel *lblVenueAdminName;
    
    IBOutlet UIButton *btnDish;
    IBOutlet UIButton *btnDate;
    
    IBOutlet UITableView *tblList;
    
    IBOutlet UIView *viewDateHeader;
    IBOutlet UILabel *lblDate;
    
    NSMutableArray *arrDish;
    NSMutableArray *arrDate;
}

@end
