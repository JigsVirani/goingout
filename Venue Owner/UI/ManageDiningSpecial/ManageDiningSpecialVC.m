//
//  ManageDiningSpecialVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 24/11/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "ManageDiningSpecialVC.h"
#import "GlobalViewController.h"

@interface ManageDiningSpecialVC (){
    NSInteger intDeleteDiningSpecialIndex;
}

@end

@implementation ManageDiningSpecialVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    btnDish.selected = true;
    btnDish.backgroundColor = [UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0];
    
    tblList.rowHeight = UITableViewAutomaticDimension;
    tblList.estimatedRowHeight = 40;
    
    
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        lblVenueAdminName.text = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"name"];
        
        if (![[THIS.dictUserDetail valueForKey:@"logo"] isEqualToString:@""]){
            [imgVenueAdmin sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@&size=logo", IMAGEURL, [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"imageVenue"]]]];
        }
    }
    
    LOADINGSHOW
    [self performSelector:@selector(diningSpecialListJson) withObject:nil afterDelay:0.1f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    [tblList reloadData];
}

#pragma mark - Web Service Call

-(void)diningSpecialListJson
{
    NSString *strVenueId = @"";
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        strVenueId = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"id"];
    }
    
    NSString *strSortedBy = @"";
    if (btnDish.isSelected == true) {
        strSortedBy = @"by_dish=1";
    }else{
        strSortedBy = @"by_date=1";
    }
    
    NSDictionary *dictJson = [Global ArrayJsonData:[NSString stringWithFormat:@"venue_id=%@&%@",strVenueId,strSortedBy] strbase:@"ListDiningSpecialsOfVenue?" passToken:true];
    
    LOADINGHIDE
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            if (btnDish.isSelected == true) {
                
                arrDish = [[NSMutableArray alloc] initWithObjects: @"", nil];
                
                for (int i = 0; i < [[dictJson valueForKey:@"data"] count]; i++) {
                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[[dictJson valueForKey:@"data"] objectAtIndex:i]];
                    
                    [arrDish addObject:dict];
                }
            }else{
                
                arrDate = [[NSMutableArray alloc] init];
                for (int i = 0; i < [[dictJson valueForKey:@"data"] count]; i++) {
                    
                    NSUInteger index = [[arrDate valueForKey:@"date"] indexOfObject:[[[dictJson valueForKey:@"data"] objectAtIndex:i] valueForKey:@"date"]];
                    
                    NSMutableArray *arrData = [[NSMutableArray alloc] init];
                    if (index != NSNotFound) {
                        
                        arrData = [[arrDate objectAtIndex:index] valueForKey:@"data"];
                    }
                    
                    NSMutableDictionary *dictDate = [[NSMutableDictionary alloc] initWithDictionary:[[[dictJson valueForKey:@"data"] objectAtIndex:i] valueForKey:@"dish"]];
                    [dictDate setObject:[[[dictJson valueForKey:@"data"] objectAtIndex:i] valueForKey:@"date"] forKey:@"date"];
                    [arrData addObject:dictDate];
                    
                    NSMutableDictionary *dictData = [[NSMutableDictionary alloc] init];
                    [dictData setObject:[[[dictJson valueForKey:@"data"] objectAtIndex:i] valueForKey:@"date"] forKey:@"date"];
                    [dictData setObject:arrData forKey:@"data"];
                    
                    if (index != NSNotFound) {
                        [arrDate replaceObjectAtIndex:index withObject:dictData];
                    }else{
                        [arrDate addObject:dictData];
                    }
                    
                    NSLog(@"%@",arrDate);
                }
            }
            
            [tblList reloadData];
        }
        else
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }
    else
    {
        PROBLEM
    }
}

-(void)deleteDiningSpecialJson
{
    NSString *strDiningSpecialId = @"";
    NSString *strDate = @"";
    if (btnDish.isSelected == true) {
        strDiningSpecialId = [NSString stringWithFormat:@"%@",[[arrDish objectAtIndex:tblList.tag] valueForKey:@"id"]];
    }else{
        strDiningSpecialId = [NSString stringWithFormat:@"%@",[[[[arrDate objectAtIndex:[[tblList accessibilityIdentifier] integerValue]] valueForKey:@"data"] objectAtIndex:intDeleteDiningSpecialIndex] valueForKey:@"id"]];
        strDate = [NSString stringWithFormat:@"&date=%@",[[arrDate objectAtIndex:[[tblList accessibilityIdentifier] integerValue]] valueForKey:@"date"]];
    }
    
    NSDictionary *dictJson = [Global ArrayJsonData:[NSString stringWithFormat:@"dining_special_id=%@%@",strDiningSpecialId,strDate] strbase:@"DeleteDiningSpecial?" passToken:true];

    if (btnDish.isSelected == true) {
        LOADINGHIDE
    }else{
        intDeleteDiningSpecialIndex = intDeleteDiningSpecialIndex + 1;
        
        if (intDeleteDiningSpecialIndex < [[[arrDate objectAtIndex:[tblList.accessibilityIdentifier integerValue]] valueForKey:@"data"] count]) {
            [self loopDiningSpecial];
            return;
        }else{
            LOADINGHIDE
        }
    }
    if (dictJson != nil)
    {
        if ([[dictJson valueForKey:@"success"] boolValue] == true)
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
            if (btnDish.isSelected == true) {
                [arrDish removeObjectAtIndex:tblList.tag];
            }else{
                
                NSInteger count = [[[arrDate objectAtIndex:[tblList.accessibilityIdentifier integerValue]] valueForKey:@"data"] count];
                for (int i=0; i < count; i++) {
                    
                    [[[arrDate objectAtIndex:[[tblList accessibilityIdentifier] integerValue]] valueForKey:@"data"] removeObjectAtIndex:0];
                }
                
                if ([[[arrDate objectAtIndex:[[tblList accessibilityIdentifier] integerValue]] valueForKey:@"data"] count] == 0) {
                    
                    [arrDate removeObjectAtIndex:[[tblList accessibilityIdentifier] integerValue]];
                }
            }
            [tblList reloadData];
        }
        else
        {
            [Global displayTost:[dictJson valueForKey:@"message"]];
        }
    }
    else
    {
        PROBLEM
    }
}

#pragma mark - Button Touch Up

-(IBAction)btnCloseAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnOptionAction:(id)sender
{
    btnDish.selected = false;
    btnDate.selected = false;
    
    btnDish.backgroundColor = [UIColor whiteColor];
    btnDate.backgroundColor = [UIColor whiteColor];
    
    if ([sender tag] == 1)
    {
        btnDish.selected = true;
        btnDish.backgroundColor = [UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0];
        
        if (arrDish.count == 0) {
            LOADINGSHOW
            [self performSelector:@selector(diningSpecialListJson) withObject:nil afterDelay:0.1f];
        }
    }
    else
    {
        btnDate.selected = true;
        btnDate.backgroundColor = [UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0];
        
        if (arrDate.count == 0) {
            LOADINGSHOW
            [self performSelector:@selector(diningSpecialListJson) withObject:nil afterDelay:0.1f];
        }
    }
    
    [tblList reloadData];
}

-(IBAction)btnEditAction:(id)sender
{
    DiningSpecialVC *diningSpecial = [[DiningSpecialVC alloc] initWithNibName:@"DiningSpecialVC" bundle:nil];
    if (btnDish.selected == true){
        diningSpecial.isByDish = true;
        diningSpecial.dictDiningSpecialDetail = [[NSMutableDictionary alloc] init];
        diningSpecial.dictDiningSpecialDetail = [arrDish objectAtIndex:[sender tag]];
    }else{
        diningSpecial.isByDish = false;
        diningSpecial.arrDateWise = [[NSMutableArray alloc] init];
        diningSpecial.arrDateWise = [[arrDate objectAtIndex:[[sender accessibilityIdentifier] integerValue]] valueForKey:@"data"];
    }
    diningSpecial.intFromManageDiningSpecial = 1;
    diningSpecial.intAddDiningSpecial = 0;
    [self.navigationController pushViewController:diningSpecial animated:YES];
}

-(IBAction)btnDeleteAction:(id)sender
{
    UIButton *btn = (UIButton*) sender;
    if (btnDish.selected == true)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert"
                                     message:@"Are you sure you want to remove this day's Dining Specials?"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"CANCEL"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"YES"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                       tblList.tag = [btn tag];
                                       LOADINGSHOW
                                       [self performSelector:@selector(deleteDiningSpecialJson) withObject:nil afterDelay:0.1f];
                                       
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert"
                                     message:@"Are you sure you want to remove this day's Dining Specials?"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"CANCEL"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"YES"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                       tblList.accessibilityIdentifier = btn.accessibilityIdentifier;
                                       intDeleteDiningSpecialIndex = 0;
                                       LOADINGSHOW
                                       [self performSelector:@selector(loopDiningSpecial) withObject:nil afterDelay:0.1f];
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}
    
-(IBAction)btnDateAction:(id)sender {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrDate objectAtIndex:[sender tag]]];
    
    if ([dict valueForKey:@"isSelected"] && [[dict valueForKey:@"isSelected"] isEqualToString:@"Yes"]){
        [dict setValue:@"No" forKey:@"isSelected"];
    }else{
        [dict setValue:@"Yes" forKey:@"isSelected"];
    }
    [arrDate replaceObjectAtIndex:[sender tag] withObject:dict];
    [tblList reloadData];
}

-(void)loopDiningSpecial {
    
    if (intDeleteDiningSpecialIndex < [[[arrDate objectAtIndex:[tblList.accessibilityIdentifier integerValue]] valueForKey:@"data"] count]) {
        [self deleteDiningSpecialJson];
    }else{
        intDeleteDiningSpecialIndex = 0;
        LOADINGHIDE
    }
}


#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (btnDish.selected == true){
        return 1;
    }else{
        return arrDate.count;
    }
}
    
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (btnDish.selected == true){
        return nil;
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CurrenscreenWidth, 40)];
        [view setBackgroundColor:[UIColor colorWithRed:241.0/255.0 green:241.0/255.0 blue:241.0/255.0 alpha:1.0]];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 8, tableView.frame.size.width-30, 24)];
        [label setFont:[UIFont boldSystemFontOfSize:15]];
        [label setText:[Global getmonthname:[[arrDate objectAtIndex:section] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"MMMM dd, yyyy"]];
        [view addSubview:label];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CurrenscreenWidth, 40)];
        btn.tag = section;
        [btn addTarget:self action:@selector(btnDateAction:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        
        return view;
    }
}
    
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    if (btnDish.selected == true){
        return 0;
    }else{
        return 40;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (btnDish.selected == true){
        return arrDish.count;
    }else{
        
        if ([[arrDate objectAtIndex:section] valueForKey:@"isSelected"] && [[[arrDate objectAtIndex:section] valueForKey:@"isSelected"] isEqualToString:@"Yes"]) {
            return 1;
//            return [[[arrDate objectAtIndex:section] valueForKey:@"data"] count];
        }else{
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnDish.selected == true)
    {
        if (indexPath.row == 0)
        {
            static NSString *CellIdentifier = @"ManageDiningSpecialDishTableCell1";
            
            ManageDiningSpecialDishTableCell1 *cell;
            
            if (cell == nil)
            {
                cell = [(ManageDiningSpecialDishTableCell1 *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            return cell;
        }
        else
        {
            static NSString *CellIdentifier = @"ManageDiningSpecialDishTableCell2";
            
            ManageDiningSpecialDishTableCell2 *cell;
            
            if (cell == nil)
            {
                cell = [(ManageDiningSpecialDishTableCell2 *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[arrDish objectAtIndex:indexPath.row] valueForKey:@"name"]];
            
            if (![[[arrDish objectAtIndex:indexPath.row] valueForKey:@"photo_id"] isEqualToString:@""]) {
                
                [cell.imgPhoto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@size=vpp_large", IMAGEURL, [[arrDish objectAtIndex:indexPath.row] valueForKey:@"photo_id"]]]];
                cell.constImagePhotoHeight.constant = 44;
            }else{
                cell.constImagePhotoHeight.constant = 0;
            }
            
            if ([[arrDish objectAtIndex:indexPath.row] valueForKey:@"isSelected"] && [[[arrDish objectAtIndex:indexPath.row] valueForKey:@"isSelected"] isEqualToString:@"Yes"])
            {
                
                NSString *strMealType = @"";
                if ([[[arrDish objectAtIndex:indexPath.row] valueForKey:@"meal_breakfast"] boolValue] == true) {
                    strMealType = @"Breakfast";
                }
                
                if ([[[arrDish objectAtIndex:indexPath.row] valueForKey:@"meal_lunch"] boolValue] == true) {
                    
                    if (![strMealType isEqualToString:@""]) {
                        strMealType = [strMealType stringByAppendingString:@", "];
                    }
                    strMealType = [strMealType stringByAppendingString:@"Lunch"];
                }
                
                if ([[[arrDish objectAtIndex:indexPath.row] valueForKey:@"meal_brunch"] boolValue] == true) {
                    
                    if (![strMealType isEqualToString:@""]) {
                        strMealType = [strMealType stringByAppendingString:@", "];
                    }
                    strMealType = [strMealType stringByAppendingString:@"Brunch"];
                }
                
                if ([[[arrDish objectAtIndex:indexPath.row] valueForKey:@"meal_dinner"] boolValue] == true) {
                    
                    if (![strMealType isEqualToString:@""]) {
                        strMealType = [strMealType stringByAppendingString:@", "];
                    }
                    strMealType = [strMealType stringByAppendingString:@"Dinner"];
                }
                
                if ([[[arrDish objectAtIndex:indexPath.row] valueForKey:@"meal_latenight"] boolValue] == true) {
                    
                    if (![strMealType isEqualToString:@""]) {
                        strMealType = [strMealType stringByAppendingString:@", "];
                    }
                    strMealType = [strMealType stringByAppendingString:@"Late night"];
                }
                
                if (![strMealType isEqualToString:@""]) {
                    strMealType = [strMealType stringByAppendingString:@". "];
                }
                
                if (![strMealType isEqualToString:@""]) {
                    strMealType = [NSString stringWithFormat:@"Meals: %@",strMealType];
                }
                
                NSString *strPrice = @"";
                if ([[[arrDish objectAtIndex:indexPath.row] valueForKey:@"price"] integerValue] != 0 && ![[[arrDish objectAtIndex:indexPath.row] valueForKey:@"price"] isEqualToString:@""]) {
                    
                    float myVal = [[[arrDish objectAtIndex:indexPath.row] valueForKey:@"price"] floatValue];
                    int wholePart = (int)myVal;
                    float fractionalPart = fmodf(myVal, wholePart);
                    
                    if (fractionalPart > 0.0) {
                        strPrice = [NSString stringWithFormat:@"Price: $%.2f",[[[arrDish objectAtIndex:indexPath.row] valueForKey:@"price"] floatValue]];
                    }else{
                        strPrice = [NSString stringWithFormat:@"Price: $%ld",[[[arrDish objectAtIndex:indexPath.row] valueForKey:@"price"] integerValue]];
                    }
                }
                
                NSString *strDescription = @"";
                if (![[[arrDish objectAtIndex:indexPath.row] valueForKey:@"description"] isEqualToString:@""]) {
                    strDescription = [NSString stringWithFormat:@"Description: %@", [[arrDish objectAtIndex:indexPath.row] valueForKey:@"description"]];
                }
                
                NSString *strAll = strMealType;
                if (![strPrice isEqualToString:@""]) {
                    strAll = [strAll stringByAppendingString:[NSString stringWithFormat:@"\n%@",strPrice]];
                }
                if (![strDescription isEqualToString:@""]) {
                    strAll = [strAll stringByAppendingString:[NSString stringWithFormat:@"\n%@",strDescription]];
                }
                cell.lblDescription.text = strAll;
                
//                cell.lblDescription.attributedText = [self setAttributedText:strMealType str: str:strPrice];
                
                if (![[[arrDish objectAtIndex:indexPath.row] valueForKey:@"photo_id"] isEqualToString:@""]) {
                    cell.constImageViewHeight.constant = 60;
                }else{
                    cell.constImageViewHeight.constant = 16;
                }
            }
            else
            {
                cell.lblDescription.text = @"";
                cell.constImageViewHeight.constant = 0;
            }
            
            cell.btnEdit1.tag = indexPath.row;
            cell.btnEdit2.tag = indexPath.row;
            cell.btnDelete.tag = indexPath.row;
            
            [cell.btnEdit1 addTarget:self action:@selector(btnEditAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnEdit2 addTarget:self action:@selector(btnEditAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnDelete addTarget:self action:@selector(btnDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
    }
    else
    {
        static NSString *CellIdentifier = @"ManageDiningSpecialDateTableCell";
        
        ManageDiningSpecialDateTableCell *cell;
        
        if (cell == nil)
        {
            cell = [(ManageDiningSpecialDateTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        NSString *strTitles = @"";
        for (int i=0; i < [[[arrDate objectAtIndex:indexPath.section] valueForKey:@"data"] count]; i++) {
            if (i == 0) {
                strTitles = [[[[arrDate objectAtIndex:indexPath.section] valueForKey:@"data"] objectAtIndex:i] valueForKey:@"name"];
            }else{
                strTitles = [NSString stringWithFormat:@"%@, %@",strTitles, [[[[arrDate objectAtIndex:indexPath.section] valueForKey:@"data"] objectAtIndex:i] valueForKey:@"name"]];
            }
        }
        cell.lblTitle.text = strTitles;
        
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[[[arrDate objectAtIndex:indexPath.section] valueForKey:@"data"] objectAtIndex:indexPath.row]];
//        
//        cell.lblTitle.text = [dict valueForKey:@"name"];
        cell.lblDescription.text = @"";
        cell.constBtnEditHeight.constant = 0;
        
        cell.btnEdit1.tag = indexPath.row;
        cell.btnEdit2.tag = indexPath.row;
        cell.btnDelete.tag = indexPath.row;
        
        cell.btnEdit1.accessibilityIdentifier = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
        cell.btnEdit2.accessibilityIdentifier = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
        cell.btnDelete.accessibilityIdentifier = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
        
        [cell.btnEdit1 addTarget:self action:@selector(btnEditAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEdit2 addTarget:self action:@selector(btnEditAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnDelete addTarget:self action:@selector(btnDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (btnDish.selected == true) {
        if (indexPath.row == 0){
            DiningSpecialVC *diningSpecial = [[DiningSpecialVC alloc] initWithNibName:@"DiningSpecialVC" bundle:nil];
//            diningSpecial.intFromManageDiningSpecial = 1;
            diningSpecial.intAddDiningSpecial = 1;
            [self.navigationController pushViewController:diningSpecial animated:YES];
        }else{
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrDish objectAtIndex:indexPath.row]];
            if ([[arrDish objectAtIndex:indexPath.row] valueForKey:@"isSelected"] && [[[arrDish objectAtIndex:indexPath.row] valueForKey:@"isSelected"] isEqualToString:@"Yes"]) {
                [dict setValue:@"No" forKey:@"isSelected"];
            }else{
                [dict setValue:@"Yes" forKey:@"isSelected"];
            }
            [arrDish replaceObjectAtIndex:indexPath.row withObject:dict];
            [tblList reloadData];
        }
    }else{
    }
}

#pragma mark - Attributed Text

-(NSMutableAttributedString *)setAttributedText: (NSString *)str1 str:(NSString *) str2 str:(NSString *) str3
{
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:str1 attributes:attrsDictionary];
    
    font = [UIFont fontWithName:@"Helvetica" size:13.0];
    attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:str2 attributes:attrsDictionary];
    
    [attrString appendAttributedString:newAttString];
    
    font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0];
    attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    newAttString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",str3] attributes:attrsDictionary];
    
    [attrString appendAttributedString:newAttString];
    
    return attrString;
}

@end
