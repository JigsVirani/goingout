//
//  ManageDiningSpecialTableCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 25/11/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageDiningSpecialDishTableCell1 : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lblTitle;

@end
