//
//  DiningSpecialTableCell.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 14/09/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "DiningSpecialTableCell.h"

@implementation DiningSpecialTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
