//
//  DiningSpecialPostVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 15/12/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiningSpecialPostVC : UIViewController
{
    IBOutlet UITableView *tblDiningSpecial;
    
    IBOutlet UIImageView *imgVenueAdmin;
    IBOutlet UILabel *lblVenueAdminName;
    IBOutlet UILabel *lblDiningDate;
}

@property (nonatomic, retain) NSMutableArray *arrDiningSpecial;
@property (nonatomic, retain) NSString *strDiningDate;

@end
