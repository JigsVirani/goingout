//
//  CreateEventVC.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 23/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalViewController.h"

@interface DiningSpecialVC : UIViewController <UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, PECropViewControllerDelegate>
{
    IBOutlet UIScrollView *scrlDiningSpecial;
    
    IBOutlet UIButton *btnPost;
    
    IBOutlet UITableView *tblAddMoreDish;
    
    IBOutlet UIView *viewTableFooter;
    
    IBOutlet UIButton *btnFacebook;
    IBOutlet UIButton *btnTwitter;
    IBOutlet UIButton *btnInsta;
    
    IBOutlet UIButton *btnDate;
    IBOutlet UIImageView *imgArrowDate;
    
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIView *viewDatePicker;
    
    IBOutlet UIImageView *imgVenueAdmin;
    IBOutlet UILabel *lblVenueAdminName;
    
    NSInteger intAddMoreIndex;
    
    IBOutlet NSLayoutConstraint *constViewDateHeight;
    IBOutlet NSLayoutConstraint *constTableViewHeight;
    IBOutlet NSLayoutConstraint *constViewFooterHeight;
}

-(IBAction)btnCancelAction:(id)sender;
-(IBAction)btnPostAction:(id)sender;

-(IBAction)btnDateAction:(id)sender;
-(IBAction)btnDoneDatePicker:(id)sender;

-(IBAction)btnFooterAction:(id)sender;

@property (retain, nonatomic) NSMutableArray *arrExistingDiningSpecial;
@property (retain, nonatomic) NSString *strSelectedMeals;

@property (nonatomic, assign) NSInteger intSelectedIndex;

@property (nonatomic, assign) NSInteger intFromManageDiningSpecial;
@property (nonatomic, assign) NSInteger intAddDiningSpecial;
@property (nonatomic) BOOL isByDish;
@property (nonatomic, retain) NSMutableDictionary *dictDiningSpecialDetail;
@property (nonatomic, retain) NSMutableArray *arrDateWise;

@property (nonatomic, retain) NSMutableArray *arrAddMore;
@property (nonatomic, retain) NSString *strDiningDate;
@property (nonatomic) BOOL isFromUpdateDiningSpecial;

@end
