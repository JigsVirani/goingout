//
//  DiningSpecialPostVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 15/12/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "DiningSpecialPostVC.h"
#import "GlobalViewController.h"

@interface DiningSpecialPostVC ()

@end

@implementation DiningSpecialPostVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%@\n%@", _strDiningDate, _arrDiningSpecial);
    
    tblDiningSpecial.rowHeight = UITableViewAutomaticDimension;
    tblDiningSpecial.estimatedRowHeight = 364;
    
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        lblVenueAdminName.text = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"name"];
        
        if (![[THIS.dictUserDetail valueForKey:@"logo"] isEqualToString:@""]){
            [imgVenueAdmin sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@&size=logo", IMAGEURL, [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"imageVenue"]]]];
        }
    }
    
    if ([[Global stringFromDate:[NSDate date] strFormatter: @"yyyy-MM-dd"] isEqualToString: _strDiningDate]) {
        
        lblDiningDate.text = [NSString stringWithFormat:@"TODAY %@",[[Global getmonthname:_strDiningDate dateformat:@"yyyy-MM-dd" dateformat2:@"MMM dd"] uppercaseString]];
    }else{
        lblDiningDate.text = [NSString stringWithFormat:@"%@",[[Global getmonthname:_strDiningDate dateformat:@"yyyy-MM-dd" dateformat2:@"MMM dd"] uppercaseString]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView Delegate

-(IBAction)btnMenuAction:(id)sender
{
    [[SlideNavigationController sharedInstance] openMenu:MenuLeft withCompletion:nil];
}

-(IBAction)btnEditAction:(id)sender
{
    DiningSpecialVC *vc = [[DiningSpecialVC alloc] initWithNibName:@"DiningSpecialVC" bundle:nil];
    vc.arrAddMore = [[NSMutableArray alloc] initWithArray:_arrDiningSpecial];
    vc.strDiningDate = _strDiningDate;
    vc.isFromUpdateDiningSpecial = true;
    [self.navigationController pushViewController:vc animated:true];
}

-(IBAction)btnDashboardAction:(id)sender
{
    for (id controller in [self.navigationController viewControllers])
    {
        if ([controller isKindOfClass:[DashboardVC class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
            return;
        }
    }
}

#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrDiningSpecial.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DiningSpecialPostTableCell";
    
    DiningSpecialPostTableCell *cell;
    
    if (cell == nil)
    {
        cell = [(DiningSpecialPostTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[_arrDiningSpecial objectAtIndex:indexPath.row]];
    
    if ([dict valueForKey:@"Photo"]) {
        cell.imgDiningPhoto.image = [dict valueForKey:@"Photo"];
        cell.constDiningPhotoHeight.constant = 237;
    }else{
        cell.constDiningPhotoHeight.constant = 0;
    }
    
    if ([[Global stringFromDate:[NSDate date] strFormatter: @"yyyy-MM-dd"] isEqualToString: _strDiningDate]) {
        
        if (![[dict valueForKey:@"Price"] isEqualToString:@""]) {
            cell.lblDiningTitle.attributedText =  [self setTitleAttributedString:[NSString stringWithFormat:@"%@ - $%@", [dict valueForKey:@"DishName"],[dict valueForKey:@"Price"]]];
        }else{
            cell.lblDiningTitle.attributedText =  [self setTitleAttributedString:[NSString stringWithFormat:@"%@", [dict valueForKey:@"DishName"]]];
        }
    }else{
        cell.lblDiningTitle.text = [NSString stringWithFormat:@"%@ - $%@", [dict valueForKey:@"DishName"],[dict valueForKey:@"Price"]];
    }
    
    cell.lblAvailableDining.text = [dict valueForKey:@"Meal"];
    cell.lblDescription.text = [dict valueForKey:@"Description"];

    return cell;
}

#pragma mark - Attributed Text
-(NSAttributedString*) setTitleAttributedString: (NSString*)strTitle{
    
    UIFont *font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"TODAY: " attributes:attrsDictionary];
    
    font = [UIFont fontWithName:@"LeagueGothic-Regular" size:22.0];
    attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:[strTitle uppercaseString] attributes:attrsDictionary];
    
    [attrString appendAttributedString:newAttString];
    
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:78.0/255.0 green:158.0/255.0 blue:244.0/255.0 alpha:1.0] range:[attrString.string rangeOfString:@"TODAY: "]];
    [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:[attrString.string rangeOfString:[strTitle uppercaseString]]];
    
    
    return attrString;
}


@end
