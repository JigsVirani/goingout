//
//  DiningSpecialPostTableCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 16/12/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiningSpecialPostTableCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *imgDiningPhoto;

@property (nonatomic, retain) IBOutlet UILabel *lblDiningTitle;
@property (nonatomic, retain) IBOutlet UILabel *lblAvailableDining;
@property (nonatomic, retain) IBOutlet UILabel *lblDescription;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *constDiningPhotoHeight;

@end
