//
//  CreateEventVC.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 23/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "DiningSpecialVC.h"

@interface DiningSpecialVC () <UITextFieldDelegate, UITextViewDelegate>
{
    NSInteger intUploadDiningSpecialIndex;
}
@end

@implementation DiningSpecialVC
@synthesize arrAddMore;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _arrExistingDiningSpecial = [[NSMutableArray alloc] init];
    
    if (_isFromUpdateDiningSpecial == false) {
        arrAddMore = [[NSMutableArray alloc] init];
        
        [btnDate setTitle:[Global stringFromDate:[NSDate date] strFormatter:@"EEEE, MMMM dd"] forState:UIControlStateNormal];
        
        [self viewAddMore];
    }else{
        
        [btnDate setTitle:[Global getmonthname:_strDiningDate dateformat:@"yyyy-MM-dd" dateformat2:@"EEEE, MMMM dd"] forState:UIControlStateNormal];
        
        [self setTableFrame];
        
        [btnPost setTitle:@"Save" forState:UIControlStateNormal];
        [btnPost setTitle:@"Save" forState:UIControlStateSelected];
        
        constViewFooterHeight.constant = 0;
    }
    
    CGRect idealFrame = [btnDate.currentTitle boundingRectWithSize:btnDate.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:btnDate.titleLabel.font } context:nil];
    imgArrowDate.frame = CGRectMake(idealFrame.size.width+30, imgArrowDate.frame.origin.y, imgArrowDate.frame.size.width, imgArrowDate.frame.size.height);
    
    intUploadDiningSpecialIndex = 0;
    
    if ([[THIS.dictUserDetail valueForKey:@"venues"] count] != 0) {
        
        lblVenueAdminName.text = [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"name"];
        
        if (![[THIS.dictUserDetail valueForKey:@"logo"] isEqualToString:@""]){
            [imgVenueAdmin sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/getphoto?id=%@&size=logo", IMAGEURL, [[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"imageVenue"]]]];
        }
    }
    
    if (_intFromManageDiningSpecial == 1 && _intAddDiningSpecial == 0) {
        
        if (_isByDish == true) {
            NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc]initWithDictionary:_dictDiningSpecialDetail];
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:[dictTemp valueForKey:@"description"] forKey:@"Description"];
            if ([[dictTemp valueForKey:@"price"] integerValue] != 0) {
                [dict setValue:[dictTemp valueForKey:@"price"] forKey:@"Price"];
            }else{
                [dict setValue:@"" forKey:@"Price"];
            }
            [dict setValue:[dictTemp valueForKey:@"name"] forKey:@"DishName"];
            
            NSString *strMealType = @"";
            if ([[dictTemp valueForKey:@"meal_breakfast"] boolValue] == true) {
                strMealType = @"Breakfast";
            }
            if ([[dictTemp valueForKey:@"meal_lunch"] boolValue] == true) {
                
                if (![strMealType isEqualToString:@""]) {
                    strMealType = [strMealType stringByAppendingString:@", "];
                }
                strMealType = [strMealType stringByAppendingString:@"Lunch"];
            }
            if ([[dictTemp valueForKey:@"meal_brunch"] boolValue] == true) {
                
                if (![strMealType isEqualToString:@""]) {
                    strMealType = [strMealType stringByAppendingString:@", "];
                }
                strMealType = [strMealType stringByAppendingString:@"Brunch"];
            }
            if ([[dictTemp valueForKey:@"meal_dinner"] boolValue] == true) {
                
                if (![strMealType isEqualToString:@""]) {
                    strMealType = [strMealType stringByAppendingString:@", "];
                }
                strMealType = [strMealType stringByAppendingString:@"Dinner"];
            }
            if ([[dictTemp valueForKey:@"meal_latenight"] boolValue] == true) {
                
                if (![strMealType isEqualToString:@""]) {
                    strMealType = [strMealType stringByAppendingString:@", "];
                }
                strMealType = [strMealType stringByAppendingString:@"Late night"];
            }
            [dict setValue:strMealType forKey:@"Meal"];
            [dict setValue:[dictTemp valueForKey:@"id"] forKey:@"dining_id"];
            arrAddMore = [[NSMutableArray alloc] initWithObjects:dict, nil];
            
            if ([dictTemp valueForKey:@"date"]) {
                [btnDate setTitle:[Global getmonthname:[dictTemp valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"EEEE, MMMM dd"] forState:UIControlStateNormal];
                
                CGRect idealFrame = [btnDate.currentTitle boundingRectWithSize:btnDate.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:btnDate.titleLabel.font } context:nil];
                imgArrowDate.frame = CGRectMake(idealFrame.size.width+30, imgArrowDate.frame.origin.y, imgArrowDate.frame.size.width, imgArrowDate.frame.size.height);
            }
            
            constViewFooterHeight.constant = 0;
            constViewDateHeight.constant = 0;
            
        }else{
            
            arrAddMore = [[NSMutableArray alloc] init];
            for (int i=0; i < _arrDateWise.count; i++) {
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"",@"DishName",@"",@"Price",@"",@"Description",@"",@"Meal", nil];
                
                [dict setObject:[[_arrDateWise objectAtIndex:i] valueForKey:@"name"] forKey:@"DishName"];
                if ([[[_arrDateWise objectAtIndex:i] valueForKey:@"price"] integerValue] != 0) {
                    [dict setValue:[[_arrDateWise objectAtIndex:i] valueForKey:@"price"] forKey:@"Price"];
                }else{
                    [dict setValue:@"" forKey:@"Price"];
                }
                [dict setObject:[[_arrDateWise objectAtIndex:i] valueForKey:@"description"] forKey:@"Description"];
                
                NSString *strMealType = @"";
                if ([[[_arrDateWise objectAtIndex:i] valueForKey:@"meal_breakfast"] boolValue] == true) {
                    strMealType = @"Breakfast";
                }
                if ([[[_arrDateWise objectAtIndex:i] valueForKey:@"meal_lunch"] boolValue] == true) {
                    
                    if (![strMealType isEqualToString:@""]) {
                        strMealType = [strMealType stringByAppendingString:@", "];
                    }
                    strMealType = [strMealType stringByAppendingString:@"Lunch"];
                }
                if ([[[_arrDateWise objectAtIndex:i] valueForKey:@"meal_brunch"] boolValue] == true) {
                    
                    if (![strMealType isEqualToString:@""]) {
                        strMealType = [strMealType stringByAppendingString:@", "];
                    }
                    strMealType = [strMealType stringByAppendingString:@"Brunch"];
                }
                if ([[[_arrDateWise objectAtIndex:i] valueForKey:@"meal_dinner"] boolValue] == true) {
                    
                    if (![strMealType isEqualToString:@""]) {
                        strMealType = [strMealType stringByAppendingString:@", "];
                    }
                    strMealType = [strMealType stringByAppendingString:@"Dinner"];
                }
                if ([[[_arrDateWise objectAtIndex:i] valueForKey:@"meal_latenight"] boolValue] == true) {
                    
                    if (![strMealType isEqualToString:@""]) {
                        strMealType = [strMealType stringByAppendingString:@", "];
                    }
                    strMealType = [strMealType stringByAppendingString:@"Late night"];
                }
                
                [dict setObject:strMealType forKey:@"Meal"];
                
                [dict setValue:[[_arrDateWise objectAtIndex:i] valueForKey:@"id"] forKey:@"dining_id"];
                [arrAddMore addObject:dict];
                
                if (i == 0) {
                    
                    [btnDate setTitle:[Global getmonthname:[[_arrDateWise objectAtIndex:i] valueForKey:@"date"] dateformat:@"yyyy-MM-dd" dateformat2:@"EEEE, MMMM dd"] forState:UIControlStateNormal];
                    
                    CGRect idealFrame = [btnDate.currentTitle boundingRectWithSize:btnDate.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:btnDate.titleLabel.font } context:nil];
                    imgArrowDate.frame = CGRectMake(idealFrame.size.width+30, imgArrowDate.frame.origin.y, imgArrowDate.frame.size.width, imgArrowDate.frame.size.height);
                }
            }
            [self setTableFrame];
            
            btnDate.enabled = false;
            imgArrowDate.hidden = true;
        }
        
        [tblAddMoreDish reloadData];
        
        [btnPost setTitle:@"Save" forState:UIControlStateNormal];
        [btnPost setTitle:@"Save" forState:UIControlStateSelected];
    }
    
    if (_intAddDiningSpecial == 1) {
        
        constViewFooterHeight.constant = 0;
        constViewDateHeight.constant = 0;
        
        [btnPost setTitle:@"Save" forState:UIControlStateNormal];
        [btnPost setTitle:@"Save" forState:UIControlStateSelected];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:true];
    
    if (_arrExistingDiningSpecial.count > 1) {
        arrAddMore = [[NSMutableArray alloc] init];
    }
    for (int i = 0; i < _arrExistingDiningSpecial.count; i++) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        if (_arrExistingDiningSpecial.count == 1) {
            dict = [[NSMutableDictionary alloc] initWithDictionary:[arrAddMore objectAtIndex:_intSelectedIndex]];
        }
        
        [dict setValue:[[_arrExistingDiningSpecial objectAtIndex:i] valueForKey:@"description"] forKey:@"Description"];
        if ([[[_arrExistingDiningSpecial objectAtIndex:i] valueForKey:@"price"] integerValue] != 0) {
            [dict setValue:[[_arrExistingDiningSpecial objectAtIndex:i] valueForKey:@"price"] forKey:@"Price"];
        }else{
            [dict setValue:@"" forKey:@"Price"];
        }
        [dict setValue:[[_arrExistingDiningSpecial objectAtIndex:i] valueForKey:@"name"] forKey:@"DishName"];
        
        NSString *strMealType = @"";
        if ([[[_arrExistingDiningSpecial objectAtIndex:i] valueForKey:@"meal_breakfast"] boolValue] == true) {
            strMealType = @"Breakfast";
        }
        if ([[[_arrExistingDiningSpecial objectAtIndex:i] valueForKey:@"meal_lunch"] boolValue] == true) {
            
            if (![strMealType isEqualToString:@""]) {
                strMealType = [strMealType stringByAppendingString:@", "];
            }
            strMealType = [strMealType stringByAppendingString:@"Lunch"];
        }
        if ([[[_arrExistingDiningSpecial objectAtIndex:i] valueForKey:@"meal_brunch"] boolValue] == true) {
            
            if (![strMealType isEqualToString:@""]) {
                strMealType = [strMealType stringByAppendingString:@", "];
            }
            strMealType = [strMealType stringByAppendingString:@"Brunch"];
        }
        if ([[[_arrExistingDiningSpecial objectAtIndex:i] valueForKey:@"meal_dinner"] boolValue] == true) {
            
            if (![strMealType isEqualToString:@""]) {
                strMealType = [strMealType stringByAppendingString:@", "];
            }
            strMealType = [strMealType stringByAppendingString:@"Dinner"];
        }
        if ([[[_arrExistingDiningSpecial objectAtIndex:i] valueForKey:@"meal_latenight"] boolValue] == true) {
            
            if (![strMealType isEqualToString:@""]) {
                strMealType = [strMealType stringByAppendingString:@", "];
            }
            strMealType = [strMealType stringByAppendingString:@"Late night"];
        }
        [dict setValue:strMealType forKey:@"Meal"];
        
        if (_arrExistingDiningSpecial.count == 1) {
            [arrAddMore replaceObjectAtIndex:_intSelectedIndex withObject:dict];
        }else{
            [arrAddMore addObject:dict];
        }
        [self validatePost];
        
        NSLog(@"%@",[_arrExistingDiningSpecial objectAtIndex:i]);
    }
    _arrExistingDiningSpecial = [[NSMutableArray alloc] init];
    [tblAddMoreDish reloadData];
    [self setTableFrame];
//    int64_t intTime = (int64_t)(1 * NSEC_PER_SEC);
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, intTime), dispatch_get_main_queue(), ^
//                   {
//                       dispatch_async(dispatch_get_main_queue(), ^(void)
//                                      {
//                                      });
//                   });
    
    if (_strSelectedMeals != nil)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrAddMore objectAtIndex:_intSelectedIndex]];
        [dict setObject:_strSelectedMeals forKey:@"Meal"];
        [arrAddMore replaceObjectAtIndex:_intSelectedIndex withObject:dict];
        
        [self validatePost];
        
        [tblAddMoreDish reloadData];
        
        _strSelectedMeals = nil;
    }
    if ([arrAddMore count] != 0 && [[arrAddMore objectAtIndex:0] valueForKey:@"dining_id"]) {
        
        [btnPost setTitle:@"Save" forState:UIControlStateNormal];
        [btnPost setTitle:@"Save" forState:UIControlStateSelected];
    }
}

#pragma mark - Web Service Call

-(void)runLoopOfDiningSpecial{
    
    if (intUploadDiningSpecialIndex < arrAddMore.count) {
        [self createDiningSpecialJson];
    }else{
        intUploadDiningSpecialIndex = 0;
        LOADINGHIDE
    }
}

-(void)createDiningSpecialJson
{
    UIImageView *imgPhoto = [[UIImageView alloc] initWithImage:[[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] valueForKey:@"Photo"]];
    
    NSArray *arrMeals = [[[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] valueForKey:@"Meal"] componentsSeparatedByString:@", "];
    
    NSString *strMealBreakfast = @"0";
    NSString *strMealBrunch = @"0";
    NSString *strMealLunch = @"0";
    NSString *strMealDinner = @"0";
    NSString *strMealLateNight = @"0";
    
    for (int i=0; i<arrMeals.count; i++) {
        
        if ([[arrMeals objectAtIndex:i] isEqualToString:@"Breakfast"]) {
            strMealBreakfast = @"1";
        }
        if ([[arrMeals objectAtIndex:i] isEqualToString:@"Brunch"]) {
            strMealBrunch = @"1";
        }
        if ([[arrMeals objectAtIndex:i] isEqualToString:@"Lunch"]) {
            strMealLunch = @"1";
        }
        if ([[arrMeals objectAtIndex:i] isEqualToString:@"Dinner"]) {
            strMealDinner = @"1";
        }
        if ([[arrMeals objectAtIndex:i] isEqualToString:@"Late Night"]) {
            strMealLateNight = @"1";
        }
    }
    
    NSString *strDate = [NSString stringWithFormat:@"%@-%@", [Global stringFromDate:[NSDate date] strFormatter:@"yyyy"],[Global getmonthname:btnDate.currentTitle dateformat:@"EEEE, MMMM dd" dateformat2:@"MM-dd"]];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    if (_dictDiningSpecialDetail == nil && _intAddDiningSpecial != 1) {
        [dict setValue:strDate forKey:@"date"];
    }if (_intFromManageDiningSpecial == 1 && _isByDish == false) {
        [dict setValue:strDate forKey:@"date"];
    }
    [dict setValue:[[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] valueForKey:@"DishName"] forKey:@"dish"];
    [dict setValue:[[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] valueForKey:@"Description"] forKey:@"description"];
    [dict setValue:[[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] valueForKey:@"Price"] forKey:@"price"];
    [dict setValue:strMealBreakfast forKey:@"meal_breakfast"];
    [dict setValue:strMealBrunch forKey:@"meal_brunch"];
    [dict setValue:strMealLunch forKey:@"meal_lunch"];
    [dict setValue:strMealDinner forKey:@"meal_dinner"];
    [dict setValue:strMealLateNight forKey:@"meal_latenight"];
    
    if (_intFromManageDiningSpecial == 1 && _isByDish == false) {
        
        [[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] setValue:[NSNumber numberWithInteger:[strMealBreakfast integerValue]] forKey:@"meal_breakfast"];
        [[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] setValue:[NSNumber numberWithInteger:[strMealBrunch integerValue]] forKey:@"meal_brunch"];
        [[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] setValue:[NSNumber numberWithInteger:[strMealDinner integerValue]] forKey:@"meal_dinner"];
        [[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] setValue:[NSNumber numberWithInteger:[strMealLateNight integerValue]] forKey:@"meal_latenight"];
        [[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] setValue:[NSNumber numberWithInteger:[strMealLunch integerValue]] forKey:@"meal_lunch"];
    }
    
    NSString *strApiType = @"";
    
    if ([[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] valueForKey:@"dining_id"]){
        strApiType = @"UpateDiningSpecial";
        
        [dict setValue:[[arrAddMore objectAtIndex:intUploadDiningSpecialIndex] valueForKey:@"dining_id"] forKey:@"id"];
        [dict setValue:@"NO_UPDATE" forKey:@"photo_status"];
    }else{
        
        if (_intAddDiningSpecial != 1) {
            strApiType = @"CreateDiningSpecial";
        }else{
            strApiType = @"addDiningSpecial";
        }
        
        [dict setValue:[[[THIS.dictUserDetail valueForKey:@"venues"] objectAtIndex:THIS.intSelectedVenueIndex] valueForKey:@"id"] forKey:@"venue_id"];
    }

    NSDictionary *dictJson = [Global requestForPostWithImage:dict isPassToken:true withPhoto:imgPhoto GetKey:strApiType];
    
    if (dictJson != nil){
        if (_intFromManageDiningSpecial != 1 && _isFromUpdateDiningSpecial == false && [[dictJson valueForKey:@"success"] boolValue] == true){
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrAddMore objectAtIndex:intUploadDiningSpecialIndex]];
            [dict setValue:[[dictJson valueForKey:@"data"] valueForKey:@"dining_id"] forKey:@"dining_id"];
            [arrAddMore replaceObjectAtIndex:intUploadDiningSpecialIndex withObject:dict];
        }
    }
    
    intUploadDiningSpecialIndex = intUploadDiningSpecialIndex + 1;
    
    if (intUploadDiningSpecialIndex < arrAddMore.count) {
        [self runLoopOfDiningSpecial];
        
    }else{
        
        LOADINGHIDE
        if (dictJson != nil)
        {
            if ([[dictJson valueForKey:@"success"] boolValue] == true)
            {
                [Global displayTost:[dictJson valueForKey:@"message"]];
                
                if (_intFromManageDiningSpecial == 1) {
                    
                    if (_isByDish == true) {
                        [_dictDiningSpecialDetail setValue:[[arrAddMore objectAtIndex:0] valueForKey:@"Description"] forKey:@"description"];
                        [_dictDiningSpecialDetail setValue:[[arrAddMore objectAtIndex:0] valueForKey:@"DishName"] forKey:@"name"];
                        [_dictDiningSpecialDetail setValue:[[arrAddMore objectAtIndex:0] valueForKey:@"Price"] forKey:@"price"];
                        [_dictDiningSpecialDetail setValue:strDate forKey:@"date"];
                        
                        
                        [_dictDiningSpecialDetail setValue:[NSNumber numberWithInteger:[strMealBreakfast integerValue]] forKey:@"meal_breakfast"];
                        [_dictDiningSpecialDetail setValue:[NSNumber numberWithInteger:[strMealBrunch integerValue]] forKey:@"meal_brunch"];
                        [_dictDiningSpecialDetail setValue:[NSNumber numberWithInteger:[strMealDinner integerValue]] forKey:@"meal_dinner"];
                        [_dictDiningSpecialDetail setValue:[NSNumber numberWithInteger:[strMealLateNight integerValue]] forKey:@"meal_latenight"];
                        [_dictDiningSpecialDetail setValue:[NSNumber numberWithInteger:[strMealLunch integerValue]] forKey:@"meal_lunch"];
                    }else{
                        for (int i=0; i < arrAddMore.count; i++) {
                            
                            [[_arrDateWise objectAtIndex:i] setValue:[[arrAddMore objectAtIndex:i] valueForKey:@"Description"] forKey:@"description"];
                            [[_arrDateWise objectAtIndex:i] setValue:[[arrAddMore objectAtIndex:i] valueForKey:@"DishName"] forKey:@"name"];
                            [[_arrDateWise objectAtIndex:i] setValue:[[arrAddMore objectAtIndex:i] valueForKey:@"Price"] forKey:@"price"];
                            [[_arrDateWise objectAtIndex:i] setValue:strDate forKey:@"date"];
                            
                            
                            [[_arrDateWise objectAtIndex:i] setValue:[[arrAddMore objectAtIndex:i] valueForKey:@"meal_breakfast"] forKey:@"meal_breakfast"];
                            [[_arrDateWise objectAtIndex:i] setValue:[[arrAddMore objectAtIndex:i] valueForKey:@"meal_brunch"] forKey:@"meal_brunch"];
                            [[_arrDateWise objectAtIndex:i] setValue:[[arrAddMore objectAtIndex:i] valueForKey:@"meal_dinner"] forKey:@"meal_dinner"];
                            [[_arrDateWise objectAtIndex:i] setValue:[[arrAddMore objectAtIndex:i] valueForKey:@"meal_latenight"] forKey:@"meal_latenight"];
                            [[_arrDateWise objectAtIndex:i] setValue:[[arrAddMore objectAtIndex:i] valueForKey:@"meal_lunch"] forKey:@"meal_lunch"];
                        }
                    }
                    
                    [self.navigationController popViewControllerAnimated:true];
                }else{
                    DiningSpecialPostVC *diningSpecialPost = [[DiningSpecialPostVC alloc] initWithNibName:@"DiningSpecialPostVC" bundle:nil];
                    diningSpecialPost.arrDiningSpecial = [[NSMutableArray alloc] initWithArray:arrAddMore];
                    
                    if (_intAddDiningSpecial != 1) {
                        diningSpecialPost.strDiningDate = strDate;
                    }else{
                        diningSpecialPost.strDiningDate = @"";
                    }
                    [self.navigationController pushViewController:diningSpecialPost animated:true];
                }
            }
            else
            {
                [Global displayTost:[dictJson valueForKey:@"message"]];
                intUploadDiningSpecialIndex = 0;
            }
        }
        else
        {
            PROBLEM
        }
    }
}

#pragma mark - Button TouchUp

-(IBAction)btnCancelAction:(id)sender
{
    [self.view endEditing:YES];
    
    if (_intFromManageDiningSpecial == 1 || [[btnPost currentTitle] isEqualToString:@"Save"] || _intAddDiningSpecial == 1){
        [self.navigationController popViewControllerAnimated:true];
    }else{
        for (id controller in [self.navigationController viewControllers]){
            if ([controller isKindOfClass:[DashboardVC class]]){
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
    }
}

-(IBAction)btnPostAction:(id)sender
{
    [self.view endEditing:YES];
    
    if (![[arrAddMore objectAtIndex:arrAddMore.count-1] valueForKey:@"DishName"] || [[[arrAddMore objectAtIndex:arrAddMore.count-1] valueForKey:@"DishName"] isEqualToString:@""]){
        [Global displayTost:@"Please enter or select Dish Name"];
    }else if (![[arrAddMore objectAtIndex:arrAddMore.count-1] valueForKey:@"Meal"] || [[[arrAddMore objectAtIndex:arrAddMore.count-1] valueForKey:@"Meal"] isEqualToString:@""]){
        [Global displayTost:@"Please select meal name"];
    }else{
        
        intUploadDiningSpecialIndex = 0;
        LOADINGSHOW
        [self performSelector:@selector(runLoopOfDiningSpecial) withObject:nil afterDelay:0.1];
    }
}

-(IBAction)btnDateAction:(id)sender;
{
    [self.view endEditing:true];
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    [self.view addSubview:viewDatePicker];
    viewDatePicker.frame = CGRectMake(0, self.view.frame.size.height-viewDatePicker.frame.size.height, self.view.frame.size.width, viewDatePicker.frame.size.height);
    
    datePicker.minimumDate = [NSDate date];
}

-(IBAction)btnDoneDatePicker:(id)sender
{
    [viewDatePicker removeFromSuperview];
    
    if ([sender tag] == 2)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE, MMMM dd"];
        
        [btnDate setTitle:[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]] forState:UIControlStateNormal];
        
        CGRect idealFrame = [btnDate.currentTitle boundingRectWithSize:btnDate.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:btnDate.titleLabel.font } context:nil];
        imgArrowDate.frame = CGRectMake(idealFrame.size.width+30, imgArrowDate.frame.origin.y, imgArrowDate.frame.size.width, imgArrowDate.frame.size.height);
        
        [self validatePost];
    }
}

-(IBAction)btnFooterAction:(id)sender
{
    [self.view endEditing:YES];
    if ([sender tag] == 1)
    {
        for (id controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[EventPostVC class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
        EventPostVC *eventPostVC = [[EventPostVC alloc] initWithNibName:@"EventPostVC" bundle:nil];
        [self.navigationController pushViewController:eventPostVC animated:YES];
    }
    else if([sender tag] == 2)
    {
        for (id controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[CreateEventVC class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
                return;
            }
        }
        CreateEventVC *createEvent = [[CreateEventVC alloc] initWithNibName:@"CreateEventVC" bundle:nil];
        [self.navigationController pushViewController:createEvent animated:YES];
    }
    else if ([sender tag] == 4)
    {
        TakePhotoVC *takePhoto = [[TakePhotoVC alloc] initWithNibName:@"TakePhotoVC" bundle:nil];
        [self.navigationController pushViewController:takePhoto animated:YES];
    }
}

-(IBAction)btnSharingAction:(id)sender
{
    [self.view endEditing:true];
    if ([sender tag] == 1)
    {
        if ([btnFacebook isSelected] == TRUE)
        {
            btnFacebook.selected = FALSE;
        }
        else
        {
            btnFacebook.selected = true;
        }
        
    }
    else if ([sender tag] == 2)
    {
        btnTwitter = sender;
        if ([btnTwitter isSelected] == TRUE)
        {
            btnTwitter.selected = FALSE;
        }
        else
        {
            btnTwitter.selected = true;
        }
    }
    else if ([sender tag] == 3)
    {
        btnInsta = sender;
        if ([btnInsta isSelected] == TRUE)
        {
            btnInsta.selected = FALSE;
        }
        else
        {
            btnInsta.selected = true;
        }
    }
}

-(IBAction)btnAddAnotherDishAction:(id)sender
{
    [self.view endEditing:YES];
    
    if (![[arrAddMore objectAtIndex:arrAddMore.count-1] valueForKey:@"DishName"] || [[[arrAddMore objectAtIndex:arrAddMore.count-1] valueForKey:@"DishName"] isEqualToString:@""]) {
        [Global displayTost:@"Please enter or select Dish Name"];
        
    }else if (![[arrAddMore objectAtIndex:arrAddMore.count-1] valueForKey:@"Meal"] || [[[arrAddMore objectAtIndex:arrAddMore.count-1] valueForKey:@"Meal"] isEqualToString:@""]){
        [Global displayTost:@"Please select meal name"];
    }else{
        [self viewAddMore];
    }
}

#pragma mark:Cell Event

-(IBAction)btnCrossAction:(id)sender
{
    [self.view endEditing:YES];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrAddMore objectAtIndex:[sender tag]]];
    [dict removeObjectForKey:@"Photo"];
    [arrAddMore replaceObjectAtIndex:[sender tag] withObject:dict];
    
    [tblAddMoreDish reloadData];
    [self setTableFrame];
    [self validatePost];
}

-(IBAction)btnUploadPhotoAction:(id)sender
{
    [self.view endEditing:YES];
    
    intAddMoreIndex = [sender tag];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take A New Photo",@"Choose From Gallery",nil];
    
    [actionSheet showInView:self.view];
}

-(IBAction)btnChooseExistingAction:(id)sender
{
    [self.view endEditing:YES];
    
    UIButton *btn = (UIButton*) sender;
    
    ThemesVC *themes = [[ThemesVC alloc] initWithNibName:@"ThemesVC" bundle:nil];
    themes.intFromDiningSpecial = 1;
    themes.intSelectedIndex = btn.tag;
    themes.intFromManageDiningSpecial = _intFromManageDiningSpecial;
    [self.navigationController pushViewController:themes animated:YES];
}

-(IBAction)btnMealAction:(id)sender
{
    [self.view endEditing:YES];
    
    UIButton *btn = (UIButton*) sender;
    
    ThemesVC *themes = [[ThemesVC alloc] initWithNibName:@"ThemesVC" bundle:nil];
    themes.intFromDiningSpecial = 2;
    themes.intSelectedIndex = btn.tag;
    [self.navigationController pushViewController:themes animated:YES];
}

#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrAddMore.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DiningSpecialTableCell";
    
    DiningSpecialTableCell *cell;
    
    if (cell == nil)
    {
        cell = [(DiningSpecialTableCell *)[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if (![[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Photo"])
    {
        cell.viewPhoto.hidden = true;
    }
    else
    {
        if ([[[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Photo"] isKindOfClass:[UIImage class]] == true)
        {
            cell.imgPhoto.image = [[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Photo"];
        }
    }
    
    cell.txtDishName.text = [NSString stringWithFormat:@"%@",[[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"DishName"]];
    
    if (![[[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Price"] isEqualToString:@""]) {
        float myVal = [[[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Price"] floatValue];
        int wholePart = (int)myVal;
        float fractionalPart = fmodf(myVal, wholePart);
        
        if (fractionalPart > 0.0) {
            cell.txtPrice.text = [NSString stringWithFormat:@"%.2f",[[[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Price"] floatValue]];
        }else{
            cell.txtPrice.text = [NSString stringWithFormat:@"%ld",[[[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Price"] integerValue]];
        }
    }
    
    cell.txtViewDescription.text = [NSString stringWithFormat:@"%@",[[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Description"]];
    
    if (![[[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Meal"] isEqualToString:@""])
    {
        [cell.btnMeal setTitle:[[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Meal"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnMeal setTitle:@"Meals" forState:UIControlStateNormal];
    }
    
    CGRect idealFrame = [cell.btnMeal.currentTitle boundingRectWithSize:cell.btnMeal.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:cell.btnMeal.titleLabel.font } context:nil];
    cell.imgArrowMeal.frame = CGRectMake(idealFrame.size.width+30, cell.imgArrowMeal.frame.origin.y, cell.imgArrowMeal.frame.size.width, cell.imgArrowMeal.frame.size.height);
    
    cell.btnCross.tag = indexPath.row;
    [cell.btnCross addTarget:self action:@selector(btnCrossAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnUploadPhoto.tag = indexPath.row;
    [cell.btnUploadPhoto addTarget:self action:@selector(btnUploadPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnSelectPhoto.tag = indexPath.row;
    [cell.btnSelectPhoto addTarget:self action:@selector(btnUploadPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnChooseExisting.tag = indexPath.row;
    [cell.btnChooseExisting addTarget:self action:@selector(btnChooseExistingAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnMeal.tag = indexPath.row;
    [cell.btnMeal addTarget:self action:@selector(btnMealAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.txtDishName.delegate = self;
    cell.txtDishName.tag = indexPath.row;
    cell.txtDishName.accessibilityIdentifier = @"1";
    [cell.txtDishName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    cell.txtPrice.delegate = self;
    cell.txtPrice.tag = indexPath.row;
    cell.txtPrice.accessibilityIdentifier = @"2";
    [cell.txtPrice addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    cell.txtViewDescription.delegate = self;
    cell.txtViewDescription.tag = indexPath.row;
    cell.txtViewDescription.accessibilityIdentifier = @"3";
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[arrAddMore objectAtIndex:indexPath.row] valueForKey:@"Photo"])
    {
        return 512;
    }
    else
    {
        return 262;
    }
}

#pragma mark - Set AddMore View

- (void)viewAddMore
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"",@"DishName",@"",@"Price",@"",@"Description",@"",@"Meal", nil];
    
    [arrAddMore addObject:dict];
    
    [self setTableFrame];
    
    [tblAddMoreDish reloadData];
}

-(void)setTableFrame
{
    NSInteger height = 0;
    for (int i=0; i<arrAddMore.count; i++)
    {
        if ([[arrAddMore objectAtIndex:i] valueForKey:@"Photo"])
        {
            height = height + 512;
        }
        else
        {
            height = height + 262;
        }
    }
    
    constTableViewHeight.constant = height;
    
//    tblAddMoreDish.frame = CGRectMake(0, tblAddMoreDish.frame.origin.y, CurrenscreenWidth, height);
    
//    viewTableFooter.frame = CGRectMake(0, tblAddMoreDish.frame.origin.y+tblAddMoreDish.frame.size.height, CurrenscreenWidth, viewTableFooter.frame.size.height);
    
//    scrlDiningSpecial.contentSize = CGSizeMake(CurrenscreenWidth, viewTableFooter.frame.origin.y+viewTableFooter.frame.size.height);
}

#pragma mark - Image Action

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            [Global displayTost:@"Camera preview not available on this device"];
        }
    }
    else if (buttonIndex == 1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        UIImage *image;
        
        controller.image = img;
        image = img;
        
        CGFloat width = image.size.width;
        CGFloat height = image.size.height;
        CGFloat length = MIN(width, height);
        controller.imageCropRect = CGRectMake((width - length) / 2,
                                              (height - length) / 2,
                                              length,
                                              length);
        
        controller.cropRect = CGRectMake(0, 0, CurrenscreenWidth, 248);
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        
        [self presentViewController:navigationController animated:YES completion:NULL];
    }];
    
    
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
//    imgPhoto.image = croppedImage;
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrAddMore objectAtIndex:intAddMoreIndex]];
    [dict setObject:croppedImage forKey:@"Photo"];
    [arrAddMore replaceObjectAtIndex:intAddMoreIndex withObject:dict];
    
    [tblAddMoreDish reloadData];
    
    [self setTableFrame];
    
    [self validatePost];
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - TextField Delegate

-(void)textFieldDidChange:(UITextField *)textField
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrAddMore objectAtIndex:[textField tag]]];
    
    if (textField.accessibilityIdentifier.integerValue == 1)
    {
        [dict setObject:textField.text forKey:@"DishName"];
    }
    else if (textField.accessibilityIdentifier.integerValue == 2)
    {
        [dict setObject:textField.text forKey:@"Price"];
    }
    
    [arrAddMore replaceObjectAtIndex:[textField tag] withObject:dict];
    
    [self validatePost];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField.accessibilityIdentifier isEqualToString:@"2"] && textField.text.length == 0 && [string isEqualToString:@"0"]) {
        return false;
    }else if ([textField.accessibilityIdentifier isEqualToString:@"2"] && textField.text.length == 0 && [string isEqualToString:@"."]) {
        return false;
    }
    return true;
}

#pragma mark - TextView Delegate



- (void)textViewDidChange:(UITextView *)textView;
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[arrAddMore objectAtIndex:[textView tag]]];
    
    if (textView.accessibilityIdentifier.integerValue == 3)
    {
        [dict setObject:textView.text forKey:@"Description"];
    }
    [arrAddMore replaceObjectAtIndex:[textView tag] withObject:dict];
    
    [self validatePost];
}

#pragma mark - Validate Post

-(void) validatePost
{
    if (![[[arrAddMore objectAtIndex:0] valueForKey:@"DishName"] isEqualToString:@""])
    {
        [btnPost.titleLabel setFont:[UIFont boldSystemFontOfSize:14.0]];
        [btnPost setTitleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    }
    else
    {
        [btnPost.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [btnPost setTitleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.7] forState:UIControlStateNormal];
    }
}

#pragma mark - Sharing

-(void)fbShare
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *fbShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        NSString *strText = @"";
        if (![[[arrAddMore objectAtIndex:0] valueForKey:@"DishName"] isEqualToString:@""])
        {
            strText = [NSString stringWithFormat:@"Dish Name : %@",[[arrAddMore objectAtIndex:0] valueForKey:@"DishName"]];
        }
        if (![[[arrAddMore objectAtIndex:0] valueForKey:@"Price"] isEqualToString:@""])
        {
            strText = [strText stringByAppendingString:[[arrAddMore objectAtIndex:0] valueForKey:@"Price"]];
        }
        if (![[[arrAddMore objectAtIndex:0] valueForKey:@"Description"] isEqualToString:@""])
        {
            strText = [strText stringByAppendingString:[[arrAddMore objectAtIndex:0] valueForKey:@"Description"]];
        }
        
        [fbShare setInitialText:strText];
        
        if ([[arrAddMore objectAtIndex:0] valueForKey:@"Photo"])
        {
            [fbShare addImage:[[arrAddMore objectAtIndex:0] valueForKey:@"Photo"]];
        }
        
        [fbShare setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             if (result == SLComposeViewControllerResultCancelled)
             {
                 [Global displayTost:@"You have cancelled Facebook sharing"];
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 [Global displayTost:@"Successfully posted on Facebook"];
             }
             
             btnFacebook.selected = false;
         }];
        [self.navigationController presentViewController:fbShare animated:YES completion:^{
            LOADINGHIDE
        }];
        
    }
    else
    {
        LOADINGHIDE
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:@"Facebook integration is not available.  A Facebook account must be set up on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        btnFacebook.selected = false;
    }
}

-(void)twitterShare
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *twitShare = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString *strText = @"";
        if (![[[arrAddMore objectAtIndex:0] valueForKey:@"DishName"] isEqualToString:@""])
        {
            strText = [NSString stringWithFormat:@"Dish Name : %@",[[arrAddMore objectAtIndex:0] valueForKey:@"DishName"]];
        }
        if (![[[arrAddMore objectAtIndex:0] valueForKey:@"Price"] isEqualToString:@""])
        {
            strText = [strText stringByAppendingString:[[arrAddMore objectAtIndex:0] valueForKey:@"Price"]];
        }
        if (![[[arrAddMore objectAtIndex:0] valueForKey:@"Description"] isEqualToString:@""])
        {
            strText = [strText stringByAppendingString:[[arrAddMore objectAtIndex:0] valueForKey:@"Description"]];
        }
        
        [twitShare setInitialText:strText];
        
        if ([[arrAddMore objectAtIndex:0] valueForKey:@"Photo"])
        {
            [twitShare addImage:[[arrAddMore objectAtIndex:0] valueForKey:@"Photo"]];
        }
        
        [twitShare setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             if (result == SLComposeViewControllerResultCancelled)
             {
                 [Global displayTost:@"You have cancelled Twitter sharing"];
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 [Global displayTost:@"Successfully posted on Twitter"];
             }
             
             btnTwitter.selected = false;
         }];
        [self.navigationController presentViewController:twitShare animated:YES completion:^{
            LOADINGHIDE
        }];
        
    }
    else
    {
        LOADINGHIDE
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:@"Twitter integration is not available.  A Twitter account must be set up on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        btnTwitter.selected = false;
    }
}

@end
