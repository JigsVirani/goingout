//
//  DiningSpecialTableCell.h
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 14/09/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiningSpecialTableCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIView *viewPhoto;

@property (nonatomic, retain) IBOutlet UIImageView *imgPhoto;

@property (nonatomic, retain) IBOutlet UIButton *btnUploadPhoto;
@property (nonatomic, retain) IBOutlet UIButton *btnCross;
@property (nonatomic, retain) IBOutlet UIButton *btnChooseExisting;
@property (nonatomic, retain) IBOutlet UIButton *btnMeal;

@property (nonatomic, retain) IBOutlet UIImageView *imgArrowMeal;

@property (nonatomic, retain) IBOutlet UIView *viewPhotoFooter;

@property (nonatomic, retain) IBOutlet UITextField *txtDishName;
@property (nonatomic, retain) IBOutlet UITextField *txtPrice;

@property (nonatomic, retain) IBOutlet UILabel *lblDollarSign;

@property (nonatomic, retain) IBOutlet UIButton *btnSelectPhoto;

@property (nonatomic, retain) IBOutlet UITextView *txtViewDescription;

@end
