//
//  AppDelegate.m
//  Venue Owner
//
//  Created by Sandeep Gangajaliya on 21/08/16.
//  Copyright © 2016 Sandeep Gangajaliya. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginVC.h"
#import "IQKeyboardManager.h"
#import "Reachability.h"
#import "GlobalViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
    LoginVC *loginVC = [[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
    self.nav = [[SlideNavigationController alloc]initWithRootViewController:loginVC];
    
    MenuViewController *leftMenu = [[MenuViewController alloc] init];
    leftMenu.cellIdentifier = @"MenuCell";
    [SlideNavigationController sharedInstance].rightMenu = nil;
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    
    [self.nav setNavigationBarHidden:YES];
    self.intSelectedVenueIndex = 0;
    
    self.window.rootViewController = self.nav;
    [self.window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    _dictUserDetail = [[NSMutableDictionary alloc] init];
    _dictCurrentDay = [[NSMutableDictionary alloc] init];
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetail"]) {
        
        _dictUserDetail = [[NSMutableDictionary alloc] initWithDictionary: [[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetail"]];
        
        NSLog(@"UserDetail = %@", _dictUserDetail);
        
        DashboardVC *dashboardVC = [[DashboardVC alloc] initWithNibName:@"DashboardVC" bundle:nil];
        [self.nav pushViewController:dashboardVC animated:YES];
    }
    
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

static AppDelegate *temp = nil;
+(AppDelegate*)sharedInstance
{
    if(!temp)
    {
        temp = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    }
    return temp;
}

#pragma mark - Network Reachability

+(BOOL)CheckNetworkReachability
{
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.google.com"];
    if ([reach isReachable])
    {
        return YES;
    }
    else
    {
        return YES;
    }
}

@end
